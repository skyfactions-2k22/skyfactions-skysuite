package fr.floxiik.skyapi.bungee;

import fr.floxiik.skyapi.api.SkygeeApi;
import fr.floxiik.skyapi.bungee.skycore.SkyCoreBungee;
import fr.floxiik.skyapi.bungee.skycore.managers.TeleportManager;
import fr.floxiik.skyapi.bungee.skycrew.SkyCrewBungee;
import fr.floxiik.skyapi.data.redis.RedisAccess;
import fr.floxiik.skyapi.data.sql.DbManager;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeCore extends Plugin {

    private static BungeeCore instance;
    private static SkygeeApi api;
    private static TeleportManager teleportManager;

    public void onEnable() {
        instance = this;
        api = SkygeeApi.get();

        DbManager.initAllDatabaseConnections();
        RedisAccess.init();

        new SkyCoreBungee().init(this);
        new SkyCrewBungee().init(this);

    }

    public void onDisable() {
        DbManager.closeAllDatabaseConnections();
        RedisAccess.close();

        SkyCoreBungee.getInstance().disable(this);
        SkyCrewBungee.getInstance().disable(this);
    }

    public static SkygeeApi getApi() {
        return api;
    }

    public static BungeeCore getInstance() {
        return instance;
    }
}
