package fr.floxiik.skyapi.bungee.skycore.teleportations;

import java.util.UUID;

public class TeleportRequest {

    private UUID uuid;
    private UUID uuidTo;
    private String location;
    private String server;
    private TeleportType teleportType;

    public TeleportRequest(UUID uuid, String location, String server, TeleportType teleportType) {
        this.uuid = uuid;
        this.location = location;
        this.server = server;
        this.teleportType = teleportType;
        if(TeleportType.isPlayerToPlayer(teleportType)) {
            uuidTo = UUID.fromString(getLocation());
        }
    }

    public UUID getUuid() {
        return uuid;
    }

    public UUID getUuidTo() {
        return uuidTo;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public TeleportType getTeleportType() {
        return teleportType;
    }

    public void setTeleportType(TeleportType teleportType) {
        this.teleportType = teleportType;
    }
}
