package fr.floxiik.skyapi.bungee.skycore.commands;

import fr.floxiik.skyapi.api.LangAPI;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Broadcast extends Command {

    public Broadcast() {
        super("broadcast");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            if (player.hasPermission("skycore.broadcast")) {
                if (strings.length > 0) {
                    StringBuilder msg = new StringBuilder("§5[§dANNONCE§5]§d");
                    for (String part : strings) {
                        msg.append(" ").append(part.replaceAll("&", "§"));
                    }
                    ProxyServer.getInstance().broadcast(new TextComponent(msg.toString()));
                } else {
                    player.sendMessage("§cUsage: /broadcast <message to broadcast>");
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
    }
}
