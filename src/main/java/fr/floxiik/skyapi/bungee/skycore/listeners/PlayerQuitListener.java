package fr.floxiik.skyapi.bungee.skycore.listeners;

import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.data.Crew;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerDisconnectEvent event) {
        UUID player = event.getPlayer().getUniqueId();
        if(BungeeCore.getApi().hasCrew(player)) {
            BungeeCore.getApi().saveCrew(player);
            Crew crew = BungeeCore.getApi().getCrew(player);
            if(BungeeCore.getApi().isOwner(player)) {
                for(UUID member : crew.getMembers().keySet()) {
                    if(ProxyServer.getInstance().getPlayer(member) != null) {
                        return;
                    }
                }
            } else {
                for(UUID member : crew.getMembers().keySet()) {
                    if(!member.toString().equals(player.toString())) {
                        return;
                    } else if(ProxyServer.getInstance().getPlayer(member) != null) {
                        return;
                    }
                }
                if(ProxyServer.getInstance().getPlayer(crew.getOwner()) != null) {
                    return;
                }
            }
            BungeeCord.getInstance().getScheduler().schedule(BungeeCore.getInstance(), () -> {
                BungeeCore.getApi().saveAccount(player);
                //BungeeCore.getApi().stopCrewServer(crew.getServerId());
            }, 10, TimeUnit.MILLISECONDS);
        }
    }
}
