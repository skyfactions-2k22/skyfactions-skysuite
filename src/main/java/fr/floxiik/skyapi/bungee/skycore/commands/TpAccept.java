package fr.floxiik.skyapi.bungee.skycore.commands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.bungee.skycore.SkyCoreBungee;
import fr.floxiik.skyapi.bungee.skycore.teleportations.TeleportType;
import fr.floxiik.skyapi.bungee.skycore.teleportations.TpaPending;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TpAccept extends Command {


    public TpAccept() {
        super("tpaccept");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            if (SkyCoreBungee.getTeleportManager().getTpaPending().containsKey(player.getUniqueId())) {
                TpaPending tpaPending = SkyCoreBungee.getTeleportManager().getTpaPending().get(player.getUniqueId());
                if (tpaPending.isValid()) {
                    TeleportType teleportType = tpaPending.getFrom().toString().equals(player.getUniqueId().toString()) ? TeleportType.TPAHERE : TeleportType.TPA;

                    if(teleportType == TeleportType.TPAHERE) {
                        BungeeCore.getApi().sendPlayerMessage(tpaPending.getFrom(),
                                LangAPI.get().parse(tpaPending.getFrom(), "skyapi.teleport.accept"));
                        BungeeCore.getApi().sendPlayerMessage(tpaPending.getTo(),
                                LangAPI.get().parse(tpaPending.getTo(), "skyapi.teleport.accepted"));
                    } else {
                        BungeeCore.getApi().sendPlayerMessage(tpaPending.getFrom(),
                                LangAPI.get().parse(tpaPending.getFrom(), "skyapi.teleport.accepted"));
                        BungeeCore.getApi().sendPlayerMessage(tpaPending.getTo(),
                                LangAPI.get().parse(tpaPending.getTo(), "skyapi.teleport.accept"));
                    }

                    SkyCoreBungee.getTeleportManager().teleportPlayer(tpaPending.getFrom(), tpaPending.getTo().toString(), "", TeleportType.TPA, 5);
                    SkyCoreBungee.getTeleportManager().removeTpaPending(player.getUniqueId());
                } else {
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.teleport.expired"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.teleport.expired"));
            }
        }
    }
}