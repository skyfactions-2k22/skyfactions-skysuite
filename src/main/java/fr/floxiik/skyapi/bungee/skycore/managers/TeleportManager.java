package fr.floxiik.skyapi.bungee.skycore.managers;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.bungee.skycore.teleportations.TeleportRequest;
import fr.floxiik.skyapi.bungee.skycore.teleportations.TeleportType;
import fr.floxiik.skyapi.bungee.skycore.teleportations.TpaPending;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.UUID;

public class TeleportManager {

    private HashMap<UUID, TeleportRequest> playersCooldowns = new HashMap<>();
    private HashMap<UUID, TpaPending> tpaPending = new HashMap<>();

    public void sendPlayerToPlayerTeleportRequest(UUID from, UUID to) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        ProxiedPlayer playerTo = ProxyServer.getInstance().getPlayer(to);
        if (playerTo != null) {
            out.writeUTF("TeleportToPlayer");
            out.writeUTF(from.toString());
            out.writeUTF(to.toString());
            out.writeUTF(playerTo.getServer().getInfo().getName());
            ProxyServer.getInstance().getServers().values().forEach(server -> server.sendData("skycore:tp", out.toByteArray()));

            ProxyServer.getInstance().getPlayer(from).connect(playerTo.getServer().getInfo());
        } else {
            BungeeCore.getApi().sendPlayerMessage(from, "§cTéléportation annulée, le joueur s'est déconnecté.");
        }
    }

    public void sendTeleportRequest(UUID from, TeleportRequest teleportRequest) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("TeleportRequest");
        out.writeUTF(from.toString());
        out.writeUTF(teleportRequest.getLocation());
        out.writeUTF(teleportRequest.getServer());
        out.writeUTF(teleportRequest.getTeleportType().getType());
        ProxyServer.getInstance().getServers().values().forEach(server -> server.sendData("skycore:tp", out.toByteArray()));

        ProxyServer.getInstance().getPlayer(from).connect(ProxyServer.getInstance().getServerInfo(teleportRequest.getServer()));
    }

    public void finishCooldown(UUID from) {
        if (playersCooldowns.containsKey(from)) {
            TeleportRequest teleportRequest = playersCooldowns.get(from);
            if (TeleportType.isPlayerToPlayer(teleportRequest.getTeleportType())) {
                sendPlayerToPlayerTeleportRequest(from, teleportRequest.getUuidTo());
            } else {
                sendTeleportRequest(from, teleportRequest);
            }
            playersCooldowns.remove(from);
        }
    }

    public void sendCooldownTeleportRequest(UUID from, String to, String server, TeleportType teleportType, int cooldown) {
        playersCooldowns.put(from, new TeleportRequest(from, to, server, teleportType));

        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("TeleportCooldown");
        out.writeUTF(from.toString());
        out.writeInt(cooldown);
        ProxyServer.getInstance().getPlayer(from).getServer().getInfo().sendData("skycore:tp", out.toByteArray());

        BungeeCore.getApi().sendPlayerMessage(from, LangAPI.get().parse(from, "skyapi.teleport.seconds")
                .replace("{amount}", String.valueOf(cooldown)));
    }

    /*public void teleportPlayerToOtherPlayer(UUID from, UUID to, int cooldown) {
        if(BungeeCore.getApi().getServer(from).equals(BungeeCore.getApi().getServer(to))) {
            sendServerTeleportRequest(from, to);
        } else {
            if(cooldown == 0) {
                sendServerTeleportRequest(from, to);
            } else {
                sendCooldownTeleportRequest(from, to, cooldown);
            }
        }
    }*/

    public void teleportPlayer(UUID player, String to, String server, TeleportType teleportType, int cooldown) {
        if (TeleportType.isPlayerToPlayer(teleportType)) {
            if (BungeeCore.getApi().getServer(player).equals(BungeeCore.getApi().getServer(UUID.fromString(to)))) {
                sendPlayerToPlayerTeleportRequest(player, UUID.fromString(to));
            } else {
                sendCooldownTeleportRequest(player, to, server, teleportType, cooldown);
            }
        } else {
            sendCooldownTeleportRequest(player, to, server, teleportType, cooldown);
        }
    }


    public HashMap<UUID, TpaPending> getTpaPending() {
        return tpaPending;
    }

    public void addTpaPending(UUID uuid, TpaPending tpaPending) {
        if (this.tpaPending.containsKey(uuid)) {
            this.tpaPending.replace(uuid, tpaPending);
        } else {
            this.tpaPending.put(uuid, tpaPending);
        }
    }

    public void removeTpaPending(UUID uuid) {
        this.tpaPending.remove(uuid);
    }
}
