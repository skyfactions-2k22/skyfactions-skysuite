package fr.floxiik.skyapi.bungee.skycore.listeners;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import fr.floxiik.skyapi.api.LangAPI;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.concurrent.CompletableFuture;

public class ProxyPingListener implements Listener {

    private final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);

    @EventHandler
    public void onPing(ProxyPingEvent event) {
        ServerPing ping = event.getResponse();
        ServerPing.Players player = ping.getPlayers();

        player.setOnline(BungeeCord.getInstance().getOnlineCount());
        player.setMax(BungeeCord.getInstance().getOnlineCount()+10);

        TextComponent motd = new TextComponent();
        /*try {
            motd.addExtra(LangAPI.get().parse(getCountry(event.getConnection().getAddress().getAddress().getHostName()).get(), "skyapi.proxy.line1"));
            motd.addExtra(LangAPI.get().parse(getCountry(event.getConnection().getAddress().getAddress().getHostName()).get(), "skyapi.proxy.line2"));
            player.setSample(new ServerPing.PlayerInfo[] {
                    new ServerPing.PlayerInfo(String.valueOf(event.getConnection().getAddress().getHostString()), UUID.randomUUID()),
                    new ServerPing.PlayerInfo(String.valueOf(event.getConnection().getAddress().getHostName()), UUID.randomUUID()),
                    new ServerPing.PlayerInfo(String.valueOf(event.getConnection().getAddress().getAddress().getHostName()), UUID.randomUUID()),
                    new ServerPing.PlayerInfo(String.valueOf(event.getConnection().getAddress().getAddress().getCanonicalHostName()), UUID.randomUUID()),
                    new ServerPing.PlayerInfo(String.valueOf(event.getConnection().getVirtualHost().getHostString()), UUID.randomUUID()),
                    new ServerPing.PlayerInfo(String.valueOf(event.getConnection().getVirtualHost().getHostName()), UUID.randomUUID())
            });
        } catch (InterruptedException | ExecutionException e) {
            motd.addExtra(LangAPI.get().parse("en_GB", "skyapi.proxy.line1"));
            motd.addExtra(LangAPI.get().parse("en_GB", "skyapi.proxy.line2"));
        }*/

        motd.addExtra(LangAPI.get().parse("en_GB", "skyapi.proxy.line1"));
        motd.addExtra(LangAPI.get().parse("en_GB", "skyapi.proxy.line2"));

        ping.setDescriptionComponent(motd);
        ping.setPlayers(player);

        event.setResponse(ping);
    }

    public CompletableFuture<String> getCountry(String ip) {
        return CompletableFuture.supplyAsync(() -> {
            URL url = null;
            try {
                url = new URL("http://ip-api.com/json/" + ip);
                BufferedReader stream = new BufferedReader(new InputStreamReader(
                        url.openStream()));
                StringBuilder entirePage = new StringBuilder();
                String inputLine;
                while ((inputLine = stream.readLine()) != null)
                    entirePage.append(inputLine);
                stream.close();
                if (!(entirePage.toString().contains("\"countryCode\":\"")))
                    return null;
                return parseCountryCode(entirePage.toString().split("\"countryCode\":\"")[1].split("\",")[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    public CompletableFuture<String> getCountryCode(String ip) {
        return CompletableFuture.supplyAsync(() -> {
            URL url = null;
            try {
                url = new URL("http://ip-api.com/json/" + ip);
                BufferedReader stream = new BufferedReader(new InputStreamReader(
                        url.openStream()));
                StringBuilder entirePage = new StringBuilder();
                String inputLine;
                while ((inputLine = stream.readLine()) != null)
                    entirePage.append(inputLine);
                stream.close();
                if (!(entirePage.toString().contains("\"countryCode\":\"")))
                    return null;
                return entirePage.toString().split("\"countryCode\":\"")[1].split("\",")[0];
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    public String parseCountryCode(String string) {
        switch (string) {
            case "CA":
            case "US":
            case "GB":
            case "AU":
            case "NZ":
            case "PT":
            case "UD":
                return "en_GB";
            case "FR":
            case "BE":
                return "fr_FR";
            default:
                return "en_GB";
        }
    }
}
