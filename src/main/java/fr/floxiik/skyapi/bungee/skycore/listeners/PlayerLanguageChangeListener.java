package fr.floxiik.skyapi.bungee.skycore.listeners;


import com.rexcantor64.triton.api.events.PlayerChangeLanguageBungeeEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerLanguageChangeListener implements Listener {

    @EventHandler
    public void onLangChange(PlayerChangeLanguageBungeeEvent event) {
        event.getLanguagePlayer().refreshAll();
    }
}
