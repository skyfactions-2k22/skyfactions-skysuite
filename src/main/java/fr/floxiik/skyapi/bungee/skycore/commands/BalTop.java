package fr.floxiik.skyapi.bungee.skycore.commands;

import fr.floxiik.skyapi.bungee.BungeeCore;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BalTop extends Command {


    public BalTop() {
        super("baltop");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            int i = 1;
            player.sendMessage("§8§m--------------§7[ §a" + "BalanceTop" + " §7]§8§m--------------");
            player.sendMessage("");
            player.sendMessage("  §eClassement des joueurs les plus §ariches");
            player.sendMessage("  §edu serveur.");
            player.sendMessage("");
            for(String user : BungeeCore.getApi().getBaltop().keySet()) {
                player.sendMessage(" §7• §e§l" + i + "§7# §6" + user + (user.equals(player.getName()) ? " §7(Vous)" : "") + " §7- §2$§a" + BungeeCore.getApi().getBaltop().get(user));
                i++;
            }
            player.sendMessage("");
            player.sendMessage("  §eVous êtes §a" + BungeeCore.getApi().getBalTopRank(player.getUniqueId()) + "# §edans le classement !");
            StringBuilder string = new StringBuilder("----");
            for(int j = 1; i < "BalanceTop".length(); i++) {
                string.append("-");
            }
            player.sendMessage("§8§m---------------" + string + "--------------");
        }
    }
}