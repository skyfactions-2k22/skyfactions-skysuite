package fr.floxiik.skyapi.bungee.skycore.listeners;

import fr.floxiik.skyapi.bungee.BungeeCore;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class TabCompleteListener implements Listener {

    @EventHandler
    public void onTabComplete(TabCompleteEvent ev) {
        String partialPlayerName = ev.getCursor().toLowerCase();
        int lastSpaceIndex = partialPlayerName.lastIndexOf(' ');
        if (lastSpaceIndex >= 0)
            partialPlayerName = partialPlayerName.substring(lastSpaceIndex + 1);
        for (ProxiedPlayer p : BungeeCore.getInstance().getProxy().getPlayers()) {
            if (p.getName().toLowerCase().startsWith(partialPlayerName))
                ev.getSuggestions().add(p.getName());
        }
    }
}
