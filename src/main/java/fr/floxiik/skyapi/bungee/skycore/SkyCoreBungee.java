package fr.floxiik.skyapi.bungee.skycore;

import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.bungee.skycore.commands.BalTop;
import fr.floxiik.skyapi.bungee.skycore.commands.Broadcast;
import fr.floxiik.skyapi.bungee.skycore.commands.TpAccept;
import fr.floxiik.skyapi.bungee.skycore.listeners.*;
import fr.floxiik.skyapi.bungee.skycore.managers.TeleportManager;
import net.md_5.bungee.api.ProxyServer;

import java.util.concurrent.TimeUnit;

public class SkyCoreBungee {

    private static SkyCoreBungee instance;
    private static TeleportManager teleportManager;

    public void init(BungeeCore bungeeCore) {
        instance = this;
        teleportManager = new TeleportManager();

        bungeeCore.getProxy().registerChannel("skycore:tp");
        bungeeCore.getProxy().getPluginManager().registerListener(bungeeCore, new CoreMessageListener());

        ProxyServer.getInstance().getPluginManager().registerListener(bungeeCore, new PlayerQuitListener());
        ProxyServer.getInstance().getPluginManager().registerListener(bungeeCore, new PlayerJoinListener());
        ProxyServer.getInstance().getPluginManager().registerListener(bungeeCore, new PlayerLanguageChangeListener());
        ProxyServer.getInstance().getPluginManager().registerListener(bungeeCore, new ProxyPingListener());
        ProxyServer.getInstance().getPluginManager().registerListener(bungeeCore, new TabCompleteListener());
        ProxyServer.getInstance().getPluginManager().registerCommand(bungeeCore, new TpAccept());
        ProxyServer.getInstance().getPluginManager().registerCommand(bungeeCore, new BalTop());
        ProxyServer.getInstance().getPluginManager().registerCommand(bungeeCore, new Broadcast());

        ProxyServer.getInstance().getScheduler().schedule(bungeeCore, () -> {
            BungeeCore.getApi().refreshBalTop();
        }, 0, 1, TimeUnit.HOURS);
    }

    public void disable(BungeeCore bungeeCore) {
    }

    public static SkyCoreBungee getInstance() {
        return instance;
    }

    public static TeleportManager getTeleportManager() {
        return teleportManager;
    }
}
