package fr.floxiik.skyapi.bungee.skycore.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.api.SkygeeApi;
import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.bungee.skycore.SkyCoreBungee;
import fr.floxiik.skyapi.bungee.skycore.teleportations.TeleportType;
import fr.floxiik.skyapi.bungee.skycore.teleportations.TpaPending;
import fr.floxiik.skyapi.bungee.skycrew.SkyCrewBungee;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CoreMessageListener implements Listener {

    @EventHandler
    public void onPluginMessage(PluginMessageEvent event) {
        if (event.getTag().equals("skycore:tp")) {
            final ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
            final String sub = in.readUTF();
            if (sub.equals("TeleportToPlayer")) {
                final String playerWhoTeleport = in.readUTF();
                final String playerWhereTeleport = in.readUTF();
                final String serverToTeleport = in.readUTF();

                ByteArrayDataOutput out = ByteStreams.newDataOutput();

                out.writeUTF("TeleportToPlayer");
                out.writeUTF(playerWhoTeleport);
                out.writeUTF(playerWhereTeleport);
                out.writeUTF(serverToTeleport);

                ProxiedPlayer player1 = ProxyServer.getInstance().getPlayer(UUID.fromString(playerWhoTeleport));
                ProxiedPlayer player2 = ProxyServer.getInstance().getPlayer(UUID.fromString(playerWhereTeleport));

                ProxyServer.getInstance().getServers().values().forEach(server -> server.sendData("skycore:tp", out.toByteArray()));

                ProxyServer.getInstance().getScheduler().schedule(BungeeCore.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        if (!player1.getServer().getInfo().getName().equals(player2.getServer().getInfo().getName())) {
                            player1.connect(player2.getServer().getInfo());
                        }
                    }
                }, 3, TimeUnit.SECONDS);
            }
            if (sub.equals("TeleportToPlayerRequest")) {
                final String playerWhoTeleport = in.readUTF();
                final String playerWhereTeleport = in.readUTF();
                final String tpa = in.readUTF();

                ProxiedPlayer player1 = ProxyServer.getInstance().getPlayer(UUID.fromString(playerWhoTeleport));
                ProxiedPlayer player2 = ProxyServer.getInstance().getPlayer(UUID.fromString(playerWhereTeleport));

                player1.sendMessage("§eDemande de téléportation §benvoyée §e!");

                if (player2 != null && player1 != null) {
                    player2.sendMessage("");
                    if(tpa.equals("tpahere")) {
                        SkyCoreBungee.getTeleportManager().addTpaPending(UUID.fromString(playerWhereTeleport), new TpaPending(UUID.fromString(playerWhereTeleport), UUID.fromString(playerWhoTeleport), false));
                        player2.sendMessage("§eLe joueur §b" + player1.getName() + " §eveut vous téléporter à lui !");
                    } else {
                        SkyCoreBungee.getTeleportManager().addTpaPending(UUID.fromString(playerWhereTeleport), new TpaPending(UUID.fromString(playerWhoTeleport), UUID.fromString(playerWhereTeleport), true));
                        player2.sendMessage("§eLe joueur §b" + player1.getName() + " §eveut se téléporter à vous !");
                    }
                    player2.sendMessage("");
                    TextComponent base = new TextComponent("  " + LangAPI.get().parse(player2.getUniqueId(), "skyapi.json.doyou") + " ");
                    TextComponent accept = new TextComponent(LangAPI.get().parse(player2.getUniqueId(), "skyapi.json.accept"));
                    accept.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(LangAPI.get().parse(player2.getUniqueId(), "skyapi.json.accept.hover")).create()));
                    accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpaccept"));
                    TextComponent decline = new TextComponent(LangAPI.get().parse(player2.getUniqueId(), "skyapi.json.decline"));
                    decline.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(LangAPI.get().parse(player2.getUniqueId(), "skyapi.json.decline.hover")).create()));
                    decline.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpdecline"));
                    TextComponent where = new TextComponent(LangAPI.get().parse(player2.getUniqueId(), "skyapi.json.or"));
                    base.addExtra(accept);
                    base.addExtra(where);
                    base.addExtra(decline);
                    base.addExtra(" " + LangAPI.get().parse(player2.getUniqueId(), "skyapi.json.question"));
                    player2.sendMessage(base);
                    player2.sendMessage("");
                }
            }
            if (sub.equals("TeleportRequest")) {
                final String playerWhoTeleport = in.readUTF();
                final String locationToTeleport = in.readUTF();
                final String serverToTeleport = in.readUTF();
                final String teleportType = in.readUTF();
                final int cooldown = in.readInt();

                TeleportType teleportationType;
                if(teleportType.equals("home_warp")) {
                    teleportationType = TeleportType.HOME_WARP;
                } else if(teleportType.equals("home")) {
                    teleportationType = TeleportType.HOME;
                    if(SkyCrewBungee.getVersusManager().inVersus.contains(SkygeeApi.get().getCrew(UUID.fromString(playerWhoTeleport)).getId())) {

                    }
                } else if(teleportType.equals("location")) {
                    teleportationType = TeleportType.LOCATION;
                } else {
                    teleportationType = TeleportType.TP;
                }


                SkyCoreBungee.getTeleportManager().teleportPlayer(UUID.fromString(playerWhoTeleport), locationToTeleport, serverToTeleport, teleportationType, cooldown);

            }
            if (sub.equals("CooldownFinish")) {
                final UUID uuid = UUID.fromString(in.readUTF());
                SkyCoreBungee.getTeleportManager().finishCooldown(uuid);
            }
            if (sub.equals("RefreshAccount")) {
                final UUID account = UUID.fromString(in.readUTF());
                BungeeCore.getApi().refreshAccount(account);
            }
        }
    }
}
