package fr.floxiik.skyapi.bungee.skycore.teleportations;

public enum TeleportType {

    TPA("tpa"),
    TPAHERE("tpahere"),
    TP("tp"),
    TPHERE("tphere"),
    HOME("home"),
    HOME_WARP("home_warp"),
    LOCATION("location");

    private String type;

    TeleportType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static boolean isPlayerToPlayer(TeleportType teleportType) {
        return teleportType == TeleportType.TP || teleportType == TeleportType.TPA || teleportType == TeleportType.TPAHERE || teleportType == TeleportType.TPHERE;
    }
}
