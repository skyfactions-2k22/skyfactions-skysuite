package fr.floxiik.skyapi.bungee.skycore.listeners;

import fr.floxiik.skyapi.api.SkygeeApi;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PostLoginEvent event) {
        UUID playerUUID = event.getPlayer().getUniqueId();
        //BungeeCore.getApi().getAccount(playerUUID);
        SkygeeApi.get().checkCrewServer(playerUUID);
    }
}
