package fr.floxiik.skyapi.bungee.skycore.teleportations;

import fr.floxiik.skyapi.bungee.BungeeCore;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class TpaPending {

    private ScheduledTask task;
    boolean valid = true;
    int timer = 60;
    final UUID from;
    final UUID to;
    final boolean tpa;

    public TpaPending(UUID from, UUID to, boolean tpa) {
        this.from = from;
        this.to = to;
        this.tpa = tpa;
        this.startTimer();
    }

    public void startTimer() {
        task = BungeeCore.getInstance().getProxy().getScheduler().schedule(BungeeCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                timer--;
                if(timer < 0) {
                    valid = false;
                    task.cancel();
                }
            }
        }, 1, 60, TimeUnit.SECONDS);
    }

    public boolean isValid() {
        return this.valid;
    }

    public UUID getFrom() {
        return from;
    }

    public UUID getTo() {
        return to;
    }

    public boolean isTpa() {
        return tpa;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
