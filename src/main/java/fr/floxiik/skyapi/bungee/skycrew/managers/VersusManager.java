package fr.floxiik.skyapi.bungee.skycrew.managers;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Versus;
import fr.floxiik.skyapi.data.providers.bungee.BungeeCrewProvider;
import net.md_5.bungee.api.ProxyServer;
import org.bukkit.Bukkit;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class VersusManager {

    private static final CloudNetDriver DRIVER = CloudNetDriver.getInstance();
    public ArrayList<Integer> inVersus = new ArrayList<>();

    public void versusRequest(Crew crew) {
        int attacker = crew.getId();
        BungeeCrewProvider bungeeCrewProvider = new BungeeCrewProvider(null, crew.getId());
        try {
            int victim = bungeeCrewProvider.getRandomFromDb(crew.getPoints() - 2000, crew.getPoints() + 2000, attacker).get();
            if(victim == 0) {
                ProxyServer.getInstance().broadcast("pas de versus");
                //Send error message to faction
            } else {
                Versus versus = new Versus(attacker, victim);
                launchVersus(versus);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void launchVersus(Versus versus) {
        ProxyServer.getInstance().broadcast("launchVersus");
        BungeeCrewProvider bungeeCrewProvider = new BungeeCrewProvider(null, versus.getId());
        try {
            Versus test = bungeeCrewProvider.registerVersus(versus).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    public void checkVersus() {
        BungeeCrewProvider bungeeCrewProvider = new BungeeCrewProvider(null, 0);
        BungeeCore.getInstance().getProxy().getScheduler().schedule(BungeeCore.getInstance(), () -> {
            ProxyServer.getInstance().broadcast("checkVersus");
            try {
                for (Versus versus : bungeeCrewProvider.getAllVersus().get()) {
                    ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(versus.getServerId());
                    if (!service.isConnected()) {
                        service.provider().start();
                    }
                    if (new Date(Instant.now().toEpochMilli()).after(new Date(versus.getStartDate().getTime() + (60 * 60 * 1000)))) {
                        finishVersus(versus);
                    } else if (versus.getStartDate().before(new Date(Instant.now().toEpochMilli()))) {
                        startVersus(versus);
                    } else {
                        // Envoyer message (démarrage dans x minutes)
                    }
                }
            } catch(InterruptedException | ExecutionException e){
            e.printStackTrace();
        }
    },1,1,TimeUnit.MINUTES);
}

    public void startVersus(Versus versus) {
        ProxyServer.getInstance().broadcast("startVersus");
        if(versus.getWinner() == 0) return;

        ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(versus.getServerId());
        if (service.isConnected()) {
            inVersus.add(versus.getAttacker());
            inVersus.add(versus.getVictim());
        }
    }

    public void finishVersus(Versus versus) {
        ProxyServer.getInstance().broadcast("finishVersus");
    }
}
