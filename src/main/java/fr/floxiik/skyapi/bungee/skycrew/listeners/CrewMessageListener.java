package fr.floxiik.skyapi.bungee.skycrew.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.bungee.skycrew.CrewInvit;
import fr.floxiik.skyapi.bungee.skycrew.SkyCrewBungee;
import fr.floxiik.skyapi.data.Crew;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CrewMessageListener implements Listener {

    @EventHandler
    public void onPluginMessage(PluginMessageEvent event) {
        if(event.getTag().equals("skycrew:crewchannel")) {
            final ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
            final String sub = in.readUTF();
            if(sub.equals("InviteMember")) {
                final String nameToInvite = in.readUTF();
                final String nameWhoInvite = in.readUTF();

                final ProxiedPlayer invited = ProxyServer.getInstance().getPlayer(nameToInvite);
                final ProxiedPlayer inviter = ProxyServer.getInstance().getPlayer(nameWhoInvite);
                final Crew crew = BungeeCore.getApi().getCrew(inviter.getUniqueId());

                if(invited == null) {
                    inviter.sendMessage(new TextComponent(LangAPI.get().parse(inviter.getUniqueId(), "skyapi.playernotfound")));
                    return;
                }

                if(BungeeCore.getApi().hasCrew(invited.getUniqueId())) {
                    inviter.sendMessage(new TextComponent(LangAPI.get().parse(inviter.getUniqueId(), "skycrew.alreadyinfac")));
                    return;
                }

                inviter.sendMessage(new TextComponent(LangAPI.get().parse(inviter.getUniqueId(), "skycrew.invite")
                    .replace("{player}", invited.getName())));
                invited.sendMessage(new TextComponent(LangAPI.get().parse(invited.getUniqueId(), "skycrew.invite.receive")
                    .replace("{player}", inviter.getName())
                    .replace("{fac}", crew.getName())));

                invited.sendMessage(new TextComponent(""));
                TextComponent base = new TextComponent("  " + LangAPI.get().parse(invited.getUniqueId(), "skyapi.json.doyou") + " ");
                TextComponent accept = new TextComponent(LangAPI.get().parse(invited.getUniqueId(), "skyapi.json.accept"));
                accept.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(LangAPI.get().parse(invited.getUniqueId(), "skyapi.json.accept.hover")).create()));
                accept.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/finvite accept" ) );
                TextComponent decline = new TextComponent(LangAPI.get().parse(invited.getUniqueId(), "skyapi.json.decline"));
                decline.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(LangAPI.get().parse(invited.getUniqueId(), "skyapi.json.decline.hover")).create()));
                decline.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/finvite decline" ) );
                TextComponent where = new TextComponent(LangAPI.get().parse(invited.getUniqueId(), "skyapi.json.or"));
                base.addExtra(accept);
                base.addExtra(where);
                base.addExtra(decline);
                base.addExtra(" " + LangAPI.get().parse(invited.getUniqueId(), "skyapi.json.question"));
                invited.sendMessage(base);
                invited.sendMessage(new TextComponent(""));

                SkyCrewBungee.getInstance().addCrewInvite(invited.getUniqueId(), new CrewInvit(crew.getId(), inviter.getUniqueId()));
            } else if(sub.equals("TeleportRequest")) {
                final String playerWhoTeleport = in.readUTF();
                final String locationToTeleport = in.readUTF();
                final String serverToTeleport = in.readUTF();

                ByteArrayDataOutput out = ByteStreams.newDataOutput();

                out.writeUTF("TeleportRequest");
                out.writeUTF(playerWhoTeleport);
                out.writeUTF(locationToTeleport);
                out.writeUTF(serverToTeleport);

                ProxyServer.getInstance().getServers().values().forEach(server -> server.sendData("skycrew:crewchannel", out.toByteArray()));

                ProxyServer.getInstance().getScheduler().schedule(BungeeCore.getInstance(), () -> ProxyServer.getInstance().getPlayer(UUID.fromString(playerWhoTeleport)).connect(ProxyServer.getInstance().getServerInfo(serverToTeleport)), 3, TimeUnit.SECONDS);
            }
        }
    }
}
