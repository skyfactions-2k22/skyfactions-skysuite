package fr.floxiik.skyapi.bungee.skycrew.commands;

import fr.floxiik.skyapi.api.SkygeeApi;
import fr.floxiik.skyapi.bungee.skycrew.SkyCrewBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class RaidCommand extends Command {

    public RaidCommand() {
        super("raidtest");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (commandSender instanceof ProxiedPlayer) {
            if(SkygeeApi.get().hasCrew(((ProxiedPlayer) commandSender).getUniqueId())) {
                SkyCrewBungee.getVersusManager().versusRequest(SkygeeApi.get().getCrew(((ProxiedPlayer) commandSender).getUniqueId()));
            } else {
                commandSender.sendMessage("Tu n'as pas de faction bg.");
            }
        }
    }
}
