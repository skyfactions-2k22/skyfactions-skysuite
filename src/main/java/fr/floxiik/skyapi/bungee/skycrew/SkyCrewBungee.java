package fr.floxiik.skyapi.bungee.skycrew;

import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.bungee.skycore.listeners.PlayerJoinListener;
import fr.floxiik.skyapi.bungee.skycore.listeners.PlayerQuitListener;
import fr.floxiik.skyapi.bungee.skycrew.commands.InviteCommand;
import fr.floxiik.skyapi.bungee.skycrew.commands.RaidCommand;
import fr.floxiik.skyapi.bungee.skycrew.listeners.CrewMessageListener;
import fr.floxiik.skyapi.bungee.skycrew.managers.VersusManager;
import net.md_5.bungee.api.ProxyServer;

import java.util.HashMap;
import java.util.UUID;

public class SkyCrewBungee {

    public static SkyCrewBungee instance;
    public static VersusManager versusManager;
    public HashMap<UUID, CrewInvit> crewInvites = new HashMap<>();

    public void init(BungeeCore bungeeCore) {
        instance = this;
        versusManager = new VersusManager();

        bungeeCore.getProxy().registerChannel("skycrew:crewchannel");
        bungeeCore.getProxy().getPluginManager().registerListener(bungeeCore, new CrewMessageListener());
        ProxyServer.getInstance().getPluginManager().registerListener(bungeeCore, new PlayerQuitListener());
        ProxyServer.getInstance().getPluginManager().registerListener(bungeeCore, new PlayerJoinListener());
        ProxyServer.getInstance().getPluginManager().registerCommand(bungeeCore, new InviteCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(bungeeCore, new RaidCommand());

        versusManager.checkVersus();
    }

    public void disable(BungeeCore bungeeCore) {
    }

    public static SkyCrewBungee getInstance() {
        return instance;
    }

    public static VersusManager getVersusManager() {
        return versusManager;
    }

    public HashMap<UUID, CrewInvit> getCrewInvites() {
        return crewInvites;
    }

    public void addCrewInvite(UUID uuid, CrewInvit crewInvit) {
        if(this.crewInvites.containsKey(uuid)) {
            this.crewInvites.replace(uuid, crewInvit);
        } else {
            this.crewInvites.put(uuid, crewInvit);
        }
    }

    public void removeCrewInvites(UUID uuid) {
        this.crewInvites.remove(uuid);
    }
}
