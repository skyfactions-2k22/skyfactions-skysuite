package fr.floxiik.skyapi.bungee.skycrew;

import fr.floxiik.skyapi.bungee.BungeeCore;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CrewInvit {

    private ScheduledTask task;
    boolean valid = true;
    int timer = 60;
    final int crewId;
    final UUID inviter;

    public CrewInvit(int crewId, UUID inviter) {
        this.crewId = crewId;
        this.inviter = inviter;
        this.startTimer();
    }

    public void startTimer() {
        task = BungeeCore.getInstance().getProxy().getScheduler().schedule(BungeeCore.getInstance(), () -> {
            timer--;
            if(timer < 0) {
                valid = false;
                task.cancel();
            }
        }, 1, 60, TimeUnit.SECONDS);
    }

    public boolean isValid() {
        return this.valid;
    }

    public int getCrewId() {
        return crewId;
    }

    public UUID getInviter() {
        return inviter;
    }
}
