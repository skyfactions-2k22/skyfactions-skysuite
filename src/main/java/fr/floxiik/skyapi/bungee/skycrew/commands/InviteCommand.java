package fr.floxiik.skyapi.bungee.skycrew.commands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.bungee.skycrew.CrewInvit;
import fr.floxiik.skyapi.bungee.skycrew.SkyCrewBungee;
import fr.floxiik.skyapi.data.Crew;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class InviteCommand extends Command {

    public InviteCommand() {
        super("finvite");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            if (strings.length > 0) {
                if (strings[0].equals("accept")) {
                    if (SkyCrewBungee.getInstance().getCrewInvites().containsKey(player.getUniqueId())) {
                        CrewInvit crewInvit = SkyCrewBungee.getInstance().getCrewInvites().get(player.getUniqueId());
                        if(crewInvit.isValid()) {
                            Crew crew = BungeeCore.getApi().getCrew(crewInvit.getInviter());
                            SkyCrewBungee.getInstance().removeCrewInvites(player.getUniqueId());
                            BungeeCore.getApi().addInCrew(player.getUniqueId(), crewInvit.getCrewId());
                            crew.getMembers().keySet().forEach((member) -> {
                                BungeeCore.getApi().sendPlayerMessage(member, LangAPI.get().parse(member, "skycrew.invite.join")
                                    .replace("{player}", player.getName()));
                            });
                            BungeeCore.getApi().sendPlayerMessage(crewInvit.getInviter(), LangAPI.get().parse(crewInvit.getInviter(), "skycrew.invite.join")
                                    .replace("{player}", player.getName()));
                            player.sendMessage(new TextComponent(LangAPI.get().parse(player.getUniqueId(), "skycrew.invite.accept")
                                    .replace("{fac}", crew.getName())));
                        } else {
                            player.sendMessage(new TextComponent(LangAPI.get().parse(player.getUniqueId(), "skycrew.invite.expired")));
                        }
                    }
                } else if (strings[0].equals("decline")) {
                    if (SkyCrewBungee.getInstance().getCrewInvites().containsKey(player.getUniqueId())) {
                        CrewInvit crewInvit = SkyCrewBungee.getInstance().getCrewInvites().get(player.getUniqueId());
                        if(crewInvit.isValid()) {
                            Crew crew = BungeeCore.getApi().getCrew(crewInvit.getInviter());
                            player.sendMessage(new TextComponent(LangAPI.get().parse(player.getUniqueId(), "skycrew.invite.decline")
                                    .replace("{fac}", crew.getName())));
                        } else {
                            player.sendMessage(new TextComponent(LangAPI.get().parse(player.getUniqueId(), "skycrew.invite.expired")));
                        }
                    }
                }
            }
        }
    }
}
