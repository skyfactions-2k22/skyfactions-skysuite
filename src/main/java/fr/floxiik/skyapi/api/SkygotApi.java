package fr.floxiik.skyapi.api;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.*;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycore.utils.RandomGenerator;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import fr.floxiik.skyapi.data.Versus;
import fr.floxiik.skyapi.data.providers.spigot.SpigotAccountProvider;
import fr.floxiik.skyapi.data.providers.spigot.SpigotCrewProvider;

import java.io.File;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class SkygotApi {

    private static SkygotApi api;
    private static final CloudNetDriver DRIVER = CloudNetDriver.getInstance();
    private final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);

    public SkygotApi() {
        api = this;
    }

    public static SkygotApi get() {
        return api == null ? new SkygotApi() : api;
    }

    public void saveAccount(UUID uuid, boolean redis, boolean mysql) {
        SpigotAccountProvider spigotAccountProvider = new SpigotAccountProvider(uuid);
        Account account = spigotAccountProvider.getAccount();
        if(redis)
            spigotAccountProvider.sendRedis(account);
        if(mysql) {
            try {
                spigotAccountProvider.sendMySQL(account);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }

    public void setAccount(Account account, boolean redis, boolean mysql) {
        SpigotAccountProvider spigotAccountProvider = new SpigotAccountProvider(account.getUuid());
        if(redis)
            spigotAccountProvider.sendRedis(account);
        if(mysql) {
            try {
                spigotAccountProvider.sendMySQL(account);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }

    public Account getAccount(UUID uuid) {
        return SkyCore.getPlayerManager().getAccount(uuid);
    }

    public Account getAccount(String username) {
        return SkyCore.getPlayerManager().getAccount(username);
    }

    public void saveCrew(UUID uuid) {
        try {
            new SpigotCrewProvider(uuid, getAccount(uuid).getCrew()).sendCrewToMySQL();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Crew getCrew(UUID uuid) {
        return new SpigotCrewProvider(uuid, getAccount(uuid).getCrew()).getCrew();
    }

    public Crew getCrew(Account account) {
        return new SpigotCrewProvider(account.getUuid(), account.getCrew()).getCrew();
    }

    public Crew getCrew(int crewId) {
        return new SpigotCrewProvider(null, crewId).getCrew();
    }

    public File getSchematic(Crew crew) {
        try {
            return new SpigotCrewProvider(crew.getOwner(), crew.getId()).getSchematic().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateSchematic(Crew crew, File file) {
        try {
            new SpigotCrewProvider(crew.getOwner(), crew.getId()).updateSchematic(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean hasCrew(UUID uuid) {
        return getAccount(uuid).getCrew() != 0;
    }

    public boolean hasCrew(Account account) {
        return account.getCrew() != 0;
    }

    public boolean isOwner(UUID uuid) {
        UUID owner = new SpigotCrewProvider(uuid, getAccount(uuid).getCrew()).getCrew().getOwner();
        return owner.toString().equals(uuid.toString());
    }

    public boolean isInCrew(UUID uuid, int id) {
        int crewId = new SpigotCrewProvider(uuid, getAccount(uuid).getCrew()).getCrew().getId();
        return crewId == id;
    }

    public void setRank(UUID uuid, int id, String rank) {
        new SpigotCrewProvider(uuid, id).promote(rank);
    }

    public void addInCrew(UUID uuid, int id) {
        new SpigotCrewProvider(uuid, id).addInCrew();
    }

    public void removeInCrew(UUID uuid, int id) {
        new SpigotCrewProvider(uuid, id).removeInCrew();
    }

    public void setHome(UUID uuid, int id, String name, Home location) {
        new SpigotCrewProvider(uuid, id).setHome(name, location);
    }

    public void delHome(UUID uuid, int id, String name) {
        new SpigotCrewProvider(uuid, id).delHome(name);
    }

    public Crew createCrew(UUID owner, String name) {
        Crew crew = null;
        Account account = null;
        try {
            crew = new SpigotCrewProvider(owner, 0).createNewCrew(owner, name).get();
            account = getAccount(owner);
            account.setCrew(crew.getId());
            setAccount(account, SpigotCore.getApi().isOnline(owner), true);
            return crew;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteCrew(UUID owner) {
        new SpigotCrewProvider(owner, getAccount(owner).getCrew()).deleteCrew();
    }

    public void checkCrewServer(UUID player) {
        if (hasCrew(player)) {
            Crew crew = getCrew(player);
            ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(crew.getServerId());
            if (service == null) {
                //createVersusServer(crew.getId());
            } else {
                service.provider().start();
            }
        }
    }

    public void sendToHome(UUID player) {
        Crew crew = getCrew(player);
        playerManager.getPlayerExecutor(player).connect(crew.getServerId());
    }

    public void sendToServerRandom(UUID player, String serverGroup) {
        List<String> servers = new ArrayList<>();
        for (ServiceInfoSnapshot serviceInfoSnapshot : CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices(serverGroup)) {
            servers.add(serviceInfoSnapshot.getServiceId().getName());
        }
        playerManager.getPlayerExecutor(player).connect(servers.get(RandomGenerator.between(0, servers.size() - 1)));
    }

    public boolean isOnline(UUID uuid) {
        ICloudPlayer cloudPlayer = this.playerManager.getOnlinePlayer(uuid);
        return cloudPlayer != null;
    }

    public String getServer(UUID uuid) {
        ICloudPlayer cloudPlayer = this.playerManager.getOnlinePlayer(uuid);
        if (cloudPlayer != null) {
            return cloudPlayer.getConnectedService().getServerName();
        } else {
            return null;
        }
    }

    public void sendPlayerMessage(UUID uuid, String message) {
        ICloudPlayer cloudPlayer = this.playerManager.getOnlinePlayer(uuid);
        if (cloudPlayer != null) {
            cloudPlayer.getPlayerExecutor().sendChatMessage(message);
        }
    }

    private void startCrewServer(String serverId) {
        DRIVER.getNodeInfoProvider().sendCommandLineAsync("ser " + serverId + " start");
    }

    public void stopCrewServer(String serverId) {
        ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(serverId);
        if (service != null) {
            service.provider().stop();
        }
    }

    public void deleteCrewServer(String serverId) {
        ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(serverId);
        if (service != null) {
            service.provider().stopAsync();
        }
    }

    public ServiceId createCrewServer(int crewId) {
        ServiceTask serviceTask = new ServiceTask();
        serviceTask.setStaticServices(true);
        serviceTask.setName(crewId + "");
        serviceTask.setRuntime("jvm");
        serviceTask.setStartPort(45000);
        serviceTask.setAutoDeleteOnStop(true);
        serviceTask.setDeletedFilesAfterStop(Arrays.asList("/plugins"));
        serviceTask.setGroups(Arrays.asList("Crew"));
        serviceTask.setMinServiceCount(0);
        serviceTask.setMaintenance(false);
        serviceTask.setIncludes(new ArrayList<>());
        serviceTask.setDeployments(new ArrayList<>());
        serviceTask.setAssociatedNodes(new ArrayList<>());
        serviceTask.setProcessConfiguration(new ProcessConfiguration(
                ServiceEnvironmentType.MINECRAFT_SERVER,
                2048,
                new ArrayList<>()
        ));
        serviceTask.setTemplates(Arrays.asList(new ServiceTemplate(
                "Crew",
                "default",
                "local",
                true
        ), new ServiceTemplate("Crew", "plugins", "local", true)));
        ServiceInfoSnapshot serviceInfoSnapshot = DRIVER.getCloudServiceFactory().createCloudService(serviceTask);

        if (serviceInfoSnapshot != null) {
            serviceInfoSnapshot.provider().start();
            return serviceInfoSnapshot.getServiceId();
        }
        return null;
    }

    public ServiceId createVersusServer(Versus versus) {
        ServiceTask serviceTask = new ServiceTask();
        serviceTask.setStaticServices(true);
        serviceTask.setName(versus.getServerId() + "vs");
        serviceTask.setRuntime("jvm");
        serviceTask.setStartPort(50000);
        serviceTask.setAutoDeleteOnStop(true);
        serviceTask.setDeletedFilesAfterStop(Collections.singletonList("/plugins"));
        serviceTask.setGroups(Arrays.asList("Versus"));
        serviceTask.setMinServiceCount(0);
        serviceTask.setMaintenance(false);
        serviceTask.setIncludes(new ArrayList<>());
        serviceTask.setDeployments(new ArrayList<>());
        serviceTask.setAssociatedNodes(new ArrayList<>());
        serviceTask.setProcessConfiguration(new ProcessConfiguration(
                ServiceEnvironmentType.MINECRAFT_SERVER,
                512,
                new ArrayList<>()
        ));
        serviceTask.setTemplates(Arrays.asList(new ServiceTemplate(
                "Versus",
                "default",
                "local",
                true
        ), new ServiceTemplate("Versus", "plugins", "local", true)));
        ServiceInfoSnapshot serviceInfoSnapshot = DRIVER.getCloudServiceFactory().createCloudService(serviceTask);

        if (serviceInfoSnapshot != null) {
            serviceInfoSnapshot.provider().start();
            return serviceInfoSnapshot.getServiceId();
        }
        return null;
    }
}
