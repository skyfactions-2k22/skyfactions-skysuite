package fr.floxiik.skyapi.api;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.*;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import fr.floxiik.skyapi.bukkit.skycore.utils.RandomGenerator;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import fr.floxiik.skyapi.data.Versus;
import fr.floxiik.skyapi.data.providers.bungee.BungeeAccountProvider;
import fr.floxiik.skyapi.data.providers.bungee.BungeeCrewProvider;
import net.md_5.bungee.api.ProxyServer;

import java.io.File;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class SkygeeApi {

    private static SkygeeApi api;
    private static final CloudNetDriver DRIVER = CloudNetDriver.getInstance();
    private final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
    private LinkedHashMap<String, Integer> baltop = new LinkedHashMap<>();
    private HashMap<UUID, Long> timePlayed = new HashMap<>();

    public SkygeeApi() {
        api = this;
    }

    public static SkygeeApi get() {
        return api == null ? new SkygeeApi() : api;
    }

    public Account getAccount(UUID uuid) {
        return new BungeeAccountProvider(uuid).getAccount();
    }

    public void updateAccount(Account account, boolean connected) {
        try {
            BungeeAccountProvider accountProvider = new BungeeAccountProvider(account.getUuid());
            if (connected) {
                accountProvider.sendRedis(account);
                refreshAccount(account.getUuid());
            } else {
                account.setTimePlayed(account.getTimePlayed() + (Instant.now().getEpochSecond() - timePlayed.get(account.getUuid())));
                accountProvider.sendRedis(account);
                accountProvider.sendMySQL(account);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    public void refreshAccount(UUID uuid) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();

        out.writeUTF("RefreshAccount");
        out.writeUTF(uuid.toString());

        ICloudPlayer player = this.playerManager.getOnlinePlayer(uuid);
        if(player != null) {
            ProxyServer.getInstance().getServerInfo(player.getConnectedService().getServerName()).sendData("skycore:tp", out.toByteArray());
        }
    }

    public void updateAccount(UUID uuid, boolean connected) {
        try {
            BungeeAccountProvider accountProvider = new BungeeAccountProvider(uuid);
            Account account = accountProvider.getAccount();
            if (connected) {
                accountProvider.sendRedis(account);
                refreshAccount(uuid);
            } else {
                accountProvider.sendRedis(account);
                accountProvider.sendMySQL(account);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void saveAccount(UUID uuid) {
        try {
            new BungeeAccountProvider(uuid).sendMySQL(getAccount(uuid));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void refreshBalTop() {
        try {
            baltop.clear();
            baltop = new BungeeAccountProvider(null).getBalTop10().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public int getBalTopRank(UUID uuid) {
        try {
            return new BungeeAccountProvider(uuid).getBalTopRank().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public LinkedHashMap<String, Integer> getBaltop() {
        return baltop;
    }

    public void saveCrew(UUID uuid) {
        new BungeeCrewProvider(uuid, getAccount(uuid).getCrew()).sendCrewToMySQL();
    }

    public Crew getCrew(UUID uuid) {
        return new BungeeCrewProvider(uuid, getAccount(uuid).getCrew()).getCrew();
    }

    public File getSchematic(Crew crew) {
        try {
            return new BungeeCrewProvider(crew.getOwner(), crew.getId()).getSchematic().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateSchematic(Crew crew, File file) {
        try {
            new BungeeCrewProvider(crew.getOwner(), crew.getId()).updateSchematic(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean hasCrew(UUID uuid) {
        return getAccount(uuid).getCrew() != 0;
    }

    public boolean isOwner(UUID uuid) {
        UUID owner = new BungeeCrewProvider(uuid, getAccount(uuid).getCrew()).getCrew().getOwner();
        return owner.toString().equals(uuid.toString());
    }

    public boolean isInCrew(UUID uuid, int id) {
        int crewId = new BungeeCrewProvider(uuid, getAccount(uuid).getCrew()).getCrew().getId();
        return crewId == id;
    }

    public void setRank(UUID uuid, int id, String rank) {
        new BungeeCrewProvider(uuid, id).promote(rank);
    }

    public void addInCrew(UUID uuid, int id) {
        new BungeeCrewProvider(uuid, id).addInCrew();
    }

    public void removeInCrew(UUID uuid, int id) {
        new BungeeCrewProvider(uuid, id).removeInCrew();
    }

    public void setHome(UUID uuid, int id, String name, Home location) {
        new BungeeCrewProvider(uuid, id).setHome(name, location);
    }

    public void delHome(UUID uuid, int id, String name) {
        new BungeeCrewProvider(uuid, id).delHome(name);
    }

    public Crew createCrew(UUID owner, String name) {
        Crew crew = null;
        Account account = null;
        try {
            crew = new BungeeCrewProvider(owner, 0).createNewCrew(owner, name).get();
            account = getAccount(owner);
            account.setCrew(crew.getId());
            new BungeeAccountProvider(owner).sendRedis(account);
        } catch (SQLException | ExecutionException | InterruptedException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public void deleteCrew(UUID owner) {
        new BungeeCrewProvider(owner, getAccount(owner).getCrew()).deleteCrew();
    }

    public void checkCrewServer(UUID player) {
        if (hasCrew(player)) {
            Crew crew = getCrew(player);
            ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(crew.getServerId());
            if (service == null) {
                createCrewServer(crew.getId());
            } else {
                service.provider().start();
            }
        }
    }

    public void sendToHome(UUID player) {
        Crew crew = getCrew(player);
        playerManager.getPlayerExecutor(player).connect(crew.getServerId());
    }

    public void sendToServerRandom(UUID player, String serverGroup) {
        List<String> servers = new ArrayList<>();
        for (ServiceInfoSnapshot serviceInfoSnapshot : CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices(serverGroup)) {
            servers.add(serviceInfoSnapshot.getServiceId().getName());
        }
        playerManager.getPlayerExecutor(player).connect(servers.get(RandomGenerator.between(0, servers.size() - 1)));
    }

    public boolean isOnline(UUID uuid) {
        ICloudPlayer cloudPlayer = this.playerManager.getOnlinePlayer(uuid);
        return cloudPlayer != null;
    }

    public String getServer(UUID uuid) {
        ICloudPlayer cloudPlayer = this.playerManager.getOnlinePlayer(uuid);
        if (cloudPlayer != null) {
            return cloudPlayer.getConnectedService().getServerName();
        } else {
            return null;
        }
    }

    public void sendPlayerMessage(UUID uuid, String message) {
        ICloudPlayer cloudPlayer = this.playerManager.getOnlinePlayer(uuid);
        if (cloudPlayer != null) {
            cloudPlayer.getPlayerExecutor().sendChatMessage(message);
        }
    }

    private void startCrewServer(String serverId) {
        DRIVER.getNodeInfoProvider().sendCommandLineAsync("ser " + serverId + " start");
    }

    public void stopCrewServer(String serverId) {
        ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(serverId);
        if (service != null) {
            service.provider().stop();
        }
    }

    public void deleteCrewServer(String serverId) {
        ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(serverId);
        if (service != null) {
            service.provider().stop();
        }
    }

    public ServiceId createCrewServer(int crewId) {
        ServiceTask serviceTask = new ServiceTask();
        serviceTask.setStaticServices(true);
        serviceTask.setName(crewId + "");
        serviceTask.setRuntime("jvm");
        serviceTask.setStartPort(45000);
        serviceTask.setAutoDeleteOnStop(true);
        serviceTask.setDeletedFilesAfterStop(Arrays.asList("/plugins"));
        serviceTask.setGroups(Arrays.asList("Crew"));
        serviceTask.setMinServiceCount(0);
        serviceTask.setMaintenance(false);
        serviceTask.setIncludes(new ArrayList<>());
        serviceTask.setDeployments(new ArrayList<>());
        serviceTask.setAssociatedNodes(new ArrayList<>());
        serviceTask.setProcessConfiguration(new ProcessConfiguration(
                ServiceEnvironmentType.MINECRAFT_SERVER,
                2048,
                new ArrayList<>()
        ));
        serviceTask.setTemplates(Arrays.asList(new ServiceTemplate(
                "Crew",
                "default",
                "local",
                true
        ), new ServiceTemplate("Crew", "plugins", "local", true)));
        ServiceInfoSnapshot serviceInfoSnapshot = DRIVER.getCloudServiceFactory().createCloudService(serviceTask);

        if (serviceInfoSnapshot != null) {
            serviceInfoSnapshot.provider().start();
            return serviceInfoSnapshot.getServiceId();
        }
        return null;
    }

    public ServiceId createVersusServer(Versus versus) {
        ServiceTask serviceTask = new ServiceTask();
        serviceTask.setStaticServices(true);
        serviceTask.setName(versus.getId() + "vs");
        serviceTask.setRuntime("jvm");
        serviceTask.setStartPort(50000);
        serviceTask.setAutoDeleteOnStop(true);
        serviceTask.setDeletedFilesAfterStop(Collections.singletonList("/plugins"));
        serviceTask.setGroups(Arrays.asList("Versus"));
        serviceTask.setMinServiceCount(0);
        serviceTask.setMaintenance(false);
        serviceTask.setIncludes(new ArrayList<>());
        serviceTask.setDeployments(new ArrayList<>());
        serviceTask.setAssociatedNodes(new ArrayList<>());
        serviceTask.setProcessConfiguration(new ProcessConfiguration(
                ServiceEnvironmentType.MINECRAFT_SERVER,
                512,
                new ArrayList<>()
        ));
        serviceTask.setTemplates(Arrays.asList(new ServiceTemplate(
                "Versus",
                "default",
                "local",
                true
        ), new ServiceTemplate("Versus", "plugins", "local", true)));
        ServiceInfoSnapshot serviceInfoSnapshot = DRIVER.getCloudServiceFactory().createCloudService(serviceTask);

        if (serviceInfoSnapshot != null) {
            serviceInfoSnapshot.provider().start();
            return serviceInfoSnapshot.getServiceId();
        }
        return null;
    }
}
