package fr.floxiik.skyapi.api;

import com.rexcantor64.triton.api.Triton;
import com.rexcantor64.triton.api.TritonAPI;
import com.rexcantor64.triton.api.language.LanguageManager;
import com.rexcantor64.triton.api.players.LanguagePlayer;

import java.util.UUID;

public class LangAPI {

    private static LangAPI api;

    public LangAPI() {
        api = this;
    }

    public static LangAPI get() {
        return api == null ? new LangAPI() : api;
    }

    public String parse(UUID uuid, String text) {
        Triton apiTriton = TritonAPI.getInstance();
        LanguagePlayer player = apiTriton.getPlayerManager().get(uuid);
        player.refreshAll();
        LanguageManager manager = apiTriton.getLanguageManager();
        return manager.getText(player, text);
    }

    public String parse(String lang, String text) {
        Triton apiTriton = TritonAPI.getInstance();
        LanguageManager manager = apiTriton.getLanguageManager();
        return manager.getText(lang, text);
    }
}
