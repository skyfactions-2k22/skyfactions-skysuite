package fr.floxiik.skyapi.data.redis;

public class RedisCredentials {
    final private String ip;
    final private String password;
    final private int port;
    final private String clientName;

    public RedisCredentials(String ip, String password, int port) {
        this(ip, password, port, "Redis_bungee_access");
    }

    public RedisCredentials(String ip, String password, int port, String clientName) {
        this.ip = ip;
        this.password = password;
        this.port = port;
        this.clientName = clientName;
    }

    public String getIp() {
        return ip;
    }

    public String getPassword() {
        return password;
    }

    public int getPort() {
        return port;
    }

    public String getClientName() {
        return clientName;
    }

    public String toRedisURL() {
        return "redis://" + ip + ":" + 6379;
    }
}
