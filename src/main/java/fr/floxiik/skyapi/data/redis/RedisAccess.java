package fr.floxiik.skyapi.data.redis;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;

public class RedisAccess {
    private RedissonClient redissonClient;
    public static RedisAccess instance;

    public RedisAccess(RedisCredentials redisCredentials) {
        instance = this;
        this.redissonClient = initRedisson(redisCredentials);
    }

    public static void init() {
        new RedisAccess(new RedisCredentials("127.0.0.1", null, 6379));
    }

    public static void close() {
        RedisAccess.instance.getRedissonClient().shutdown();
    }

    public RedissonClient initRedisson(RedisCredentials redisCredentials) {
        final Config config = new Config();

        config.setCodec(new JsonJacksonCodec());
        config.setThreads(8);
        config.setNettyThreads(8);
        if(redisCredentials.getPassword() != null) {
            config.useSingleServer()
                    .setAddress(redisCredentials.toRedisURL())
                    .setDatabase(3)
                    .setPassword(redisCredentials.getPassword())
                    .setClientName(redisCredentials.getClientName());
        } else {
            config.useSingleServer()
                    .setAddress(redisCredentials.toRedisURL())
                    .setDatabase(3)
                    .setClientName(redisCredentials.getClientName());
        }

        return Redisson.create(config);
    }

    public RedissonClient getRedissonClient() {
        return redissonClient;
    }
}
