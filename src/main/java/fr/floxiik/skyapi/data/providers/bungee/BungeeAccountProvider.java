package fr.floxiik.skyapi.data.providers.bungee;

import fr.floxiik.skyapi.bukkit.skycore.jobs.JobsEnum;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Quest;
import fr.floxiik.skyapi.data.redis.RedisAccess;
import fr.floxiik.skyapi.data.sql.DbManager;
import net.md_5.bungee.api.ProxyServer;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import java.sql.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class BungeeAccountProvider {
    public static final String REDIS_KEY = "account:";
    public static final Account DEFAULT_ACCOUNT = new Account(0, UUID.randomUUID(), "", 0, getDefaultLevels(), 0, getDefaultQuests(), "", "", 0);

    private RedisAccess redisAccess;
    private UUID uuid;

    public BungeeAccountProvider(UUID uuid) {
        this.uuid = uuid;
        this.redisAccess = RedisAccess.instance;
    }

    public Account getAccount() {
        Account account = null;
        try {
            account = getRedis();
            if (account == null) {
                account = getMySQL().get();
                sendRedis(account);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return account;
    }

    public void sendRedis(Account account) {
        final RedissonClient redissonClient = redisAccess.getRedissonClient();
        final String key = REDIS_KEY + this.uuid.toString();
        final RBucket<Account> accountRBucket = redissonClient.getBucket(key);

        accountRBucket.setAsync(account);
    }

    public void sendMySQL(Account account) throws SQLException {
        CompletableFuture.runAsync(() -> {
            try {
                Account sentAccount = account;
                if (sentAccount == null) {
                    sentAccount = getAccount();
                }
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE players SET uuid=?, username=?, crew=?, money=?, level=?, quests=?, invContents=?, armorContents=?, timePlayed WHERE id=?");

                preparedStatement.setString(1, this.uuid.toString());
                preparedStatement.setString(2, sentAccount.getUsername());
                preparedStatement.setInt(3, sentAccount.getCrew());
                preparedStatement.setInt(4, sentAccount.getMoney());
                preparedStatement.setString(5, sentAccount.getLevelString());
                preparedStatement.setString(6, sentAccount.getQuestString());
                preparedStatement.setString(7, sentAccount.getInventory());
                preparedStatement.setString(8, sentAccount.getArmor());
                preparedStatement.setLong(9, sentAccount.getTimePlayed());
                preparedStatement.setInt(10, sentAccount.getId());

                preparedStatement.executeUpdate();

                connection.close();
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    private Account getRedis() throws ExecutionException, InterruptedException {
        final RedissonClient redissonClient = redisAccess.getRedissonClient();
        final String key = REDIS_KEY + this.uuid.toString();
        final RBucket<Account> accountRBucket = redissonClient.getBucket(key);

        return accountRBucket.getAsync().get();
    }

    private CompletableFuture<Account> getMySQL() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM players WHERE uuid = ?");

                preparedStatement.setString(1, this.uuid.toString());
                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                if (resultSet.next()) {

                    final int id = resultSet.getInt("id");
                    final String username = resultSet.getString("username");
                    final int money = resultSet.getInt("money");
                    final String level = resultSet.getString("level");
                    final String quests = resultSet.getString("quests");
                    String[] strings;
                    HashMap<String, Float> levels = new HashMap<>();
                    if (level != null) {
                        strings = level.split("&");
                        for (String str : strings) {
                            String[] values = str.split(";");
                            levels.put(values[0], Float.parseFloat(values[1]));
                        }
                    }
                    ArrayList<Quest> questsList = new ArrayList<>();
                    if (quests != null) {
                        strings = quests.split("&");
                        for (String str : strings) {
                            String[] values = str.split(";");
                            if (values.length > 3)
                                questsList.add(new Quest(values[0], values[1], values[2], Integer.parseInt(values[3]), Integer.parseInt(values[4]), values[5], Integer.parseInt(values[6]), Float.parseFloat(values[7])));
                        }
                    }
                    final int crew = resultSet.getInt("crew");
                    final String inventory = resultSet.getString("invContents");
                    final String armor = resultSet.getString("armorContents");
                    final long timePlayed = resultSet.getLong("timePlayed");

                    connection.close();
                    return new Account(id, this.uuid, username, money, levels, crew, questsList, inventory, armor, timePlayed);
                } else {
                    connection.close();
                    return createAccount().get();
                }
            } catch (ExecutionException | InterruptedException | SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    public CompletableFuture<Account> createAccount() throws SQLException {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final Account account = DEFAULT_ACCOUNT.clone();
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO players (uuid, username, money, level, crew, quests, invContents, armorContents, timePlayed) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

                account.setUsername(ProxyServer.getInstance().getPlayer(this.uuid).getName());
                //account.setUsername(givenUsingJava8_whenGeneratingRandomAlphabeticString_thenCorrect());
                preparedStatement.setString(1, this.uuid.toString());
                preparedStatement.setString(2, account.getUsername());
                preparedStatement.setInt(3, account.getMoney());
                //preparedStatement.setInt(3, RandomGenerator.between(0, 1000000000));
                preparedStatement.setString(4, account.getLevelString());
                preparedStatement.setInt(5, 0);
                preparedStatement.setString(6, account.getQuestString());
                preparedStatement.setString(7, account.getInventory());
                preparedStatement.setString(8, account.getArmor());
                preparedStatement.setLong(9, account.getTimePlayed());

                final int row = preparedStatement.executeUpdate();
                final ResultSet resultSet = preparedStatement.getGeneratedKeys();

                if (row > 0 && resultSet.next()) {
                    final int id = resultSet.getInt(1);

                    account.setId(id);
                    account.setUuid(this.uuid);

                }
                connection.close();
                return account;
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    public CompletableFuture<LinkedHashMap<String, Integer>> getBalTop10() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                LinkedHashMap<String, Integer> baltop = new LinkedHashMap<>();
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT username,max(money) as balance FROM players GROUP BY username ORDER BY max(money) DESC LIMIT 10");

                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                while (resultSet.next()) {

                    final String username = resultSet.getString("username");
                    final int money = resultSet.getInt("balance");
                    baltop.put(username, money);

                }
                connection.close();
                return baltop;
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    public CompletableFuture<Integer> getBalTopRank() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT uuid,max(money) as balance FROM players GROUP BY uuid ORDER BY max(money) DESC");

                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                int rank = 0;

                while (resultSet.next()) {

                    rank++;
                    final String uuid = resultSet.getString("uuid");
                    if(uuid.equals(this.uuid.toString())) {
                        connection.close();
                        return rank;
                    }

                }
                connection.close();
                return rank;
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    private static HashMap<String, Float> getDefaultLevels() {
        HashMap<String, Float> levels = new HashMap<>();
        for(JobsEnum job : JobsEnum.values()) {
            levels.put(job.getLibelle(), 0.0f);
        }
        return levels;
    }

    private static ArrayList<Quest> getDefaultQuests() {
        ArrayList<Quest> quests = new ArrayList<>();
        quests.add(new Quest("b", "LOG", "LOG", 0, 10, "null", 1000, 20));
        quests.add(new Quest("k", "PLAYER", "DIAMOND_SWORD", 0, 1, "null", 5000, 100));
        quests.add(new Quest("p", "OBSIDIAN", "OBSIDIAN", 0, 25, "null", 2000, 50));
        quests.add(new Quest("cr", "LADDER", "LADDER", 0, 1, "null", 1000, 20));
        return quests;
    }
}
