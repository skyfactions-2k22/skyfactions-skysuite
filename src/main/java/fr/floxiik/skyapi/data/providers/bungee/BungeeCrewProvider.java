package fr.floxiik.skyapi.data.providers.bungee;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bungee.BungeeCore;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import fr.floxiik.skyapi.data.Versus;
import fr.floxiik.skyapi.data.redis.RedisAccess;
import fr.floxiik.skyapi.data.sql.DbManager;
import net.md_5.bungee.api.ProxyServer;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class BungeeCrewProvider {
    public static final String REDIS_KEY = "crew:";
    public static final Crew DEFAULT_CREW = new Crew(0, "", UUID.randomUUID());

    private RedisAccess redisAccess;
    private UUID player;
    private int id;

    public BungeeCrewProvider(UUID player, int id) {
        this.player = player;
        this.id = id;
        this.redisAccess = RedisAccess.instance;
    }

    public Crew getCrew() {
        Crew crew = null;
        try {
            crew = getCrewFromRedis();
            if (crew == null) {
                crew = getCrewFromDb().get();
                sendCrewToRedis(crew);
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return crew;
    }

    public void sendCrewToRedis(Crew crew) {
        final RedissonClient redissonClient = redisAccess.getRedissonClient();
        final String key = REDIS_KEY + this.id;
        final RBucket<Crew> crewRBucket = redissonClient.getBucket(key);

        crewRBucket.setAsync(crew);
    }

    public void sendCrewToMySQL() {
        CompletableFuture.runAsync(() -> {
            try {
                Crew crew = getCrew();
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE crews SET name=?, owner=?, description=?, points=?, shield=?, members=?, homes=?, serverId=?, level=? WHERE id=?");

                preparedStatement.setString(1, crew.getName());
                preparedStatement.setString(2, crew.getOwner().toString());
                preparedStatement.setString(3, crew.getDescription());
                preparedStatement.setInt(4, crew.getPoints());
                preparedStatement.setString(5, crew.getShield());
                preparedStatement.setString(6, crew.getMembersToString());
                preparedStatement.setString(7, crew.getHomesToString());
                preparedStatement.setString(8, crew.getServerId());
                preparedStatement.setInt(9, crew.getLevel());
                preparedStatement.setInt(10, crew.getId());

                preparedStatement.executeUpdate();

                connection.close();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        });
    }

    private Crew getCrewFromRedis() throws ExecutionException, InterruptedException {
        final RedissonClient redissonClient = redisAccess.getRedissonClient();
        final String key = REDIS_KEY + this.id;
        final RBucket<Crew> crewRBucket = redissonClient.getBucket(key);

        return crewRBucket.getAsync().get();
    }

    private CompletableFuture<Crew> getCrewFromDb() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final int crewId = this.id;
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM crews WHERE id = ?");

                preparedStatement.setInt(1, crewId);
                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                if (resultSet.next()) {

                    final int id = resultSet.getInt("id");
                    final String serverId = resultSet.getString("serverId");
                    final String name = resultSet.getString("name");
                    final UUID owner = UUID.fromString(resultSet.getString("owner"));
                    final String description = resultSet.getString("description");
                    final int level = resultSet.getInt("level");
                    final int points = resultSet.getInt("points");
                    final String shield = resultSet.getString("shield");
                    final String membersList = resultSet.getString("members");
                    final HashMap<UUID, String> members = new HashMap<>();
                    String[] strings = null;
                    if (membersList != null) {
                        strings = membersList.split(";");
                        for (String str : strings) {
                            String[] values = str.split("&");
                            members.put(UUID.fromString(values[0]), values[1]);
                        }
                    }
                    final String homesList = resultSet.getString("homes");
                    HashMap<String, Home> homes = new HashMap<>();
                    if (homesList != null) {
                        strings = homesList.split("&");
                        for (String str : strings) {
                            String[] values = str.split(";");
                            homes.put(values[0], new Home(Float.parseFloat(values[1]), Float.parseFloat(values[2]), Float.parseFloat(values[3]), Float.parseFloat(values[4]), Float.parseFloat(values[5])));
                        }
                    }
                    final Date creationDate = resultSet.getDate("creationDate");

                    connection.close();
                    return new Crew(id, serverId, name, owner, description, level, points, shield, members, homes, creationDate);
                } else {
                    connection.close();
                    return null;
                }

            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    public CompletableFuture<Integer> getRandomFromDb(int minPoints, int maxPoints, int attacker) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT id FROM crews WHERE points >= ? AND points <= ? AND shield = 'none' AND id != ? ORDER BY RAND() LIMIT 1");

                preparedStatement.setInt(1, minPoints);
                preparedStatement.setInt(2, maxPoints);
                preparedStatement.setInt(3, attacker);
                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                if (resultSet.next()) {
                    final int id = resultSet.getInt("id");
                    connection.close();
                    return id;
                } else {
                    connection.close();
                    return 0;
                }
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    public void addInCrew() {
        Crew crew = getCrew();
        crew.getMembers().put(this.player, "recruit");
        sendCrewToRedis(crew);
        Account account = BungeeCore.getApi().getAccount(this.player);
        account.setCrew(crew.getId());
        BungeeCore.getApi().updateAccount(account, BungeeCore.getApi().isOnline(account.getUuid()));
    }

    public void promote(String rank) {
        Crew crew = getCrew();
        crew.getMembers().replace(this.player, rank);
        sendCrewToRedis(crew);
    }

    public void removeInCrew() {
        Crew crew = getCrew();
        crew.getMembers().remove(this.player, "recruit");
        sendCrewToRedis(crew);
        Account account = BungeeCore.getApi().getAccount(this.player);
        account.setCrew(0);
        BungeeCore.getApi().updateAccount(account, BungeeCore.getApi().isOnline(account.getUuid()));
    }

    public CompletableFuture<Crew> createNewCrew(UUID uuid, String name) throws SQLException {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final Crew crew = DEFAULT_CREW.clone();
                crew.setName(name);
                crew.setOwner(uuid);

                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO crews (name, owner, level, points, shield) VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

                preparedStatement.setString(1, crew.getName());
                preparedStatement.setString(2, crew.getOwner().toString());
                preparedStatement.setInt(3, 1);
                preparedStatement.setInt(4, 0);
                preparedStatement.setString(5, "none");

                final int row = preparedStatement.executeUpdate();
                final ResultSet resultSet = preparedStatement.getGeneratedKeys();

                if (row > 0 && resultSet.next()) {
                    final int id = resultSet.getInt(1);
                    crew.setId(id);
                }

                crew.setHome("home", new Home(24f, 63f, 24f, -180f, -0f));
                crew.setServerId(SpigotCore.getApi().createCrewServer(crew.getId()).getName());
                crew.setLevel(1);

                preparedStatement = connection.prepareStatement("UPDATE crews SET serverId=?, homes=? WHERE id=?");

                preparedStatement.setString(1, crew.getServerId());
                preparedStatement.setString(2, crew.getHomesToString());
                preparedStatement.setInt(3, crew.getId());

                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement("INSERT INTO crews_schematics (id) VALUES (?)");

                preparedStatement.setInt(1, crew.getId());

                preparedStatement.executeUpdate();

                connection.close();

                sendCrewToRedis(crew);
                return crew;
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    public void deleteCrew() {
        //Reset Members Crew
        Crew crew = getCrew();
        if (crew.getMembers().size() > 0) {
            for (UUID member : crew.getMembers().keySet()) {
                Account memberAccount = BungeeCore.getApi().getAccount(member);
                memberAccount.setCrew(0);
                BungeeCore.getApi().updateAccount(memberAccount, ProxyServer.getInstance().getPlayer(member) != null);
            }
        }

        //Reset Owner Crew
        Account owner = BungeeCore.getApi().getAccount(this.player);
        owner.setCrew(0);
        BungeeCore.getApi().updateAccount(owner, ProxyServer.getInstance().getPlayer(owner.getUuid()) != null);

        //Remove owner of crew in database
        CompletableFuture.runAsync(() -> {
            try {
                final int crewId = this.id;
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("UPDATE crews SET owner=? WHERE id=?");

                preparedStatement.setString(1, "disbanded");
                preparedStatement.setInt(2, crewId);
                preparedStatement.executeUpdate();
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });

        BungeeCore.getApi().deleteCrewServer(crew.getServerId());

        //Delete crew from Redis
        final RedissonClient redissonClient = redisAccess.getRedissonClient();
        final String key = REDIS_KEY + this.id;
        final RBucket<Crew> crewRBucket = redissonClient.getBucket(key);
        crewRBucket.deleteAsync();
    }

    public void setHome(String name, Home location) {
        Crew crew = getCrew();
        crew.setHome(name, location);
        sendCrewToRedis(crew);
    }

    public void delHome(String name) {
        Crew crew = getCrew();
        crew.delHome(name);
        sendCrewToRedis(crew);
    }

    public CompletableFuture<Versus> registerVersus(Versus versus) {
        return CompletableFuture.supplyAsync( () -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO versus (attacker, victim) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);

                preparedStatement.setInt(1, versus.getAttacker());
                preparedStatement.setInt(2, versus.getVictim());

                final int row = preparedStatement.executeUpdate();
                final ResultSet resultSet = preparedStatement.getGeneratedKeys();

                if (row > 0 && resultSet.next()) {
                    final int idGenerated = resultSet.getInt(1);
                    connection.close();
                    Versus registered = getVersus(idGenerated).get();
                    long time = registered.getCreationDate().getTime();
                    registered.setStartDate(new Date(time + (10 * 60 * 1000)));
                    registered.setServerId(BungeeCore.getApi().createVersusServer(registered).getName());

                    updateVersus(registered);

                    return registered;
                } else {
                    return null;
                }
            } catch (Throwable exception) {
                exception.printStackTrace();
                return null;
            }
        });
    }

    public void updateVersus(Versus versus) {
        CompletableFuture.runAsync(() -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE versus SET serverId=?, start_date=?, winner=? WHERE id=?");

                preparedStatement.setString(1, versus.getServerId());
                preparedStatement.setDate(2, new java.sql.Date(versus.getStartDate().getTime()));
                preparedStatement.setInt(3, versus.getWinner());
                preparedStatement.setInt(4, versus.getId());

                preparedStatement.executeUpdate();

                connection.close();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        });
    }

    public CompletableFuture<ArrayList<Versus>> getAllVersus() {
        return CompletableFuture.supplyAsync( () -> {
            try {
                ArrayList<Versus> allVersus = new ArrayList<>();
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM versus WHERE winner IS NULL");

                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                while (resultSet.next()) {
                    final int id = resultSet.getInt("id");
                    final String serverId = resultSet.getString("serverId");
                    final Date startDate = resultSet.getTimestamp("start_date");
                    final int attacker = resultSet.getInt("attacker");
                    final int victim = resultSet.getInt("victim");
                    final int winner = resultSet.getInt("winner");
                    final Date creationDate = resultSet.getTimestamp("creation_date");
                    connection.close();
                    allVersus.add(new Versus(id, serverId, startDate, attacker, victim, winner, creationDate));
                }
                return allVersus;
            } catch (Throwable exception) {
                exception.printStackTrace();
                return null;
            }
        });
    }

    public CompletableFuture<Versus> getVersus(int versusId) {
        return CompletableFuture.supplyAsync( () -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM versus WHERE id = ?");

                preparedStatement.setInt(1, versusId);

                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                if (resultSet.next()) {
                    final int id = resultSet.getInt("id");
                    final String serverId = resultSet.getString("serverId");
                    final Date startDate = resultSet.getTimestamp("start_date");
                    final int attacker = resultSet.getInt("attacker");
                    final int victim = resultSet.getInt("victim");
                    final int winner = resultSet.getInt("winner");
                    final Date creationDate = resultSet.getTimestamp("creation_date");
                    connection.close();
                    return new Versus(id, serverId, startDate, attacker, victim, winner, creationDate);
                } else {
                    connection.close();
                    return null;
                }
            } catch (Throwable exception) {
                exception.printStackTrace();
                return null;
            }
        });
    }

    public void updateSchematic(File file) {
        CompletableFuture.runAsync(() -> {
            try {
                Crew crew = getCrew();
                final Connection connection;
                connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE crews_schematics SET schematic=? WHERE id=?");

                FileInputStream inputStream = new FileInputStream(file);
                preparedStatement.setBlob(1, inputStream);
                preparedStatement.setInt(2, crew.getId());

                preparedStatement.executeUpdate();

                inputStream.close();
                connection.close();
            } catch (SQLException | IOException exception) {
                exception.printStackTrace();
            }
        });
    }

    public CompletableFuture<File> getSchematic() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Crew crew = getCrew();
                File schematic = new File("/crews_plot/", crew.getId() + ".schematic");
                FileOutputStream outputStream = null;
                outputStream = new FileOutputStream(schematic);
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement =
                        connection.prepareStatement("SELECT * FROM crews_schematics WHERE id=?");

                preparedStatement.setInt(1, crew.getId());

                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    InputStream input = rs.getBinaryStream("schematic");
                    byte[] buffer = new byte[1024];
                    while (input.read(buffer) > 0) {
                        outputStream.write(buffer);
                    }
                }
                outputStream.close();
                connection.close();
                return schematic;
            } catch (SQLException | IOException exception) {
                throw new Error(exception);
            }
        });
    }
}
