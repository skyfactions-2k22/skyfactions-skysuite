package fr.floxiik.skyapi.data.providers.spigot;

import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Quest;
import fr.floxiik.skyapi.data.redis.RedisAccess;
import fr.floxiik.skyapi.data.sql.DbManager;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class SpigotAccountProvider {
    public static final String REDIS_KEY = "account:";

    private RedisAccess redisAccess;
    private UUID uuid;
    private String username;

    public SpigotAccountProvider(UUID uuid) {
        this.uuid = uuid;
        this.redisAccess = RedisAccess.instance;
    }

    public SpigotAccountProvider(String username) {
        this.username = username;
        this.redisAccess = RedisAccess.instance;
    }

    public Account getAccount() {
        Account account = null;
        try {
            account = getRedis();
            if (account == null) {
                account = getMySQL().get();
                sendRedis(account);
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return account;
    }


    public void sendRedis(Account account) {
        final RedissonClient redissonClient = redisAccess.getRedissonClient();
        final String key = REDIS_KEY + this.uuid.toString();
        final RBucket<Account> accountRBucket = redissonClient.getBucket(key);
        accountRBucket.setAsync(account);
    }

    public void sendMySQL(Account account) throws SQLException {
        CompletableFuture.runAsync(() -> {
            try {
                Account sentAccount = account;
                if (sentAccount == null) {
                    sentAccount = getAccount();
                }
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE players SET uuid=?, username=?, crew=?, money=?, level=?, quests=?, invContents=?, armorContents=?, timePlayed=? WHERE id=?");

                preparedStatement.setString(1, sentAccount.getUuid().toString());
                preparedStatement.setString(2, sentAccount.getUsername());
                preparedStatement.setInt(3, sentAccount.getCrew());
                preparedStatement.setInt(4, sentAccount.getMoney());
                preparedStatement.setString(5, sentAccount.getLevelString());
                preparedStatement.setString(6, sentAccount.getQuestString());
                preparedStatement.setString(7, sentAccount.getInventory());
                preparedStatement.setString(8, sentAccount.getArmor());
                preparedStatement.setLong(9, sentAccount.getTimePlayed());
                preparedStatement.setInt(10, sentAccount.getId());

                preparedStatement.executeUpdate();

                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
    }

    private Account getRedis() throws ExecutionException, InterruptedException {
        final RedissonClient redissonClient = redisAccess.getRedissonClient();
        final String key = REDIS_KEY + this.uuid.toString();
        final RBucket<Account> accountRBucket = redissonClient.getBucket(key);

        return accountRBucket.getAsync().get();
    }

    private CompletableFuture<Account> getMySQL() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM players WHERE uuid = ?");

                preparedStatement.setString(1, this.uuid.toString());
                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                if (resultSet.next()) {

                    final int id = resultSet.getInt("id");
                    final String username = resultSet.getString("username");
                    final int money = resultSet.getInt("money");
                    final String level = resultSet.getString("level");
                    final String quests = resultSet.getString("quests");
                    String[] strings;
                    HashMap<String, Float> levels = new HashMap<>();
                    if (level != null) {
                        strings = level.split("&");
                        for (String str : strings) {
                            String[] values = str.split(";");
                            levels.put(values[0], Float.parseFloat(values[1]));
                        }
                    }
                    ArrayList<Quest> questsList = new ArrayList<>();
                    if (quests != null) {
                        strings = quests.split("&");
                        for (String str : strings) {
                            String[] values = str.split(";");
                            questsList.add(new Quest(values[0], values[1], values[2], Integer.parseInt(values[3]), Integer.parseInt(values[4]), values[5], Integer.parseInt(values[6]), Float.parseFloat(values[7])));
                        }
                    }
                    final int crew = resultSet.getInt("crew");
                    final String inventory = resultSet.getString("invContents");
                    final String armor = resultSet.getString("armorContents");
                    final long timePlayed = resultSet.getLong("timePlayed");

                    connection.close();
                    return new Account(id, this.uuid, username, money, levels, crew, questsList, inventory, armor, timePlayed);
                } else {
                    connection.close();
                    return null;
                }
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }

    public CompletableFuture<Account> getUsername() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                final Connection connection = DbManager.SKYFACTIONS.getDbAccess().getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM players WHERE username = ?");

                preparedStatement.setString(1, this.username);
                preparedStatement.executeQuery();

                final ResultSet resultSet = preparedStatement.getResultSet();

                if (resultSet.next()) {

                    final int id = resultSet.getInt("id");
                    final UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                    final int money = resultSet.getInt("money");
                    final String level = resultSet.getString("level");
                    final String quests = resultSet.getString("quests");
                    String[] strings;
                    HashMap<String, Float> levels = new HashMap<>();
                    if (level != null) {
                        strings = level.split("&");
                        for (String str : strings) {
                            String[] values = str.split(";");
                            levels.put(values[0], Float.parseFloat(values[1]));
                        }
                    }
                    ArrayList<Quest> questsList = new ArrayList<>();
                    if (quests != null) {
                        strings = quests.split("&");
                        for (String str : strings) {
                            String[] values = str.split(";");
                            if (values.length > 4) {
                                questsList.add(new Quest(values[0], values[1], values[2], Integer.parseInt(values[3]), Integer.parseInt(values[4]), values[5], Integer.parseInt(values[6]), Float.parseFloat(values[7])));
                            }
                        }
                    }
                    final int crew = resultSet.getInt("crew");
                    final String inventory = resultSet.getString("invContents");
                    final String armor = resultSet.getString("armorContents");
                    final long timePlayed = resultSet.getLong("timePlayed");

                    connection.close();
                    return new Account(id, uuid, this.username, money, levels, crew, questsList, inventory, armor, timePlayed);
                } else {
                    connection.close();
                    return null;
                }
            } catch (SQLException exception) {
                throw new Error(exception);
            }
        });
    }
}
