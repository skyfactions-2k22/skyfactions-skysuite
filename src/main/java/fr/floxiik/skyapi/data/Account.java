package fr.floxiik.skyapi.data;

import fr.floxiik.skyapi.bukkit.skycore.jobs.JobsEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Account implements Cloneable {

    private int id;
    private UUID uuid;
    private String username;
    private int money;
    private HashMap<String, Float> levels;
    private int crew;
    private ArrayList<Quest> quests;
    private String inventory;
    private String armor;
    private long timePlayed;

    public Account(int id, UUID uuid, String username, int money, HashMap<String, Float> levels, int crew, ArrayList<Quest> quests, String inventory, String armor, long timePlayed) {
        this.id = id;
        this.uuid = uuid;
        this.username = username;
        this.money = money;
        this.levels = levels;
        this.crew = crew;
        this.quests = quests;
        this.inventory = inventory;
        this.armor = armor;
    }

    public Account() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public ArrayList<Quest> getQuests() {
        return quests;
    }

    public void setQuests(ArrayList<Quest> quests) {
        this.quests = quests;
    }

    public float getXp(String job) {
        return levels.get(job);
    }

    public int getLevel(String job) {
        int lvl = (int) (Math.sqrt(getXp(job))/Math.sqrt(5));
        return Math.min(lvl, JobsEnum.FARMER.getMaxLevel());
    }

    public int getLevelPourcent(String job) {
        return (int) ((getXp(job)-(5*Math.pow(getLevel(job), 2)))*100/((5*Math.pow(getLevel(job)+1, 2))-(5*Math.pow(getLevel(job), 2))));
    }

    public HashMap<String, Float> getLevels() {
        return levels;
    }

    public void setLevel(String level, float value) {
        levels.replace(level, value);
    }

    public void addLevel(String level, float value) {
        levels.replace(level, levels.get(level) + value);
    }

    public void removeLevel(String level, float value) {
        levels.replace(level, levels.get(level) - value);
    }

    public int getCrew() {
        return crew;
    }

    public void setCrew(int crew) {
        this.crew = crew;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }

    public long getTimePlayed() {
        return timePlayed;
    }

    public void setTimePlayed(long timePlayed) {
        this.timePlayed = timePlayed;
    }

    public Account clone() {
        try {
            return (Account) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getLevelString() {
        StringBuilder levelsString = new StringBuilder();
        for(String str : levels.keySet()) {
            levelsString.append(str).append(";").append(levels.get(str)).append("&");
        }
        return levelsString.toString();
    }

    public String getQuestString() {
        StringBuilder questsString = new StringBuilder();
        for(Quest quest : getQuests()) {
            questsString.append(quest.getType()).append(";").append(quest.getTargetType()).append(";").append(quest.getTargetTypeToShow()).append(";").append(quest.getCurrent()).append(";").append(quest.getTarget()).append(";").append(quest.getOther()).append(";").append(quest.getRewardMoney()).append(";").append(quest.getRewardXp()).append("&");
        }
        return questsString.toString();
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", uuid=" + uuid +
                ", money=" + money +
                ", level=" + getLevelString() +
                ", crew=" + crew +
                '}';
    }
}
