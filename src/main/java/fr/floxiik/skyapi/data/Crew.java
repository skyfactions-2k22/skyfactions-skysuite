package fr.floxiik.skyapi.data;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class Crew implements Cloneable {

    private int id;
    private String serverId;
    private String name;
    private UUID owner;
    private String description;
    private int level;
    private int points;
    private String shield;
    private HashMap<UUID, String> members = new HashMap<>();
    private HashMap<String, Home> homes = new HashMap<>();
    private Date creationDate;

    public Crew() {}

    public Crew(int id, String name, UUID owner) {
        this.id = id;
        this.name = name;
        this.owner = owner;
    }

    public Crew(int id, String serverId, String name, UUID owner, String description, int level, int points, String shield, HashMap<UUID, String> members, HashMap<String, Home> homes, Date creationDate) {
        this.id = id;
        this.serverId = serverId;
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.level = level;
        this.points = points;
        this.shield = shield;
        this.members = members;
        this.homes = homes;
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getOwner() {
        return owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getPoints() {
        return points;
    }

    public String getShield() {
        return shield;
    }

    public void setShield(String shield) {
        this.shield = shield;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public HashMap<UUID, String> getMembers() {
        return members;
    }

    public void setMembers(HashMap<UUID, String> members) {
        this.members = members;
    }

    public HashMap<String, Home> getHomes() {
        return homes;
    }

    public void setHomes(HashMap<String, Home> homes) {
        this.homes = homes;
    }

    public void setHome(String name, Home location) {
        if(homes.containsKey(name)) {
            homes.replace(name, location);
        } else {
            homes.put(name, location);
        }
    }

    public void delHome(String name) {
        homes.remove(name);
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getMembersToString() {
        StringBuilder members = new StringBuilder();
        if(getMembers().size() == 0) {
            return "NULL";
        }
        for(UUID member : getMembers().keySet()) {
            members.append(member.toString()).append("&").append(getMembers().get(member)).append(";");
        }
        return members.toString();
    }

    public String getHomesToString() {
        StringBuilder homes = new StringBuilder();
        for(String home : getHomes().keySet()) {
            homes.append(home).append(";").append(getHomes().get(home).getX()).append(";").append(getHomes().get(home).getY()).append(";").append(getHomes().get(home).getZ()).append(";").append(getHomes().get(home).getYaw()).append(";").append(getHomes().get(home).getPitch()).append("&");
        }
        return homes.toString();
    }

    public Crew clone() {
        try {
            return (Crew) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String toString() {
        return "Crew{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                ", description='" + description + '\'' +
                ", points=" + points +
                ", members=" + members +
                ", homes=" + homes +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }
}