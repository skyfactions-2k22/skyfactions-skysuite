package fr.floxiik.skyapi.data.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DbAccess {

    private DbCredentials dbCredentials;
    private HikariDataSource hikariDataSource;

    public DbAccess(DbCredentials dbCredentials) {
        this.dbCredentials = dbCredentials;
    }

    private void setupHikariCP() {
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setJdbcUrl(dbCredentials.toURL());
        hikariConfig.setUsername(dbCredentials.getUser());
        hikariConfig.setPassword(dbCredentials.getPass());
        hikariConfig.setMaxLifetime(600000L);
        hikariConfig.setIdleTimeout(300000l);
        hikariConfig.setLeakDetectionThreshold(300000L);
        hikariConfig.setConnectionTimeout(10000L);

        this.hikariDataSource = new HikariDataSource(hikariConfig);
    }

    public void initPool() {
        setupHikariCP();
    }

    public void closePool() {
        this.hikariDataSource.close();
    }

    public Connection getConnection() throws SQLException {
        if(hikariDataSource == null) {
            System.out.println("Not Connected");
            setupHikariCP();
        }
        return this.hikariDataSource.getConnection();
    }
}
