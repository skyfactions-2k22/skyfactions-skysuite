package fr.floxiik.skyapi.data.sql;

public enum DbManager {
    SKYFACTIONS(new DbCredentials("127.0.0.1", "skyfactionsDB", "2xRVW7yFbZ&[emt7B4!%243){", "skyfactions_test", 3306));

    private DbAccess dbAccess;
    DbManager(DbCredentials dbCredentials) {
        this.dbAccess = new DbAccess(dbCredentials);
    }

    public DbAccess getDbAccess() {
        return dbAccess;
    }

    public static void initAllDatabaseConnections() {
        for(DbManager dbManager : values()) {
            dbManager.dbAccess.initPool();
        }
    }

    public static void closeAllDatabaseConnections() {
        for(DbManager dbManager : values()) {
            dbManager.dbAccess.closePool();
        }
    }
}
