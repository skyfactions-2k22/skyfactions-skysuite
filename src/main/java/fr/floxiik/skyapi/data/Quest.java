package fr.floxiik.skyapi.data;

import com.google.common.base.CaseFormat;

public class Quest {

    private String type; // b = break, p = place, c = recolter, k = tuer, s = sell, by = buy, cr = craft
    private String targetType; //A block, item or a Mob / Player
    private String targetTypeToShow; //A material or a Mob / Player
    private int current;
    private int target;
    private String other;
    private int rewardMoney;
    private float rewardXp;

    public Quest() {}

    public Quest(String type, String targetType, String targetTypeToShow, int current, int target, String other, int rewardMoney, float rewardXp) {
        this.type = type;
        this.targetType = targetType;
        this.targetTypeToShow = targetTypeToShow;
        this.current = current;
        this.target = target;
        this.other = other;
        this.rewardMoney = rewardMoney;
        this.rewardXp = rewardXp;
    }

    public String getTypeString() {
        switch (getType()) {
            case "b":
                return "Break";
            case "p":
                return "Place";
            case "c":
                return "Collect";
            case "k":
                return "Kill";
            case "s":
                return "Sell";
            case "by":
                return "Buy";
            case "cr":
                return "Craft";
        }
        return null;
    }

    public String toString() {
        return getTypeString() + " " + getTarget() + " " + toCamelCase(getTargetType()) + (getOther().equals("null") ? "" : " " + getOther());
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getTargetTypeToShow() {
        return targetTypeToShow;
    }

    public void setTargetTypeToShow(String targetTypeToShow) {
        this.targetTypeToShow = targetTypeToShow;
    }

    public int getRewardMoney() {
        return rewardMoney;
    }

    public void setRewardMoney(int rewardMoney) {
        this.rewardMoney = rewardMoney;
    }

    public float getRewardXp() {
        return rewardXp;
    }

    public void setRewardXp(float rewardXp) {
        this.rewardXp = rewardXp;
    }

    public boolean isCompleted() {
        return getCurrent() >= getTarget();
    }


    private String toCamelCase(String s){
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, s);
    }
}
