package fr.floxiik.skyapi.data;

import java.util.Date;

public class Versus {

    private int id;
    private String serverId;
    private Date startDate;
    private int attacker;
    private int victim;
    private int winner;
    private Date creationDate;

    public Versus() {}

    public Versus(int attacker, int victim) {
        this.attacker = attacker;
    }

    public Versus(int id, String serverId, Date startDate, int attacker, int victim, int winner, Date creationDate) {
        this.id = id;
        this.serverId = serverId;
        this.startDate = startDate;
        this.attacker = attacker;
        this.victim = victim;
        this.winner = winner;
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getAttacker() {
        return attacker;
    }

    public void setAttacker(int attacker) {
        this.attacker = attacker;
    }

    public int getVictim() {
        return victim;
    }

    public void setVictim(int victim) {
        this.victim = victim;
    }

    public int getWinner() {
        return winner;
    }

    public void setWinner(int winner) {
        this.winner = winner;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
