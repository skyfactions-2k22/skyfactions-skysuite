package fr.floxiik.skyapi.bukkit.skyshop;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.commands.extra.Gamemode;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class SkyShop {

    public static SkyShop instance;
    private String version = "1.0.0";

    public void init(SpigotCore spigotCore) {
        instance = this;

        registerEvents(spigotCore);
        registerCommands(spigotCore);

        sendLog(true);
    }

    public void disable(SpigotCore spigotCore) {
        sendLog(false);
    }


    private void registerEvents(SpigotCore spigotCore) {
        PluginManager pm = Bukkit.getPluginManager();
        //pm.registerEvents(new RainListener(), spigotCore);
    }

    private void registerCommands(SpigotCore spigotCore) {
        spigotCore.getCommand("shop").setExecutor(new Gamemode());
    }

    public static SkyShop getInstance() {
        return instance;
    }

    public String getVersion() {
        return version;
    }

    public void sendLog(boolean start) {
        SpigotCore.getInstance().getLogger().info("<==================================>");
        SpigotCore.getInstance().getLogger().info("");
        if(start) SpigotCore.getInstance().getLogger().info("  Module (SkyShop) Chargé avec succès !");
        if(!start) SpigotCore.getInstance().getLogger().info("  Module (SkyShop) Désactivé avec succès !");
        SpigotCore.getInstance().getLogger().info("  Version: " + getVersion());
        SpigotCore.getInstance().getLogger().info("  Autheur: Achereth");
        SpigotCore.getInstance().getLogger().info("");
        SpigotCore.getInstance().getLogger().info("<==================================>");
    }
}
