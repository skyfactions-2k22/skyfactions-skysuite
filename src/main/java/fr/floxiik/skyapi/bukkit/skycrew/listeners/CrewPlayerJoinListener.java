package fr.floxiik.skyapi.bukkit.skycrew.listeners;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.BuiltInClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.session.ClipboardHolder;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.api.SkygotApi;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.SkyCrew;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

public class CrewPlayerJoinListener implements Listener {

    private boolean firstConnection = true;
    private Crew crew;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        UUID playerUUID = player.getUniqueId();

        //Recupération du crew
        if (SpigotCore.getApi().hasCrew(playerUUID)) {

            if (SkyCrew.getInstance().isCrewInstance()) {
                crew = SpigotCore.getApi().getCrew(Integer.parseInt(SpigotCore.getServerName().replace("-1", "")));
                event.setJoinMessage(null);
                Bukkit.getOnlinePlayers().forEach((member) -> member.sendMessage(LangAPI.get().parse(member.getUniqueId(), "skycrew.playerjoin")
                        .replace("{player}", player.getName())));

                //Collage Schematic
                if (firstConnection) {
                    if (SpigotCore.getServerName().equals(crew.getServerId())) {
                        firstConnection = false;
                        //SkyCrew.getCrewManager().setInstanceOwner(crew.getOwner());
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.island.update"));
                        Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), this::pasteCrew, 1L);
                    }
                }
            }
        }
    }

    private void pasteCrew() {
        File schematic = SkygotApi.get().getSchematic(crew);
        if (schematic != null) {

            Clipboard clipboard;
            ClipboardFormat format = ClipboardFormats.findByFile(schematic);

            try {
                ClipboardReader reader = format.getReader(new FileInputStream(schematic));
                clipboard = reader.read();
                reader.close();

                EditSession editSession = WorldEdit.getInstance().newEditSession(BukkitAdapter.adapt(Bukkit.getWorld("crew")));
                Operation operation = new ClipboardHolder(clipboard)
                        .createPaste(editSession)
                        .to(BlockVector3.at(0, 63, 0))
                        .build();
                Operations.complete(operation);
                editSession.close();
            } catch (NullPointerException | WorldEditException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
