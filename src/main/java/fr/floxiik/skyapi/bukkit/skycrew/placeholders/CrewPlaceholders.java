package fr.floxiik.skyapi.bukkit.skycrew.placeholders;

import fr.floxiik.skyapi.api.SkygotApi;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.utils.CrewUtils;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class CrewPlaceholders extends PlaceholderExpansion {

    @Override
    public String getAuthor() {
        return "Floxiik";
    }

    @Override
    public String getIdentifier() {
        return "skycrew";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier) {
        UUID playerUUID = player.getUniqueId();
        Account account = SkygotApi.get().getAccount(playerUUID);
        if (SpigotCore.getApi().hasCrew(account)) {
            Crew crew = SpigotCore.getApi().getCrew(account);
            if (identifier.equals("crew_name")) {
                return crew.getName();
            }
            if (identifier.equals("crew_rank")) {
                if (crew.getOwner().toString().equals(playerUUID.toString())) {
                    return "crown";
                } else if (crew.getMembers().containsKey(playerUUID)) {
                    return CrewUtils.getRank(crew.getMembers().get(playerUUID));
                } else {
                    return "";
                }
            }
            if (identifier.equals("crew_all")) {
                if (crew.getOwner().toString().equals(playerUUID.toString())) {
                    return "crown" + crew.getName() + "§r";
                } else if (crew.getMembers().containsKey(playerUUID)) {
                    return CrewUtils.getRank(crew.getMembers().get(playerUUID)) + crew.getName();
                } else {
                    return "";
                }
            }
            return null;
        } else {
            return "";
        }
    }
}