package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.bukkit.skycrew.utils.CrewUtils;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.UUID;
import java.util.regex.Pattern;

public class ShowCommand extends SubCommand {

    @Override
    public String getName() {
        return "show";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.show.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.show.usage[/lang]";
    }

    final private Pattern p = Pattern.compile("^[a-zA-Z]*$");
    private Crew crew;

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if (SpigotCore.getApi().hasCrew(playerUUID)) {
            crew = SpigotCore.getApi().getCrew(playerUUID);
            player.sendMessage("§8§m----------------§7[ §a" + crew.getName() + " §7]§8§m----------------");
            //player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.line1") + crew.getName());
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.line2") + (crew.getDescription() == null ? "§c╳" : crew.getDescription()));
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.line3") + crew.getPoints());
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.line4") + crew.getLevel());
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.line5") + crew.getCreationDate().toString());
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.line6") + parsePlayer(crew.getOwner(), false));
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.line7"));
            StringBuilder string = new StringBuilder("     ");
            Iterator<UUID> iterator = crew.getMembers().keySet().iterator();
            if(iterator.hasNext()) {
                while (iterator.hasNext()) {
                    for (int i = 0; i <= 4; i++) {
                        if (iterator.hasNext()) {
                            UUID member = iterator.next();
                            string.append(parsePlayer(member, true)).append("§e, ");
                            player.sendMessage(string.toString());
                            string.setLength(0);
                            string.append("     ");
                        }
                    }
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.show.nomember"));
            }
            string.setLength(0);
            string.append("----");
            for(int i = 1; i < crew.getName().length(); i++) {
                string.append("-");
            }
            player.sendMessage("§8§m----------------" + string + "----------------");
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }

    private String parsePlayer(UUID uuid, boolean member) {
        if(member) {
            return "§7(" + (SpigotCore.getApi().isOnline(uuid) ? "§a❍" : "§c❍") + "§7) " + CrewUtils.getRank(crew.getMembers().get(uuid)) + "§7" + SpigotCore.getApi().getAccount(uuid).getUsername();
        } else {
            return "§7(" + (SpigotCore.getApi().isOnline(uuid) ? "§a❍" : "§c❍") + "§7) " + CrewUtils.getRank("leader") + "§7" + SpigotCore.getApi().getAccount(uuid).getUsername();
        }
    }
}
