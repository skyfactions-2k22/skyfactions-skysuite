package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

public class WarpCommand extends SubCommand {

    @Override
    public String getName() {
        return "warp";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.warp.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.warp.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if (SpigotCore.getApi().hasCrew(playerUUID)) {
            Crew crew = SpigotCore.getApi().getCrew(playerUUID);
            if(args.length > 1) {
                Home home = crew.getHomes().get(args[1]);
                if(home != null) {
                    if (SpigotCore.getServerName().equals(crew.getServerId())) {
                        player.teleport(new Location(Bukkit.getWorlds().get(0), home.getX(), home.getY(), home.getZ(), home.getYaw(), home.getPitch()));
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.warp")
                            .replace("{warp}", args[1]));
                    } else {
                        SkyCore.getTeleportManager().teleportToLocation(SpigotCore.getApi().getAccount(playerUUID), home);

                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.warp.request")
                            .replace("{warp}", args[1]));
                    }
                } else {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.warp.notfound"));
                }
            } else {
                if(crew.getHomes().size() < 1) {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.warp.nowarp"));
                } else {
                    TextComponent msg;
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.warp.list")
                        .replace("{count}", String.valueOf(crew.getHomes().size())));
                    for (String warp : crew.getHomes().keySet()) {
                        msg = new TextComponent("§7- §e" + warp);
                        msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(LangAPI.get().parse(playerUUID, "skycrew.warp.teleport.hover")).create()));
                        msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/f warp " + warp));
                        player.spigot().sendMessage(msg);
                    }
                }
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
