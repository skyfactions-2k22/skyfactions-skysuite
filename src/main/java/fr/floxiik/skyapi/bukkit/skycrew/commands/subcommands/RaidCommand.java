package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import org.bukkit.entity.Player;

import java.util.regex.Pattern;

public class RaidCommand extends SubCommand {

    @Override
    public String getName() {
        return "raid";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.raid.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.raid.usage[/lang]";
    }

    final private Pattern p = Pattern.compile("^[a-zA-Z]*$");

    @Override
    public void perform(Player player, String[] args) {
        if(SpigotCore.getApi().hasCrew(player.getUniqueId())) {

            player.performCommand("raid");

        } else {
            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.nofac"));
        }
    }
}
