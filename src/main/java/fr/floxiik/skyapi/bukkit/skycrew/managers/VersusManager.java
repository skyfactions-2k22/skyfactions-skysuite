package fr.floxiik.skyapi.bukkit.skycrew.managers;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.session.ClipboardHolder;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import fr.floxiik.skyapi.api.SkygotApi;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import fr.floxiik.skyapi.data.Versus;
import fr.floxiik.skyapi.data.providers.spigot.SpigotCrewProvider;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class VersusManager {

    private static final CloudNetDriver DRIVER = CloudNetDriver.getInstance();
    public boolean isVersusInstance = false;
    public int versusInstance;
    public Versus versus;

    public VersusManager() {
        init();
    }

    public void init() {
        if (SpigotCore.getServerName().contains("vs")) {
            isVersusInstance = true;
            versusInstance = Integer.parseInt(SpigotCore.getServerName().split("vs")[0]);
            try {
                versus = new SpigotCrewProvider(null, versusInstance).getVersus(versusInstance).get();
                startCopy();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public void startCopy() {
        //Les deux factions
        Crew attacker = SkygotApi.get().getCrew(versus.getAttacker());
        Crew victim = SkygotApi.get().getCrew(versus.getVictim());

        SpigotCrewProvider attackerCrewProvider = new SpigotCrewProvider(null, versus.getAttacker());
        SpigotCrewProvider victimCrewProvider = new SpigotCrewProvider(null, versus.getVictim());

        //On envois les membres des factions au hub
        sendToSpawn(attacker);
        sendToSpawn(victim);

        //On arrete les serveurs pour enregistrer les bases en schematic
        stopServer(attacker);
        stopServer(victim);

        //On attend que les schematics soient bien sauveguardées
        Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () -> {

            try {
                //Mise à jour faction Attacker
                for (String name : attacker.getHomes().keySet()) {
                    Home newHome = attacker.getHomes().get(name);
                    newHome.setX(newHome.getX() - 50);
                    attacker.setHome(name, newHome);
                    attacker.setServerId(versus.getServerId());
                }
                attackerCrewProvider.sendCrewToRedis(attacker);

                //Mise à jour faction Victim
                for (String name : victim.getHomes().keySet()) {
                    Home newHome = victim.getHomes().get(name);
                    newHome.setX(newHome.getX() + 50);
                    victim.setHome(name, newHome);
                    victim.setServerId(versus.getServerId());
                }
                victimCrewProvider.sendCrewToRedis(victim);

                //Les schematics
                File attackerSchem = attackerCrewProvider.getSchematic().get();
                File victimSchem = victimCrewProvider.getSchematic().get();

                //Collage faction attacker
                ClipboardFormat format = ClipboardFormats.findByFile(attackerSchem);
                ClipboardReader reader = format.getReader(new FileInputStream(attackerSchem));
                Clipboard clipboard = reader.read();

                EditSession editSession = WorldEdit.getInstance().newEditSession(BukkitAdapter.adapt(Bukkit.getWorlds().get(0)));
                Operation operation = new ClipboardHolder(clipboard)
                        .createPaste(editSession)
                        .to(BlockVector3.at(-50, 63, 0))
                        .ignoreAirBlocks(true)
                        .build();
                Operations.complete(operation);
                editSession.close();

                //Collage faction victim
                format = ClipboardFormats.findByFile(victimSchem);
                reader = format.getReader(new FileInputStream(victimSchem));
                clipboard = reader.read();

                editSession = WorldEdit.getInstance().newEditSession(BukkitAdapter.adapt(Bukkit.getWorlds().get(0)));
                operation = new ClipboardHolder(clipboard)
                        .createPaste(editSession)
                        .to(BlockVector3.at(+50, 63, 0))
                        .ignoreAirBlocks(true)
                        .build();
                Operations.complete(operation);
                editSession.close();

            } catch (IOException | ExecutionException | WorldEditException | InterruptedException e) {
                e.printStackTrace();
            }
        }, 20 * 60L);
    }

    private void sendToSpawn(Crew crew) {
        for (UUID uuid : crew.getMembers().keySet()) {
            if (SkygotApi.get().isOnline(uuid)) {
                if(Bukkit.getPlayer(uuid) != null) {
                    SkygotApi.get().sendToServerRandom(uuid, "Lobby");
                    //Send message de teleport au hub
                    SkygotApi.get().sendPlayerMessage(uuid, "Un pillage va commencer... Vous avez été téléporté au spawn en attendant le début du pillage !");
                } else {
                    SkygotApi.get().sendPlayerMessage(uuid, "Un pillage va commencer... Vous n'avez plus accès à votre base jusqu'au début du pillage !");
                }
            }
        }
        if (SkygotApi.get().isOnline(crew.getOwner())) {
            if(Bukkit.getPlayer(crew.getOwner()) != null) {
                SkygotApi.get().sendToServerRandom(crew.getOwner(), "Lobby");
                //Send message de teleport au hub
                SkygotApi.get().sendPlayerMessage(crew.getOwner(), "Un pillage va commencer... Vous avez été téléporté au spawn en attendant le début du pillage.");
            } else {
                SkygotApi.get().sendPlayerMessage(crew.getOwner(), "Un pillage va commencer... Vous n'avez plus accès à votre base jusqu'au début du pillage !");
            }
        }
    }

    private void stopServer(Crew crew) {
        ServiceInfoSnapshot service = DRIVER.getCloudServiceProvider().getCloudServiceByName(crew.getServerId());

        if (service.isConnected()) {
            service.provider().stop();
        }
    }
}
