package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.regex.Pattern;

public class CreateCommand extends SubCommand {

    @Override
    public String getName() {
        return "create";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.create.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.create.usage[/lang]";
    }

    final private Pattern p = Pattern.compile("^[a-zA-Z]*$");

    @Override
    public void perform(Player player, String[] args) {
        if(!SpigotCore.getApi().hasCrew(player.getUniqueId())) {
            if (args.length > 1) {
                if (p.matcher(args[1]).find() && args[1].length() < 15 && args[1].length() > 3) {
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.create")
                        .replace("{fac}", args[1]));
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.create.generation"));
                    SpigotCore.getApi().createCrew(player.getUniqueId(), args[1]);
                    Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () -> {
                        SkyCore.getTeleportManager().teleportToServer(player, SpigotCore.getApi().getCrew(player.getUniqueId()).getServerId());
                    }, 15*20L);
                } else {
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.create.error"));
                }
            } else if (args.length == 1) {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.create.empty"));
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.create.exemple"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.create.alreadyown"));
        }
    }
}
