package fr.floxiik.skyapi.bukkit.skycrew.managers;

import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.io.BuiltInClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardWriter;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class CrewManager {

    public boolean hasCrew(UUID player) {
        return SpigotCore.getApi().getCrew(player) != null;
    }

    public Crew getCrew(UUID player) {
        if (hasCrew(player)) {
            return SpigotCore.getApi().getCrew(player);
        }
        return null;
    }

    public void saveCrewSchematic(Crew crew) {
        if (crew != null) {
            //FileOutputStream fos = null;
            File file = new File(crew.getId() + ".schem");

            BlockVector3 bot = BlockVector3.at(0, 63, 0);
            BlockVector3 top = BlockVector3.at(47, 215, 47); //MUST be a whole number eg integer

            CuboidRegion region = new CuboidRegion(BukkitAdapter.adapt(Bukkit.getWorld("crew")), bot, top);
            BlockArrayClipboard clipboard = new BlockArrayClipboard(region);

            try {
                ForwardExtentCopy forwardExtentCopy = new ForwardExtentCopy(BukkitAdapter.adapt(Bukkit.getWorld("crew")), region, clipboard, region.getMinimumPoint());
                Operations.complete(forwardExtentCopy);

                ClipboardWriter writer = BuiltInClipboardFormat.SPONGE_SCHEMATIC.getWriter(new FileOutputStream(file));
                writer.write(clipboard);
                writer.close();
                //fos.close();
            } catch (IOException | WorldEditException e) {
                e.printStackTrace();
            }
            SpigotCore.getApi().updateSchematic(crew, file);
        }
    }
}
