package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.providers.spigot.SpigotCrewProvider;
import org.bukkit.entity.Player;

import java.util.UUID;

public class DescCommand extends SubCommand {

    @Override
    public String getName() {
        return "desc";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.desc.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.desc.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if (SpigotCore.getApi().hasCrew(playerUUID)) {
            if (SpigotCore.getApi().isOwner(playerUUID)) {
                if (args.length > 1) {
                    StringBuilder message = new StringBuilder("");
                    args[0] = "";
                    for (String part : args) {
                        if (!message.toString().equals(""))
                            message.append(" ");
                        message.append(part);
                    }
                    Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                    crew.setDescription(message.toString());
                    new SpigotCrewProvider(playerUUID, crew.getId()).sendCrewToRedis(crew);
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.desc")
                        .replace("{desc}", message.toString()));
                } else if (args.length == 1) {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.specifydesc"));
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.desc.exemple"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needowner"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
