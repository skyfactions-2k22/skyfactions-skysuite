package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.entity.Player;

import java.util.UUID;

public class KickCommand extends SubCommand {

    @Override
    public String getName() {
        return "kick";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.kick.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.kick.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if(SpigotCore.getApi().hasCrew(playerUUID)) {
            if(SpigotCore.getApi().isOwner(playerUUID)) {
                if (args.length > 1) {
                    Account kicked = SpigotCore.getApi().getAccount(args[1]);
                    if(kicked == null) {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skyapi.playernotfound"));
                        return;
                    }
                    if (SpigotCore.getApi().getCrew(playerUUID).getMembers().containsKey(kicked.getUuid())) {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.kick")
                            .replace("{player}", kicked.getUsername()));
                        Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                        SpigotCore.getApi().removeInCrew(kicked.getUuid(), crew.getId());
                    } else {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.notmember"));
                    }
                } else if (args.length == 1) {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.specifyplayer"));
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.kick.exemple"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needowner"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
