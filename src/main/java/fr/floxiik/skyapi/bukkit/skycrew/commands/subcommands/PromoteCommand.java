package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.entity.Player;

import java.util.UUID;

public class PromoteCommand extends SubCommand {

    @Override
    public String getName() {
        return "promote";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.promote.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.promote.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if(SpigotCore.getApi().hasCrew(playerUUID)) {
            if(SpigotCore.getApi().isOwner(playerUUID)) {
                if (args.length > 1) {
                    Account promoted = SpigotCore.getApi().getAccount(args[1]);
                    if(promoted == null) {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skyapi.playernotfound"));
                        return;
                    }
                    if(SpigotCore.getApi().isOwner(promoted.getUuid())) {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.promote.owner.error"));
                        return;
                    }
                    Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                    if (crew.getMembers().containsKey(promoted.getUuid())) {
                        switch (crew.getMembers().get(promoted.getUuid())) {
                            case "recruit":
                                SpigotCore.getApi().setRank(promoted.getUuid(), crew.getId(), "member");
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.promote.member")
                                    .replace("{player}", promoted.getUsername()));
                                break;
                            case "member":
                                SpigotCore.getApi().setRank(promoted.getUuid(), crew.getId(), "moderator");
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.promote.moderator")
                                        .replace("{player}", promoted.getUsername()));
                                break;
                            case "moderator":
                                SpigotCore.getApi().setRank(promoted.getUuid(), crew.getId(), "co-leader");
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.promote.coleader")
                                        .replace("{player}", promoted.getUsername()));
                                break;
                            default:
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.promote.highest"));
                                break;
                        }
                    } else {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.notmember"));
                    }
                } else if (args.length == 1) {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.specifyplayer"));
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.promote.exemple"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needowner"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
