package fr.floxiik.skyapi.bukkit.skycrew.listeners;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.SkyCrew;
import fr.floxiik.skyapi.bukkit.skycrew.utils.LocationUtils;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.UUID;

public class CrewBlockBreakListener implements Listener {

    @EventHandler
    public void onPlace(BlockBreakEvent event) {
        if (SkyCrew.getInstance().isCrewInstance()) {
            Player player = event.getPlayer();
            UUID playerUUID = player.getUniqueId();
            if (SpigotCore.getApi().hasCrew(playerUUID)) {
                Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                if (SpigotCore.getApi().isInCrew(playerUUID, Integer.parseInt(SpigotCore.getServerName().replace("-1", "")))) {
                    if (SpigotCore.getServerName().equals(crew.getServerId())) {
                        Location location = event.getBlock().getLocation();
                        if (LocationUtils.isInZone(location, crew.getLevel()) && location.getY() >= 63) {
                            event.setCancelled(false);
                        } else {
                            event.setCancelled(true);
                            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.break.deny"));
                        }
                    } else {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.break.other.deny"));
                    }
                } else {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.break.other.deny"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.break.other.deny"));
            }
        }
    }
}
