package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.entity.Player;

import java.util.UUID;

public class DelHomeCommand extends SubCommand {

    @Override
    public String getName() {
        return "delwarp";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.delwarp.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.delwarp.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if (SpigotCore.getApi().hasCrew(playerUUID)) {
            if (SpigotCore.getApi().isOwner(playerUUID)) {
                Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                if (args.length > 1) {
                    if (crew.getHomes().containsKey(args[1])) {
                        SpigotCore.getApi().delHome(playerUUID, crew.getId(), args[1]);
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.warp.delete")
                            .replace("{warp}", args[1]));
                    } else {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.warp.nofound"));
                    }
                } else {
                    //afficher la liste des warps à supprimer
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needowner"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
