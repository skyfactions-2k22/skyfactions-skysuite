package fr.floxiik.skyapi.bukkit.skycrew.listeners;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class CrewEntityDamageByEntityListener implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player victim = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();
            Account victimA = SpigotCore.getApi().getAccount(victim.getUniqueId());
            Account damagerA = SpigotCore.getApi().getAccount(damager.getUniqueId());
            if((victimA.getCrew() == damagerA.getCrew()) && (victimA.getCrew()+damagerA.getCrew() != 0)) {
                event.setCancelled(true);
                damager.sendMessage(LangAPI.get().parse(damager.getUniqueId(), "skycrew.combat.member.error"));
            }
        }
    }
}
