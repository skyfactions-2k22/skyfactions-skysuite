package fr.floxiik.skyapi.bukkit.skycrew.commands;

import fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandManager implements CommandExecutor {

    public ArrayList<SubCommand> subcommands = new ArrayList<>();

    public CommandManager() {
        subcommands.add(new CreateCommand());
        subcommands.add(new DisbandCommand());
        subcommands.add(new InviteCommand());
        subcommands.add(new LeaveCommand());
        subcommands.add(new PromoteCommand());
        subcommands.add(new DemoteCommand());
        subcommands.add(new DescCommand());
        subcommands.add(new KickCommand());
        subcommands.add(new HomeCommand());
        subcommands.add(new SetWarpCommand());
        subcommands.add(new DelHomeCommand());
        subcommands.add(new WarpCommand());
        subcommands.add(new ShowCommand());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player p = (Player) sender;

            if (args.length == 0 || args[0].equals("help") || args[0].equals("aide")) {
                p.sendMessage("§8§m------------------------------------");
                for (int i = 0; i < getSubcommands().size(); i++) {
                    p.sendMessage(" §7• §6" + getSubcommands().get(i).getSyntax() + " §7- §e" + getSubcommands().get(i).getDescription());
                }
                p.sendMessage("§8§m------------------------------------");
            } else {
                for (int i = 0; i < getSubcommands().size(); i++) {
                    if (args[0].equalsIgnoreCase(getSubcommands().get(i).getName())) {
                        getSubcommands().get(i).perform(p, args);
                    }
                }
            }

        }


        return true;
    }

    public ArrayList<SubCommand> getSubcommands() {
        return subcommands;
    }
}
