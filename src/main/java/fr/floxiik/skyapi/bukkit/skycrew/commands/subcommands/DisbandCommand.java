package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.regex.Pattern;

public class DisbandCommand extends SubCommand {

    @Override
    public String getName() {
        return "disband";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.disband.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.disband.usage[/lang]";
    }

    final private Pattern p = Pattern.compile("^[a-zA-Z]*$");

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if(SpigotCore.getApi().hasCrew(playerUUID)) {
            if(SpigotCore.getApi().isOwner(playerUUID)) {
                if (args.length > 1) {
                    if (args[1].equals(LangAPI.get().parse(playerUUID, "skycrew.confirm"))) {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.disband")
                            .replace("{fac}", SpigotCore.getApi().getCrew(playerUUID).getName()));
                        SpigotCore.getApi().deleteCrew(player.getUniqueId());
                    } else {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.disband.question"));
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.disband.exemple"));
                    }
                } else if (args.length == 1) {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.disband.question"));
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.disband.exemple"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needowner"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
