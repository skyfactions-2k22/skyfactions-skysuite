package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import org.bukkit.entity.Player;

import java.util.regex.Pattern;

public class InviteCommand extends SubCommand {

    @Override
    public String getName() {
        return "invite";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.invite.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.invite.usage[/lang]";
    }

    final private Pattern p = Pattern.compile("^[a-zA-Z]*$");

    @Override
    public void perform(Player player, String[] args) {
        if(SpigotCore.getApi().hasCrew(player.getUniqueId())) {

            final ByteArrayDataOutput out = ByteStreams.newDataOutput();

            out.writeUTF("InviteMember");
            out.writeUTF(args[1]);
            out.writeUTF(player.getName());

            player.sendPluginMessage(SpigotCore.getInstance(), "skycrew:crewchannel", out.toByteArray());

        } else {
            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skycrew.nofac"));
        }
    }
}
