package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.entity.Player;

import java.util.UUID;

public class DemoteCommand extends SubCommand {

    @Override
    public String getName() {
        return "demote";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.demote.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.demote.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if(SpigotCore.getApi().hasCrew(playerUUID)) {
            if(SpigotCore.getApi().isOwner(playerUUID)) {
                if (args.length > 1) {
                    Account demoted = SpigotCore.getApi().getAccount(args[1]);
                    if(demoted == null) {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skyapi.playernotfound"));
                        return;
                    }
                    if(SpigotCore.getApi().isOwner(demoted.getUuid())) {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.demote.owner.error"));
                        return;
                    }
                    Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                    if (crew.getMembers().containsKey(demoted.getUuid())) {
                        switch (crew.getMembers().get(demoted.getUuid())) {
                            case "co-leader":
                                SpigotCore.getApi().setRank(demoted.getUuid(), crew.getId(), "moderator");
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.demote.moderator")
                                    .replace("{player}", demoted.getUsername()));
                                break;
                            case "member":
                                SpigotCore.getApi().setRank(demoted.getUuid(), crew.getId(), "recruit");
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.demote.recruit")
                                        .replace("{player}", demoted.getUsername()));
                            case "moderator":
                                SpigotCore.getApi().setRank(demoted.getUuid(), crew.getId(), "member");
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.demote.member")
                                        .replace("{player}", demoted.getUsername()));
                            default:
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.demote.lower"));
                                break;
                        }
                    } else {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.notmember"));
                    }
                } else if (args.length == 1) {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.specifyplayer"));
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.demote.exemple"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needowner"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
