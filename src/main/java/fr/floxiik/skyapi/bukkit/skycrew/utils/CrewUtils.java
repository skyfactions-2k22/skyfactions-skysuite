package fr.floxiik.skyapi.bukkit.skycrew.utils;

public class CrewUtils {

    public static String getRank(String rank) {
        switch (rank) {
            case "recruit":
                return "banana";
            case "member":
                return "stars";
            case "moderator":
                return "star";
            case "co-leader":
                return "crown";
            default:
                return "crown";
        }
    }
}
