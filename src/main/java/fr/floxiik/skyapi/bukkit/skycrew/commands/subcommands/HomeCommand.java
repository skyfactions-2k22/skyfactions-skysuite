package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

public class HomeCommand extends SubCommand {

    @Override
    public String getName() {
        return "home";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.home.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.home.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if (SpigotCore.getApi().hasCrew(playerUUID)) {
            Crew crew = SpigotCore.getApi().getCrew(playerUUID);
            Home home = crew.getHomes().get("home");
            if (SpigotCore.getServerName().equals(crew.getServerId())) {

                player.teleport(new Location(Bukkit.getWorlds().get(0), home.getX(), home.getY(), home.getZ(), home.getYaw(), home.getPitch()));
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.home"));
            } else {
                SkyCore.getTeleportManager().teleportToLocation(SpigotCore.getApi().getAccount(playerUUID), home);

                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.home"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
