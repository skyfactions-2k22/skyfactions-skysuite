package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.entity.Player;

import java.util.UUID;

public class LeaveCommand extends SubCommand {

    @Override
    public String getName() {
        return "leave";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.leave.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.leave.usage[/lang]";
    }

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if(SpigotCore.getApi().hasCrew(player.getUniqueId())) {
            if (args.length > 1) {
                if (args[1].equals(LangAPI.get().parse(playerUUID, "skycrew.confirm"))) {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.leave")
                            .replace("{fac}", SpigotCore.getApi().getCrew(playerUUID).getName()));
                    Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                    SpigotCore.getApi().removeInCrew(playerUUID, crew.getId());
                } else {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.leave.question"));
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.leave.exemple"));
                }
            } else if (args.length == 1) {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.leave.question"));
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.leave.exemple"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
