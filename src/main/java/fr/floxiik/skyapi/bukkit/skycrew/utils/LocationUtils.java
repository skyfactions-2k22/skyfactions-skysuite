package fr.floxiik.skyapi.bukkit.skycrew.utils;

import org.bukkit.Location;

public class LocationUtils {

    public static boolean isInZone(Location location, int level) {
        switch (level) {
            case 1:
                return (location.getChunk().getX() == 1) && (location.getChunk().getZ() == 1);
            case 2:
                return ((location.getChunk().getX() == 1) && (location.getChunk().getZ() == 1))
                        || ((location.getChunk().getX() == 1) && (location.getChunk().getZ() == 2))
                        || ((location.getChunk().getX() == 0) && (location.getChunk().getZ() == 2))
                        || ((location.getChunk().getX() == 0) && (location.getChunk().getZ() == 1));
            case 3:
                return ((location.getChunk().getX() == 1) && (location.getChunk().getZ() == 1))
                        || ((location.getChunk().getX() == 1) && (location.getChunk().getZ() == 2))
                        || ((location.getChunk().getX() == 0) && (location.getChunk().getZ() == 2))
                        || ((location.getChunk().getX() == 0) && (location.getChunk().getZ() == 1))
                        || ((location.getChunk().getX() == 2) && (location.getChunk().getZ() == 2))
                        || ((location.getChunk().getX() == 2) && (location.getChunk().getZ() == 1))
                        || ((location.getChunk().getX() == 2) && (location.getChunk().getZ() == 0))
                        || ((location.getChunk().getX() == 1) && (location.getChunk().getZ() == 0))
                        || ((location.getChunk().getX() == 0) && (location.getChunk().getZ() == 0));
            default:
                return false;
        }
    }
}
