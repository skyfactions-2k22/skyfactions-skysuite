package fr.floxiik.skyapi.bukkit.skycrew.commands.subcommands;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.commands.SubCommand;
import fr.floxiik.skyapi.bukkit.skycrew.utils.LocationUtils;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.regex.Pattern;

public class SetWarpCommand extends SubCommand {

    @Override
    public String getName() {
        return "setwarp";
    }

    @Override
    public String getDescription() {
        return "[lang]skycrew.setwarp.desc[/lang]";
    }

    @Override
    public String getSyntax() {
        return "[lang]skycrew.setwarp.usage[/lang]";
    }

    final private Pattern p = Pattern.compile("^[a-zA-Z]*$");

    @Override
    public void perform(Player player, String[] args) {
        UUID playerUUID = player.getUniqueId();
        if(SpigotCore.getApi().hasCrew(playerUUID)) {
            if(SpigotCore.getApi().isOwner(playerUUID)) {
                Crew crew = SpigotCore.getApi().getCrew(playerUUID);
                if(SpigotCore.getServerName().equals(crew.getServerId())) {
                    Bukkit.broadcastMessage(SpigotCore.getServerName());
                    Bukkit.broadcastMessage(crew.getServerId());

                    if(LocationUtils.isInZone(player.getLocation(), crew.getLevel())) {
                        if (args.length > 1) {
                            if (p.matcher(args[1]).find() && args[1].length() < 15 && args[1].length() > 3) {
                                Location location = player.getLocation();
                                Home home = new Home(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
                                SpigotCore.getApi().setHome(playerUUID, crew.getId(), args[1], home);
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.setwarp")
                                    .replace("{warp}", args[1]));
                            } else {
                                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.specifytext"));
                            }
                        } else {
                            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.specifywarp"));
                        }
                    } else {
                        player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needinplot"));
                    }
                } else {
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needhome"));
                    player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.home.exemple"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.needowner"));
            }
        } else {
            player.sendMessage(LangAPI.get().parse(playerUUID, "skycrew.nofac"));
        }
    }
}
