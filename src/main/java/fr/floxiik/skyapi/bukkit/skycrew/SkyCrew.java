package fr.floxiik.skyapi.bukkit.skycrew;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.listeners.EntityDamageByEntityListener;
import fr.floxiik.skyapi.bukkit.skycrew.commands.CommandManager;
import fr.floxiik.skyapi.bukkit.skycrew.listeners.*;
import fr.floxiik.skyapi.bukkit.skycrew.managers.CrewManager;
import fr.floxiik.skyapi.bukkit.skycrew.managers.VersusManager;
import fr.floxiik.skyapi.bukkit.skycrew.placeholders.CrewPlaceholders;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

public class SkyCrew {

    private static CrewManager crewManager;
    private static VersusManager versusManager;
    private static SkyCrew instance;
    private String version = "1.0.0";

    public void init(SpigotCore spigotCore) {
        instance = this;
        crewManager = new CrewManager();
        versusManager = new VersusManager();

        PluginManager pm = Bukkit.getPluginManager();
        spigotCore.getCommand("crew").setExecutor(new CommandManager());
        if(pm.getPlugin("PlaceholderAPI") != null) {
            new CrewPlaceholders().register();
        }
        pm.registerEvents(new CrewBlockPlaceListener(), spigotCore);
        pm.registerEvents(new CrewBlockBreakListener(), spigotCore);
        pm.registerEvents(new CrewPlayerJoinListener(), spigotCore);
        pm.registerEvents(new EntityDamageByEntityListener(), spigotCore);
        pm.registerEvents(new VersusListeners(), spigotCore);
        pm.registerEvents(new CrewPlayerQuitListener(), spigotCore);
        spigotCore.getServer().getMessenger().registerOutgoingPluginChannel(spigotCore, "skycrew:crewchannel");
        spigotCore.getServer().getMessenger().registerIncomingPluginChannel(spigotCore, "skycrew:crewchannel", new CrewMessageListener());

        sendLog(true);

        Bukkit.getScheduler().runTaskTimer(spigotCore, () -> {
            for(Player player : Bukkit.getOnlinePlayers()) {
                SpigotCore.getApi().saveAccount(player.getUniqueId(), false, false);
            }
        }, 0, 20*60);
    }

    public void disable(SpigotCore spigotCore) {
        /*if(isCrewInstance()) {
            getCrewManager().saveCrewSchematic(null);
        }*/
        sendLog(false);
    }

    public void stopServer(Crew crew) {
        getCrewManager().saveCrewSchematic(crew);
        Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () -> {
            Bukkit.getServer().shutdown();
        }, 20L);
    }

    public static SkyCrew getInstance() {
        return instance;
    }

    public static CrewManager getCrewManager() {
        return crewManager;
    }

    public static VersusManager getVersusManager() {
        return versusManager;
    }

    public String getVersion() {
        return version;
    }

    public boolean isCrewInstance() {
        String[] strings = SpigotCore.getServerName().split("-");
        try {
            Integer.parseInt(strings[0]);
        } catch(NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }

    public void sendLog(boolean start) {
        SpigotCore.getInstance().getLogger().info("<==================================>");
        SpigotCore.getInstance().getLogger().info("");
        if(start) SpigotCore.getInstance().getLogger().info("  Module (SkyCrew) Chargé avec succès !");
        if(!start) SpigotCore.getInstance().getLogger().info("  Module (SkyCrew) Désactivé avec succès !");
        SpigotCore.getInstance().getLogger().info("  Version: " + getVersion());
        SpigotCore.getInstance().getLogger().info("  Autheur: Achereth");
        SpigotCore.getInstance().getLogger().info("");
        SpigotCore.getInstance().getLogger().info("<==================================>");
    }
}
