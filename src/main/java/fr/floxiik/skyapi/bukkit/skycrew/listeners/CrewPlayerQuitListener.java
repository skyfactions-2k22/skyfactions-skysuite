package fr.floxiik.skyapi.bukkit.skycrew.listeners;

import fr.floxiik.skyapi.api.SkygotApi;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycrew.SkyCrew;
import fr.floxiik.skyapi.bukkit.skycrew.managers.CrewManager;
import fr.floxiik.skyapi.data.Crew;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public class CrewPlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if(SkyCrew.getInstance().isCrewInstance()) {
            Crew crew = SpigotCore.getApi().getCrew(Integer.parseInt(SpigotCore.getServerName().replace("-1", "")));
            boolean stop = true;
            for(UUID uuid : crew.getMembers().keySet()) {
                if(SkygotApi.get().isOnline(uuid)) {
                    stop = false;
                }
            }
            if(stop) {
                SkyCrew.getInstance().stopServer(crew);
            }
        }
    }
}
