package fr.floxiik.skyapi.bukkit;

import fr.floxiik.skyapi.api.SkygotApi;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.FastInvManager;
import fr.floxiik.skyapi.bukkit.skycrew.SkyCrew;
import fr.floxiik.skyapi.bukkit.skyshop.SkyShop;
import fr.floxiik.skyapi.data.redis.RedisAccess;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class SpigotCore extends JavaPlugin {

    public static SpigotCore instance;
    private static SkygotApi api;
    public static boolean debug = true;
    private static String serverName;

    @Override
    public void onEnable() {

        instance = this;
        api = SkygotApi.get();
        serverName = getServerString();

        sendName();

        RedisAccess.init();

        new SkyCrew().init(this);
        new SkyCore().init(this);
        new SkyShop().init(this);

        FastInvManager.register(this);

        sendLog(true);
    }

    @Override
    public void onDisable() {

        sendName();

        SkyCrew.getInstance().disable(this);
        SkyCore.getInstance().disable(this);
        SkyShop.getInstance().disable(this);

        sendLog(false);

        RedisAccess.close();
    }

    private String getServerString() {
        File file = new File("server.properties");
        BufferedReader read = null;
        try {
            read = new BufferedReader(new FileReader(file));
            String line;
            while ((line = read.readLine()) != null) {
                if (line.startsWith("server-name=")) {
                    return line.replace("server-name=", "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "null";
    }

    public static String getServerName() {
        return serverName;
    }

    public static SkygotApi getApi() {
        return api;
    }

    public static SpigotCore getInstance() {
        return instance;
    }

    public void sendName() {
        SpigotCore.getInstance().getLogger().info("");
        SpigotCore.getInstance().getLogger().info("███████╗██╗  ██╗██╗   ██╗███████╗██╗   ██╗██╗████████╗███████╗");
        SpigotCore.getInstance().getLogger().info("██╔════╝██║ ██╔╝╚██╗ ██╔╝██╔════╝██║   ██║██║╚══██╔══╝██╔════╝");
        SpigotCore.getInstance().getLogger().info("███████╗█████╔╝  ╚████╔╝ ███████╗██║   ██║██║   ██║   █████╗  ");
        SpigotCore.getInstance().getLogger().info("╚════██║██╔═██╗   ╚██╔╝  ╚════██║██║   ██║██║   ██║   ██╔══╝  ");
        SpigotCore.getInstance().getLogger().info("███████║██║  ██╗   ██║   ███████║╚██████╔╝██║   ██║   ███████╗");
        SpigotCore.getInstance().getLogger().info("");
    }

    public void sendLog(boolean start) {
        SpigotCore.getInstance().getLogger().info("<==================================>");
        SpigotCore.getInstance().getLogger().info("");
        if(start) SpigotCore.getInstance().getLogger().info("  Plugin (SkySuite) Chargé avec succès !");
        if(!start) SpigotCore.getInstance().getLogger().info("  Plugin (SkySuite) Désactivé avec succès !");
        SpigotCore.getInstance().getLogger().info("  Version: " + this.getDescription().getVersion());
        SpigotCore.getInstance().getLogger().info("  Autheur: Achereth");
        SpigotCore.getInstance().getLogger().info("");
        SpigotCore.getInstance().getLogger().info("<==================================>");
    }
}
