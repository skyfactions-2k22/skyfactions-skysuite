package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.api.LangAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.HashMap;
import java.util.UUID;

public class AntiSpam implements Listener {

    private HashMap<UUID, Long> cooldowns = new HashMap<>();
    //private HashMap<UUID, Long> cooldowns2 = new HashMap<>();

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        UUID playerUUID = player.getUniqueId();
        if(event.getMessage().startsWith("/rtp") || event.getMessage().startsWith("/randomtp")) {
            /*if (cooldowns2.containsKey(playerUUID)) {
                if (cooldowns2.get(playerUUID) + 120000 > System.currentTimeMillis()) {
                    event.setCancelled(true);
                    player.sendMessage("§cVous devez attendre §e2min §centre chaque téléportation !");
                } else {
                    cooldowns2.replace(playerUUID, System.currentTimeMillis());
                }
            } else {
                cooldowns.put(playerUUID, System.currentTimeMillis());
            }*/
        } else {
            if (cooldowns.containsKey(playerUUID)) {
                if (cooldowns.get(playerUUID) + 1000 > System.currentTimeMillis()) {
                    event.setCancelled(true);
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.cooldown.command"));
                } else {
                    cooldowns.replace(playerUUID, System.currentTimeMillis());
                }
            } else {
                cooldowns.put(playerUUID, System.currentTimeMillis());
            }
        }
    }
}
