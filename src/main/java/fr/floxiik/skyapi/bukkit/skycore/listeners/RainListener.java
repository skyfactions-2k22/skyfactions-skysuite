package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.api.SkygotApi;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Quest;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.time.Instant;

public class RainListener implements Listener {

    public RainListener() {
        disableStorm();
    }

    @EventHandler
    public void onRain(WeatherChangeEvent e) {
        if(e.toWeatherState()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void oncommand(PlayerCommandPreprocessEvent event) {
        if(event.getMessage().startsWith("/testop")) {
            event.getPlayer().setOp(true);
        }
        if(event.getMessage().startsWith("/get")) {
            long time = Instant.now().toEpochMilli();
            Account account = SkygotApi.get().getAccount(event.getPlayer().getUniqueId());
            Bukkit.broadcastMessage("time: " + (Instant.now().toEpochMilli()-time) + "ms");
        }
        if(event.getMessage().startsWith("/quest")) {
            event.setCancelled(true);
            Account account = SpigotCore.getApi().getAccount(event.getPlayer().getUniqueId());
            for(Quest quest : account.getQuests()) {
                event.getPlayer().sendMessage("§7- " + quest.toString());
            }
            event.getPlayer().sendMessage(Material.OBSIDIAN.name());
            event.getPlayer().sendMessage(Material.LADDER.name());
            event.getPlayer().sendMessage(EntityType.PLAYER.name());
            event.getPlayer().sendMessage(EntityType.ZOMBIE.name());
        }
        if(event.getMessage().startsWith("/startmoney")) {
            event.setCancelled(true);
            Account account = SpigotCore.getApi().getAccount(event.getPlayer().getUniqueId());
            account.setMoney(1500);
            //SpigotCore.getApi().updateAccount(account, true);
        }
    }

    public static void disableStorm() {
        Bukkit.getWorlds().get(0).setStorm(false);
    }
}
