package fr.floxiik.skyapi.bukkit.skycore.jobs;

public enum JobsEnum {
    FARMER("farmer", 30),
    MINER("miner", 30),
    WOODCUTTER("woodcutter", 30),
    TRADER("trader", 20),
    LOOTER("looter", 30),
    MURDER("murder", 30),
    HUNTER("hunter", 30),
    ARCHER("archer", 20),
    ALCHEMIST("alchemist", 20),
    EVENT("event", 20);
    
    private String libelle;
    private int maxLevel;

    JobsEnum(String libelle, int maxLevel) {
        this.libelle = libelle;
        this.maxLevel = maxLevel;
    }

    public String getLibelle() {
        return libelle;
    }

    public int getMaxLevel() {
        return maxLevel;
    }
}
