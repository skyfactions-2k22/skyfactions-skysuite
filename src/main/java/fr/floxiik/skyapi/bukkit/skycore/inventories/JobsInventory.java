package fr.floxiik.skyapi.bukkit.skycore.inventories;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.jobs.JobsEnum;
import fr.floxiik.skyapi.bukkit.skycore.utils.ProgressBar;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.FastInv;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.ItemBuilder;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.Lore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JobsInventory extends FastInv {

    private boolean preventClose = false;
    private final Player player;
    private final Account account;

    public JobsInventory(Player player) {
        super(54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.title.main") + " §8» §9Métiers");
        this.player = player;
        this.account = SpigotCore.getApi().getAccount(player.getUniqueId());

        setItem(10, new ItemBuilder(Material.WHEAT).name("§7• §fMétier §2Farmeur §7•").addLore(getJobLore(JobsEnum.FARMER.getLibelle())).build());
        setItem(19, new ItemBuilder(Material.BONE).name("§7• §fMétier §cAssassin §7•").addLore(getJobLore(JobsEnum.MURDER.getLibelle())).build());
        setItem(28, new ItemBuilder(Material.EXPERIENCE_BOTTLE).name("§7• §fMétier §eMarchand §7•").addLore(getJobLore(JobsEnum.TRADER.getLibelle())).build());
        setItem(37, new ItemBuilder(Material.TNT).name("§7• §fMétier §6Pilleur §7•").addLore(getJobLore(JobsEnum.LOOTER.getLibelle())).build());

        setItem(22, new ItemBuilder(Material.DIAMOND_PICKAXE).name("§7• §fMétier §aMineur §7•").addLore(getJobLore(JobsEnum.MINER.getLibelle())).enchant(Enchantment.DIG_SPEED, 5).flags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES).build());
        setItem(23, new ItemBuilder(Material.IRON_AXE).name("§7• §fMétier §cBûcheron §7•").addLore(getJobLore(JobsEnum.WOODCUTTER.getLibelle())).enchant(Enchantment.DIG_SPEED, 5).flags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES).build());
        setItem(24, new ItemBuilder(Material.STONE_SHOVEL).name("§7• §fMétier §eAgriculteur §7•").addLore(getJobLore(JobsEnum.FARMER.getLibelle())).enchant(Enchantment.DIG_SPEED, 5).flags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES).build());
        setItem(31, new ItemBuilder(Material.IRON_SWORD).name("§7• §fMétier §cChasseur §7•").addLore(getJobLore(JobsEnum.HUNTER.getLibelle())).enchant(Enchantment.DIG_SPEED, 5).flags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES).build());
        setItem(32, new ItemBuilder(Material.BOW).name("§7• §fMétier §3Archer §7•").addLore(getJobLore(JobsEnum.ARCHER.getLibelle())).enchant(Enchantment.DIG_SPEED, 5).flags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES).build());
        setItem(33, new ItemBuilder(Material.POTION).name("§7• §fMétier §bAlchimiste §7•").addLore(getJobLore(JobsEnum.ALCHEMIST.getLibelle())).flags(ItemFlag.HIDE_POTION_EFFECTS).data(8233).build());


        // Add some blocks to the borders
        int color = ColorEnum.getServerData();
        setItems(getBorders(), new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(11, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(12, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(13, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(14, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(20, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(29, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(38, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(41, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(42, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(43, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());

        setItem(49, new ItemBuilder(Material.BARRIER).name("§7• §cRetour §7•").build());

        ItemStack i = new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3);
        SkullMeta im = (SkullMeta) i.getItemMeta();
        im.setOwner(player.getName());
        im.setLore(Lore.getProfileLore(this.account));
        im.setDisplayName("§7• §bProfil §7•");
        i.setItemMeta(im);
        setItem(4, new ItemBuilder(i).build());

        // Prevent from closing when preventClose is to true
        setCloseFilter(p -> this.preventClose);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You opened the inventory");
    }

    @Override
    public void onClose(InventoryCloseEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You closed the inventory");
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        if (event.getSlot() == 10) {

        } else if (event.getSlot() == 49) {
            new MenuInventory(player).open(player);
        }
    }

    private List<String> getJobLore(String job) {
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add("  §6Level " + account.getLevel(job) + " §7- §e" + round(account.getLevel(job)) + "§6xp");
        lore.add(" §7[" + ProgressBar.getProgressBar(account.getLevelPourcent(job), 100, 40, '|', "§a", "§8") + "§7]");
        lore.add("");
        lore.add(" §7• §eNiveau suivant: §c" + (account.getLevel(job) + 1));
        return lore;
    }

    private float round(float f) {
        BigDecimal bigDecimal = new BigDecimal(Float.toString(f));
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        return bigDecimal.floatValue();
    }
}