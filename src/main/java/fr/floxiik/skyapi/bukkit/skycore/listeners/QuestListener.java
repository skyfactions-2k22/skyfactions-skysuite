package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Quest;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class QuestListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if(event.getSlotType() == InventoryType.SlotType.RESULT){
            ItemStack item = event.getCurrentItem();
            if(item != null) {
                Account account = SpigotCore.getApi().getAccount(player.getUniqueId());
                ArrayList<Quest> quests = account.getQuests();
                ArrayList<Quest> actualisedQuests = new ArrayList<>();
                for(Quest quest : quests) {
                    if(quest.getType().equals("cr")) {
                        if(quest.getTargetType().equals(item.getType().name())) {
                            if(!quest.isCompleted()) {
                                quest.setCurrent(quest.getCurrent() + 1);
                            }
                        }
                    }
                    actualisedQuests.add(quest);
                }
                account.setQuests(actualisedQuests);
                SpigotCore.getApi().setAccount(account, true, false);
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();

        Account account = SpigotCore.getApi().getAccount(player.getUniqueId());
        ArrayList<Quest> quests = account.getQuests();
        ArrayList<Quest> actualisedQuests = new ArrayList<>();
        for(Quest quest : quests) {
            if(quest.getType().equals("b") || quest.getType().equals("c")) {
                if(quest.getTargetType().equals(event.getBlock().getType().name())) {
                    if(!quest.isCompleted()) {
                        quest.setCurrent(quest.getCurrent() + 1);
                    }
                }
            }
            actualisedQuests.add(quest);
        }
        account.setQuests(actualisedQuests);
        if(!quests.equals(actualisedQuests)) {
            SpigotCore.getApi().setAccount(account, true, false);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();

        Account account = SpigotCore.getApi().getAccount(player.getUniqueId());
        ArrayList<Quest> quests = account.getQuests();
        ArrayList<Quest> actualisedQuests = new ArrayList<>();
        for(Quest quest : quests) {
            if(quest.getType().equals("p")) {
                if(quest.getTargetType().equals(event.getBlockPlaced().getType().name())) {
                    if(!quest.isCompleted()) {
                        quest.setCurrent(quest.getCurrent() + 1);
                    }
                }
            }
            actualisedQuests.add(quest);
        }
        account.setQuests(actualisedQuests);
        if(!quests.equals(actualisedQuests)) {
            SpigotCore.getApi().setAccount(account, true, false);
        }
    }

    @EventHandler
    public void onKill(EntityDeathEvent event) {
        Player player = event.getEntity().getKiller();
        if(player != null) {
            Account account = SpigotCore.getApi().getAccount(player.getUniqueId());
            ArrayList<Quest> quests = account.getQuests();
            ArrayList<Quest> actualisedQuests = new ArrayList<>();
            for (Quest quest : quests) {
                if (quest.getType().equals("k")) {
                    if (quest.getTargetType().equals(event.getEntity().getType().name())) {
                        if(!quest.isCompleted()) {
                            quest.setCurrent(quest.getCurrent() + 1);
                        }
                    }
                }
                actualisedQuests.add(quest);
            }
            account.setQuests(actualisedQuests);
            if(!quests.equals(actualisedQuests)) {
                SpigotCore.getApi().setAccount(account, true, false);
            }
        }
    }
}
