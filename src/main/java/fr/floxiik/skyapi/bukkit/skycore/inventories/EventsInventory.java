package fr.floxiik.skyapi.bukkit.skycore.inventories;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.FastInv;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.ItemBuilder;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.Lore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class EventsInventory extends FastInv {

    private boolean preventClose = false;
    private final Player player;
    private Account account;

    public EventsInventory(Player player) {
        super(54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.title.main") + " §8» §9Events");
        this.player = player;
        this.account = SpigotCore.getApi().getAccount(player.getUniqueId());

        /*setItem(10, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2E1MTZmYmFlMTYwNThmMjUxYWVmOWE2OGQzMDc4NTQ5ZjQ4ZjZkNWI2ODNmMTljZjVhMTc0NTIxN2Q3MmNjIn19fQ==")).name("§e§lLUNDI").build());
        setItem(11, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDY5OGFkZDM5Y2Y5ZTRlYTkyZDQyZmFkZWZkZWMzYmU4YTdkYWZhMTFmYjM1OWRlNzUyZTlmNTRhZWNlZGM5YSJ9fX0=")).name("§e§lMARDI").build());
        setItem(12, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmQ5ZTRjZDVlMWI5ZjNjOGQ2Y2E1YTFiZjQ1ZDg2ZWRkMWQ1MWU1MzVkYmY4NTVmZTlkMmY1ZDRjZmZjZDIifX19")).name("§e§lMERCREDI").build());
        setItem(13, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjJhM2Q1Mzg5ODE0MWM1OGQ1YWNiY2ZjODc0NjlhODdkNDhjNWMxZmM4MmZiNGU3MmY3MDE1YTM2NDgwNTgifX19")).name("§e§lJEUDI").build());
        setItem(14, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDFmZTM2YzQxMDQyNDdjODdlYmZkMzU4YWU2Y2E3ODA5YjYxYWZmZDYyNDVmYTk4NDA2OTI3NWQxY2JhNzYzIn19fQ==")).name("§e§lVENDREDI").build());
        setItem(15, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzVkYTFjYjZjNGMyMzcxMDIyNGI0ZjRlOGQ2ZmZjZjhiNGI1NWY3ZmU4OTFjMTIwNGFmNzQ4NWNmMjUyYTFkOCJ9fX0=")).name("§e§lSAMEDI").build());
        setItem(16, new ItemBuilder(SkullCreator.itemFromBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWY0ZTdhNWNmNWI1YTRkMmZmNGZiMDQzM2IxYTY4NzUxYWExMmU5YTAyMWQzOTE4ZTkyZTIxOWE5NTNiIn19fQ==")).name("§e§lDIMANCHE").build());
*/

        // Add some blocks to the borders
        int color = ColorEnum.getServerData();
        setItems(getBorders(), new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());

        setItem(49, new ItemBuilder(Material.BARRIER).name("§7• §cRetour §7•").build());

        ItemStack i = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
        SkullMeta im = (SkullMeta) i.getItemMeta();
        im.setOwner(player.getName());
        im.setLore(Lore.getProfileLore(this.account));
        im.setDisplayName("§7• §bProfil §7•");
        i.setItemMeta(im);
        setItem(4, new ItemBuilder(i).build());

        // Prevent from closing when preventClose is to true
        setCloseFilter(p -> this.preventClose);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You opened the inventory");
    }

    @Override
    public void onClose(InventoryCloseEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You closed the inventory");
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        if (event.getSlot() == 10) {

        } else if (event.getSlot() == 49) {
            new MenuInventory(player).open(player);
        }
    }
}