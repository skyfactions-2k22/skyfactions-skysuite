package fr.floxiik.skyapi.bukkit.skycore.commands.extra;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Clear implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.clear")) {
                if (args.length > 0 && player.hasPermission("skycore.clear.other")) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        SkyCore.getPlayerManager().clearPlayer(target);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.clear.other")
                                .replace("{player}", target.getName()));
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    SkyCore.getPlayerManager().clearPlayer(player);
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.clear"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
