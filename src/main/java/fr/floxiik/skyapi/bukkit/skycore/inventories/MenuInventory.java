package fr.floxiik.skyapi.bukkit.skycore.inventories;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.FastInv;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.ItemBuilder;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.Lore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.UUID;

public class MenuInventory extends FastInv {

    private boolean preventClose = false;
    private Player player;
    private Account account;

    public MenuInventory(Player player) {
        super(54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.title.main"));
        this.player = player;
        this.account = SpigotCore.getApi().getAccount(this.player.getUniqueId());

        setItem(10, new ItemBuilder(Material.RED_BED).name("§7• §cVotre Faction §7•").addLore(Arrays.asList("", "§eLieu où se situe votre", "§efaction.", "", "§a§oClique pour être téléporté !")).build());
        setItem(19, new ItemBuilder(Material.IRON_PICKAXE).name("§7• §aMonde Minage §7•").addLore(Arrays.asList("", "§eVous avez besoin de", "§eressources ? C'est", "§ele lieu parfait !", "", "§a§oClique pour être téléporté !")).flags(ItemFlag.HIDE_ATTRIBUTES).build());
        setItem(28, new ItemBuilder(Material.DIAMOND_SWORD).name("§7• §bZone PVP §7•").addLore(Arrays.asList("", "§eVous voulez exploiter", "§evotre puissance ? C'est", "§ele lieu parfait !", "", "§a§oClique pour être téléporté !")).flags(ItemFlag.HIDE_ATTRIBUTES).build());
        setItem(37, new ItemBuilder(Material.COMPASS).name("§7• §6Changer de Spawn §7•").addLore(Arrays.asList("", "§a§oClique pour être téléporté !")).build());

        setItem(22, new ItemBuilder(Material.EMERALD).name("§7• §aMétiers §7•").addLore(Arrays.asList("", "§eVous pouvez suivre votre", "§eavancée dans la liste", "§ede vos métiers.", "", "§a§oClique pour ouvrir !")).build());
        setItem(23, new ItemBuilder(Material.END_PORTAL_FRAME).name("§7• §dWarps §7•").addLore(Arrays.asList("", "§eC'est ici que vous verrez", "§eles endroits spéciaux", "§eoù vous pouvez vous", "§etéléporter !", "", "§a§oClique pour ouvrir !")).build());
        setItem(24, new ItemBuilder(Material.ENDER_CHEST).name("§7• §5EnderChest §7•").addLore(Arrays.asList("", "§eAccédez à votre enderchest", "§ed'où vous voulez !", "", "§a§oClique pour ouvrir !")).build());
        setItem(31, new ItemBuilder(Material.CLOCK).name("§7• §6Calendrier des Events §7•").addLore(Arrays.asList("", "§eInformez vous sur", "§eles events à venir !", "", "§a§oClique pour ouvrir !")).build());
        setItem(32, new ItemBuilder(Material.EXPERIENCE_BOTTLE).name("§7• §3Quêtes §7•").addLore(Arrays.asList("", "§eEffectuez des quêtes pour", "§eobtenir des récompenses !", "", "§a§oClique pour ouvrir !")).build());
        setItem(33, new ItemBuilder(Material.BOOK).name("§7• §cInformations §7•").addLore(Arrays.asList("", "§eApprenez en plus sur", "§ele serveur et la façon", "§ed'y jouer !", "", "§a§oClique pour ouvrir !")).build());

        // Add some blocks to the borders
        int color = ColorEnum.getServerData();
        setItems(getBorders(), new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(11, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(12, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(13, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(14, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(20, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(29, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(38, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(41, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(42, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());
        setItem(43, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());

        ItemStack i = new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3);
        SkullMeta im = (SkullMeta) i.getItemMeta();
        im.setOwner(player.getName());
        im.setDisplayName("§7• §bProfil §7•");
        im.setLore(Lore.getProfileLore(this.account));
        i.setItemMeta(im);
        setItem(4, new ItemBuilder(i).build());

        // Prevent from closing when preventClose is to true
        setCloseFilter(p -> this.preventClose);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You opened the inventory");
    }

    @Override
    public void onClose(InventoryCloseEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You closed the inventory");
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        UUID playerUUID = player.getUniqueId();
        if(event.getView().getTitle().equals(LangAPI.get().parse(playerUUID, "skyapi.gui.title.main"))) {
            if(event.getSlot() == 10) {
                player.performCommand("f home");
                player.closeInventory();
            } else if(event.getSlot() == 19) {
                SkyCore.getTeleportManager().sendToServerRandom(player, "Minage");
            } else if(event.getSlot() == 28) {
                SkyCore.getTeleportManager().sendToServerRandom(player, "Pvp");
            } else if(event.getSlot() == 37) {
                player.kickPlayer(null);
            } else if(event.getSlot() == 22) {
                new JobsInventory(player).open(player);
            } else if(event.getSlot() == 23) {
                new WarpsInventory(player).open(player);
            } else if(event.getSlot() == 24) {
                player.performCommand("enderchest");
            } else if(event.getSlot() == 28) {

            } else if(event.getSlot() == 31) {
                new EventsInventory(player).open(player);
            } else if(event.getSlot() == 32) {
                new QuestsInventory(player).open(player);
            }
        }
    }
}