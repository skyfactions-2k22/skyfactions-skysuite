package fr.floxiik.skyapi.bukkit.skycore.utils;

import fr.floxiik.skyapi.data.Quest;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import java.util.*;

public class QuestGenerator {

    private static HashMap<String, Integer> crafts = new HashMap<>();
    private static HashMap<String, String> collects = new HashMap<>();
    private static HashMap<String, Integer> kills = new HashMap<>();
    //private static ArrayList<String> sells = new ArrayList<>();
    //private static ArrayList<String> buys = new ArrayList<>();
    private static HashMap<String, Integer> breaks = new HashMap<>();
    private static HashMap<String, Integer> places = new HashMap<>();

    public static Quest randomBasic() {
        Quest quest = new Quest();
        List<String> keysAsArray;
        Random r;
        String material;
        int difficulty;
        int choose = RandomGenerator.range(1,6);
        switch (choose) {
            case 1:
                keysAsArray = new ArrayList<>(crafts.keySet());
                r = new Random();
                material = keysAsArray.get(r.nextInt(keysAsArray.size()));
                difficulty = crafts.get(material);
                quest.setType("cr");
                quest.setTargetType(material);
                quest.setTargetTypeToShow(material);
                quest.setCurrent(0);
                quest.setTarget((int) (RandomGenerator.range(1, 64) / (difficulty > 2 ? Math.pow(difficulty*2, 2) : 1)));
                quest.setOther("null");
                quest.setRewardMoney(RandomGenerator.range(150, 400) * (difficulty > 2 ? difficulty/2 : 1) * (quest.getTarget()*2));
                quest.setRewardXp(RandomGenerator.range(1, 5) * (quest.getTarget()* 0.8f) * (difficulty/1.8f));
                return quest;
            case 2:
                keysAsArray = new ArrayList<>(collects.keySet());
                r = new Random();
                material = keysAsArray.get(r.nextInt(keysAsArray.size()));
                String materialToShow = collects.get(material);
                quest.setType("c");
                quest.setTargetType(material);
                quest.setTargetTypeToShow(materialToShow);
                quest.setCurrent(0);
                quest.setTarget(RandomGenerator.range(1, 100));
                quest.setOther("null");
                quest.setRewardMoney(RandomGenerator.range(50, 200) * (quest.getTarget()*2));
                quest.setRewardXp(RandomGenerator.range(2, 9) * (quest.getTarget()* 0.8f));
                return quest;
            case 3:
                keysAsArray = new ArrayList<>(kills.keySet());
                r = new Random();
                material = keysAsArray.get(r.nextInt(keysAsArray.size()));
                difficulty = kills.get(material);
                quest.setType("k");
                quest.setTargetType(material);
                quest.setTargetTypeToShow(Material.IRON_SWORD.name());
                quest.setCurrent(0);
                quest.setTarget(RandomGenerator.range(1, 55) / (difficulty > 2 ? difficulty*2 : 1));
                quest.setOther("null");
                quest.setRewardMoney(RandomGenerator.range(150, 400) * (difficulty > 2 ? difficulty/2 : 1) * (quest.getTarget()*2));
                quest.setRewardXp(RandomGenerator.range(1, 5) * (quest.getTarget()* 0.8f) * (difficulty/1.8f));
                return quest;
            case 4:
                keysAsArray = new ArrayList<>(breaks.keySet());
                r = new Random();
                material = keysAsArray.get(r.nextInt(keysAsArray.size()));
                difficulty = breaks.get(material);
                quest.setType("b");
                quest.setTargetType(material);
                quest.setTargetTypeToShow(material);
                quest.setCurrent(0);
                quest.setTarget(RandomGenerator.range(1, 150) / (difficulty > 2 ? difficulty*2 : 1));
                quest.setOther("null");
                quest.setRewardMoney(RandomGenerator.range(150, 400) * (difficulty > 2 ? difficulty/2 : 1) * (quest.getTarget()*2));
                quest.setRewardXp(RandomGenerator.range(1, 5) * (quest.getTarget()* 0.8f) * (difficulty/1.8f));
                return quest;
            case 5:
                keysAsArray = new ArrayList<>(places.keySet());
                r = new Random();
                material = keysAsArray.get(r.nextInt(keysAsArray.size()));
                difficulty = places.get(material);
                quest.setType("p");
                quest.setTargetType(material);
                quest.setTargetTypeToShow(material);
                quest.setCurrent(0);
                quest.setTarget(RandomGenerator.range(1, 150) / (difficulty > 2 ? difficulty*2 : 1));
                quest.setOther("null");
                quest.setRewardMoney(RandomGenerator.range(150, 400) * (difficulty > 2 ? difficulty/2 : 1) * (quest.getTarget()*2));
                quest.setRewardXp(RandomGenerator.range(1, 5) * (quest.getTarget()* 0.8f) * (difficulty/1.8f));
                return quest;
            default:
                return new Quest("b", "LOG", "LOG", 0, 10, "null", 1000, 20);
        }
    }

    public static void init() {
        crafts.put(Material.BOOKSHELF.name(), 2);
        crafts.put(Material.CHEST.name(), 1);
        crafts.put(Material.TRAPPED_CHEST.name(), 1);
        crafts.put(Material.FURNACE.name(), 1);
        crafts.put(Material.CRAFTING_TABLE.name(), 1);
        crafts.put(Material.LADDER.name(), 1);
        crafts.put(Material.ENCHANTING_TABLE.name(), 2);
        crafts.put(Material.ENDER_CHEST.name(), 2);
        crafts.put(Material.ANVIL.name(), 1);
        crafts.put(Material.PAINTING.name(), 1);
        crafts.put(Material.SLIME_BLOCK.name(), 2);
        crafts.put(Material.PISTON.name(), 1);
        crafts.put(Material.STICKY_PISTON.name(), 1);
        crafts.put(Material.REDSTONE_LAMP.name(), 1);
        crafts.put(Material.HOPPER.name(), 1);
        crafts.put(Material.TNT.name(), 1);
        crafts.put(Material.CARROT_ON_A_STICK.name(), 1);
        crafts.put(Material.BEACON.name(), 3);
        crafts.put(Material.BOW.name(), 1);
        crafts.put(Material.ARROW.name(), 1);
        crafts.put(Material.FLINT_AND_STEEL.name(), 1);
        crafts.put(Material.GOLDEN_APPLE.name(), 2);
        crafts.put(Material.GOLDEN_CARROT.name(), 2);
        crafts.put(Material.CAKE.name(), 3);
        crafts.put(Material.COOKIE.name(), 1);
        crafts.put(Material.BEETROOT_SOUP.name(), 1);
        crafts.put(Material.RABBIT_STEW.name(), 1);

        collects.put(Material.POTATO.name(), Material.POTATO.name());
        collects.put(Material.CARROT.name(), Material.CARROT.name());
        collects.put(Material.PUMPKIN.name(), Material.PUMPKIN.name());
        collects.put(Material.MELON.name(), Material.MELON.name());
        collects.put(Material.SUGAR_CANE.name(), Material.SUGAR_CANE.name());
        collects.put(Material.BROWN_MUSHROOM.name(), Material.BROWN_MUSHROOM.name());
        collects.put(Material.RED_MUSHROOM.name(), Material.RED_MUSHROOM.name());
        collects.put(Material.WHEAT.name(), Material.WHEAT.name());
        collects.put(Material.NETHER_WART.name(), Material.NETHER_WART_BLOCK.name());
        collects.put(Material.CACTUS.name(), Material.CACTUS.name());

        kills.put(EntityType.BLAZE.name(), 2);
        kills.put(EntityType.CREEPER.name(), 2);
        kills.put(EntityType.SKELETON.name(), 1);
        kills.put(EntityType.SPIDER.name(), 1);
        kills.put(EntityType.ZOMBIE.name(), 1);
        kills.put(EntityType.GHAST.name(), 2);
        kills.put(EntityType.ZOMBIFIED_PIGLIN.name(), 2);
        kills.put(EntityType.ENDERMAN.name(), 2);
        kills.put(EntityType.SILVERFISH.name(), 1);
        kills.put(EntityType.WITCH.name(), 2);
        kills.put(EntityType.PIG.name(), 1);
        kills.put(EntityType.SHEEP.name(), 1);
        kills.put(EntityType.COW.name(), 1);
        kills.put(EntityType.PLAYER.name(), 3);
        kills.put(EntityType.GIANT.name(), 3);

        breaks.put(Material.OAK_LOG.name(), 1);
        breaks.put(Material.STONE.name(), 1);
        breaks.put(Material.GRASS.name(), 1);
        breaks.put(Material.DIRT.name(), 1);
        breaks.put(Material.GRAVEL.name(), 1);
        breaks.put(Material.OBSIDIAN.name(), 2);
        breaks.put(Material.END_STONE.name(), 2);
        breaks.put(Material.DIAMOND_ORE.name(), 2);
        breaks.put(Material.SEA_LANTERN.name(), 1);
        breaks.put(Material.END_PORTAL_FRAME.name(), 3);
        breaks.put(Material.IRON_ORE.name(), 1);
        breaks.put(Material.COAL_ORE.name(), 1);
        breaks.put(Material.LAPIS_ORE.name(), 1);
        breaks.put(Material.REDSTONE_ORE.name(), 1);
        breaks.put(Material.GOLD_ORE.name(), 2);
        breaks.put(Material.EMERALD_ORE.name(), 3);
        breaks.put(Material.GLOWSTONE.name(), 2);
        breaks.put(Material.SAND.name(), 1);
        breaks.put(Material.SPONGE.name(), 3);

        places.put(Material.OAK_PLANKS.name(), 1);
        places.put(Material.GLASS.name(), 1);
        places.put(Material.BOOKSHELF.name(), 2);
        places.put(Material.DIAMOND_BLOCK.name(), 2);
        places.put(Material.EMERALD_BLOCK.name(), 3);
        places.put(Material.CHEST.name(), 1);
        places.put(Material.CRAFTING_TABLE.name(), 1);
        places.put(Material.FURNACE.name(), 1);
        places.put(Material.PISTON.name(), 1);
        places.put(Material.STICKY_PISTON.name(), 1);
        places.put(Material.HOPPER.name(), 1);
        places.put(Material.BEACON.name(), 3);
        places.put(Material.OBSIDIAN.name(), 1);
        places.put(Material.SAND.name(), 1);
        places.put(Material.OAK_FENCE.name(), 1);
        places.put(Material.JUKEBOX.name(), 2);
        places.put(Material.SOUL_SAND.name(), 1);
        places.put(Material.SPONGE.name(), 3);
    }
}