package fr.floxiik.skyapi.bukkit.skycore.vault;

import fr.floxiik.skyapi.api.SkygotApi;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import net.milkbowl.vault.economy.AbstractEconomy;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.ServicePriority;

import java.util.List;
import java.util.UUID;

public class EconBridge extends AbstractEconomy {
    private static final SkygotApi api = SpigotCore.getApi();
    private static Economy econ = new EconBridge();

    public static void register(SpigotCore main) {
        Bukkit.getServer().getServicesManager().register(Economy.class, econ, main, ServicePriority.Highest);
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return "SkyApi";
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public int fractionalDigits() {
        return -1;
    }

    @Override
    public String format(double v) {
        return v + "$";
    }

    @Override
    public String currencyNamePlural() {
        return "dollars";
    }

    @Override
    public String currencyNameSingular() {
        return "dollar";
    }

    @Override
    public boolean hasAccount(String s) {
        return true;
    }

    @Override
    public boolean hasAccount(String s, String s1) {
        return true;
    }

    @Override
    public double getBalance(String s) {
        return this.getBalance(Bukkit.getOfflinePlayer(s));
    }

    @Override
    public double getBalance(OfflinePlayer offlinePlayer) {
        UUID uuid = offlinePlayer.getUniqueId();
        Account account = api.getAccount(uuid);
        return account.getMoney();
    }

    @Override
    public double getBalance(String s, String s1) {
        return this.getBalance(s);
    }

    @Override
    public double getBalance(OfflinePlayer player, String world) {
        return this.getBalance(player);
    }

    @Override
    public boolean has(OfflinePlayer player, double amount) {
        return this.getBalance(player) >= amount;
    }

    @Override
    public boolean has(String s, double v) {
        return this.getBalance(s) >= v;
    }

    @Override
    public boolean has(OfflinePlayer player, String worldName, double amount) {
        return this.getBalance(player) >= amount;
    }

    @Override
    public boolean has(String s, String s1, double v) {
        return this.getBalance(s) >= v;
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, double amount) {
        int bal = 0;
        bal = (int) this.getBalance(offlinePlayer.getName());
        if (bal < amount)
            return new EconomyResponse(0.0D, bal, EconomyResponse.ResponseType.FAILURE, "The player as not enough money");
        bal -= amount;
        Account account = api.getAccount(offlinePlayer.getUniqueId());
        account.setMoney(bal);
        api.setAccount(account, api.isOnline(offlinePlayer.getUniqueId()), !api.isOnline(offlinePlayer.getUniqueId()));

        return new EconomyResponse(amount, bal, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, double v) {
        return this.withdrawPlayer(Bukkit.getOfflinePlayer(s), v);
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer player, String worldName, double amount) {
        return this.withdrawPlayer(player, amount);
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, String s1, double v) {
        return this.withdrawPlayer(s, v);
    }

    @Override
    public EconomyResponse depositPlayer(String s, double v) {
        return this.depositPlayer(Bukkit.getOfflinePlayer(s), v);
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, double amount) {
        Player player = offlinePlayer.getPlayer();
        int bal = 0;
        bal = (int) this.getBalance(offlinePlayer.getName());
        bal += amount;
        Account account = api.getAccount(offlinePlayer.getUniqueId());
        account.setMoney(bal);
        api.setAccount(account, api.isOnline(offlinePlayer.getUniqueId()), !api.isOnline(offlinePlayer.getUniqueId()));
        return new EconomyResponse(amount, bal, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse depositPlayer(String s, String s1, double v) {
        return this.depositPlayer(s, v);
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer player, String worldName, double amount) {
        return this.depositPlayer(player, amount);
    }

    @Override
    public EconomyResponse createBank(String s, String s1) {
        return null;
    }

    @Override
    public EconomyResponse deleteBank(String s) {
        return null;
    }

    @Override
    public EconomyResponse bankBalance(String s) {
        return null;
    }

    @Override
    public EconomyResponse bankHas(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse bankWithdraw(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse bankDeposit(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse isBankOwner(String s, String s1) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String s, String s1) {
        return null;
    }

    @Override
    public List<String> getBanks() {
        return null;
    }

    @Override
    public boolean createPlayerAccount(String s) {
        return true;
    }

    @Override
    public boolean createPlayerAccount(String s, String s1) {
        return true;
    }
}
