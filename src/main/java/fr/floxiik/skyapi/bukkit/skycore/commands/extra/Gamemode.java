package fr.floxiik.skyapi.bukkit.skycore.commands.extra;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.skycore.utils.StringUtils;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Gamemode implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player theSender = ((Player) sender).getPlayer();
            if (sender.hasPermission("skycore.gamemode")) {
                switch (label) {
                    case "gmc":
                        switchMode(theSender, GameMode.CREATIVE, args, true);
                        return true;
                    case "gms":
                        switchMode(theSender, GameMode.SURVIVAL, args, true);
                        return true;
                }
                if (args.length > 0) {
                    switch (args[0]) {
                        case "0":
                        case "s":
                            switchMode(theSender, GameMode.SURVIVAL, args, false);
                            break;
                        case "1":
                        case "c":
                            switchMode(theSender, GameMode.CREATIVE, args, false);
                            break;
                        case "2":
                        case "a":
                            switchMode(theSender, GameMode.ADVENTURE, args, false);
                            break;
                        case "spec":
                        case "spectator":
                        case "3":
                            switchMode(theSender, GameMode.SPECTATOR, args, false);
                            break;
                    }
                    return true;
                } else {
                    sender.sendMessage(LangAPI.get().parse(theSender.getUniqueId(), "skyapi.gm.usage"));
                    return false;
                }
            } else {
                sender.sendMessage(LangAPI.get().parse(theSender.getUniqueId(), "skyapi.noperm"));
            }
            return false;
        }
        return false;
    }

    private void switchMode(Player player, GameMode gameMode, String[] args, boolean cut) {
        if ((cut ? args.length > 0 : args.length > 1) && player.hasPermission("skycore.gamemode.other")) {
            Player target = Bukkit.getPlayer(args[cut ? 0 : 1]);
            if (target != null) {
                target.setGameMode(gameMode);
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.gm.other")
                        .replace("{player}", target.getName())
                        .replace("{mode}", StringUtils.toCamelCase(target.getGameMode().name())));
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
            }
        } else {
            player.setGameMode(gameMode);
            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.gm")
                    .replace("{mode}", StringUtils.toCamelCase(gameMode.name())));
        }
    }
}
