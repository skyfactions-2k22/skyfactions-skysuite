package fr.floxiik.skyapi.bukkit.skycore.commands.extra;

import fr.floxiik.skyapi.api.LangAPI;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Speed implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.speed")) {
                if (args.length == 2 && player.hasPermission("skycore.speed.other")) {
                    if (isInt(args[0])) {
                        Player target = Bukkit.getPlayer(args[1]);
                        int speed = Integer.parseInt(args[0]);
                        if(speed > 10) {
                            speed = 10;
                        } else if(speed < 0) {
                            speed = 0;
                        }
                        if (target != null) {
                            if (target.isFlying()) {
                                target.setFlySpeed(speed / 10f);
                                target.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.speed.fly.other")
                                    .replace("{player}", target.getName())
                                    .replace("{amount}", args[0]));
                            } else {
                                target.setWalkSpeed(speed / 10f);
                                target.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.speed.walk.other")
                                        .replace("{player}", target.getName())
                                        .replace("{amount}", args[0]));
                            }
                            return true;
                        } else {
                            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                        }
                    } else {
                        if (args[0].equalsIgnoreCase("reset")) {
                            Player target = Bukkit.getPlayer(args[1]);
                            if (target != null) {
                                target.setWalkSpeed(0.2f);
                                target.setFlySpeed(0.2f);
                                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.speed.reset.other")
                                    .replace("{player}", target.getName()));
                                return true;
                            } else {
                                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                            }
                        } else {
                            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.speed.usage"));
                        }
                    }
                } else if (args.length == 1) {
                    if (isInt(args[0])) {
                        int speed = Integer.parseInt(args[0]);
                        if(speed > 10) {
                            speed = 10;
                        } else if(speed < 0) {
                            speed = 0;
                        }
                        if (player.isFlying()) {
                            player.setFlySpeed(speed / 10f);
                            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.speed.fly")
                                    .replace("{amount}", args[0]));
                        } else {
                            player.setWalkSpeed(speed / 10f);
                            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.speed.walk")
                                    .replace("{amount}", args[0]));
                        }
                        return true;
                    } else {
                        if (args[0].equalsIgnoreCase("reset")) {
                            player.setWalkSpeed(0.2f);
                            player.setFlySpeed(0.2f);
                            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.speed.reset"));
                            return true;
                        } else {
                            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isInt(String string) {
        try {
            Integer.parseInt(string);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }
}
