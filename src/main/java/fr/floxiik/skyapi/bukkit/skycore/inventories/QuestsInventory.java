package fr.floxiik.skyapi.bukkit.skycore.inventories;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycore.jobs.JobsEnum;
import fr.floxiik.skyapi.bukkit.skycore.utils.ProgressBar;
import fr.floxiik.skyapi.bukkit.skycore.utils.QuestGenerator;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.FastInv;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.ItemBuilder;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.Lore;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Quest;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class QuestsInventory extends FastInv {

    private boolean preventClose = false;
    private Player player;
    private Account account;
    private HashMap<Integer, Quest> questsSlots = new HashMap<>();

    public QuestsInventory(Player player) {
        super(54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.title.main") + " §8» §9Quêtes");
        this.player = player;
        this.account = SpigotCore.getApi().getAccount(player.getUniqueId());
        int slot = 20;

        for(Quest quest : account.getQuests()) {
            Material material;
            try {
                material = Material.valueOf(quest.getTargetTypeToShow());
            } catch (Exception e) {
                material = Material.DIAMOND_SWORD;
                Bukkit.broadcastMessage(e.getMessage());
            }
            if(quest.isCompleted()) {
                setItem(slot, new ItemBuilder(material).name("§a" + quest).addLore(getQuestLore(quest)).enchant(Enchantment.DIG_SPEED).flags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES).build());
            } else {
                setItem(slot, new ItemBuilder(material).name("§a" + quest).addLore(getQuestLore(quest)).flags(ItemFlag.HIDE_ATTRIBUTES).build());
            }
            questsSlots.put(slot, quest);
            if(slot == 24) {
                slot = 29;
            } else {
                slot++;
            }
        }

        // Add some blocks to the borders
        int color = ColorEnum.getServerData();
        setItems(getBorders(), new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());

        setItem(49, new ItemBuilder(Material.BARRIER).name("§7• §cRetour §7•").build());

        ItemStack i = new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3);
        SkullMeta im = (SkullMeta) i.getItemMeta();
        im.setOwner(player.getName());
        im.setLore(Lore.getProfileLore(this.account));
        im.setDisplayName("§7• §bProfil §7•");
        i.setItemMeta(im);
        setItem(4, new ItemBuilder(i).build());

        // Prevent from closing when preventClose is to true
        setCloseFilter(p -> this.preventClose);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You opened the inventory");
    }

    @Override
    public void onClose(InventoryCloseEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You closed the inventory");
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        UUID playerUUID = player.getUniqueId();
        ArrayList<Quest> quests = new ArrayList<>(account.getQuests());
        if (questsSlots.containsKey(event.getSlot())) {
            int index = 0;
            for(Quest quest : quests) {
                if(quest.toString().equals(questsSlots.get(event.getSlot()).toString())) {
                    index = quests.indexOf(quest);
                }
            }
            if(event.isRightClick()) {
                if(quests.get(index).isCompleted()) {
                    return;
                }
                if(SkyCore.getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(playerUUID), 1000D).transactionSuccess()) {
                    quests.set(index, QuestGenerator.randomBasic());
                    account.setQuests(quests);
                    SpigotCore.getApi().setAccount(account, SpigotCore.getApi().isOnline(playerUUID), false);
                    new QuestsInventory(player).open(player);
                } else {
                    player.closeInventory();
                    player.sendMessage("§cFonds insuffisants !");
                }
            } else if(event.isLeftClick()) {
                if(quests.get(index).isCompleted()) {
                    account.addLevel(JobsEnum.FARMER.getLibelle(), quests.get(index).getRewardXp());
                    SkyCore.getEconomy().depositPlayer(Bukkit.getOfflinePlayer(playerUUID), quests.get(index).getRewardMoney());
                    player.sendMessage("§eVous avez §6récupérer §evotre §brécompense§e ! ");
                    quests.set(index, QuestGenerator.randomBasic());
                    account.setQuests(quests);
                    SpigotCore.getApi().setAccount(account, SpigotCore.getApi().isOnline(playerUUID), false);
                    player.closeInventory();
                } else {
                    player.sendMessage("§cQuête pas encore terminée !");
                }
            }
        } else if (event.getSlot() == 49) {
            new MenuInventory(player).open(player);
        }
    }

    private List<String> getQuestLore(Quest quest) {
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add("         §e" + quest.getCurrent() + " §7/ §6" + quest.getTarget());
        lore.add("  §7[" + ProgressBar.getProgressBar(quest.getCurrent(), quest.getTarget(), 40, '|', "§a", "§8") + "§7]");
        lore.add("");
        lore.add("§6Récompenses:");
        lore.add(" §7• §a" + quest.getRewardMoney() + "§2$");
        lore.add(" §7• §e" + round(quest.getRewardXp()) + "§6xp");
        lore.add("");
        lore.add("§eVous devez terminer cette");
        lore.add("§equête pour en avoir une");
        lore.add("§enouvelle.");
        lore.add("");
        if(quest.isCompleted()) {
            lore.add("§7§lClic-Gauche §7pour la §bRécompense");
        } else {
            lore.add("§7§lClic-Droit §7pour §cPasser §8(§a1000§2$§8)");
        }
        return lore;
    }

    private float round(float f) {
        BigDecimal bigDecimal = new BigDecimal(Float.toString(f));
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        return bigDecimal.floatValue();
    }
}