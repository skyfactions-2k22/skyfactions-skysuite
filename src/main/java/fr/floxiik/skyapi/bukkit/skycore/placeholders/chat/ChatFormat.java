package fr.floxiik.skyapi.bukkit.skycore.placeholders.chat;

public enum ChatFormat {

    OWNER("owner", ""),
    ADMIN("admin", ""),
    MANAGER("manager", ""),
    MOD("mod", ""),
    HELPER("helper", ""),
    STAFF("staff", ""),
    MVPPP("mvppp", ""),
    MVPP("mvpp", ""),
    MVP("mvp", ""),
    VIPP("vipp", ""),
    VIP("vip", ""),
    MEMBER("member", "%skycrew_crew_rank%%skycrew_crew_name% %img_member% %player_displayname% &8»");


    private String rank;
    private String format;

    ChatFormat(String rank, String format) {
        this.rank = rank;
        this.format = format;
    }

    public String getRank() {
        return rank;
    }

    public String getFormat() {
        return format;
    }
}
