package fr.floxiik.skyapi.bukkit.skycore.inventories;

import fr.floxiik.skyapi.bukkit.skycore.SkyCore;

public enum ColorEnum {

    LOBBY(3),
    PVP(1),
    MINAGE(5),
    CREW(14);

    private int data;

    ColorEnum(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public static int getServerData() {
        String server = SkyCore.getInstance().getLocalization();
        switch (server) {
            case "Pvp Zone":
                return PVP.getData();
            case "Minage":
                return MINAGE.getData();
            case "Your Base":
                return CREW.getData();
            default:
                return LOBBY.getData();
        }
    }
}
