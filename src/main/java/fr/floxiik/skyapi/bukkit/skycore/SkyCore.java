package fr.floxiik.skyapi.bukkit.skycore;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.commands.eco.Eco;
import fr.floxiik.skyapi.bukkit.skycore.commands.eco.Money;
import fr.floxiik.skyapi.bukkit.skycore.commands.eco.Pay;
import fr.floxiik.skyapi.bukkit.skycore.commands.extra.*;
import fr.floxiik.skyapi.bukkit.skycore.commands.gui.*;
import fr.floxiik.skyapi.bukkit.skycore.commands.teleportation.*;
import fr.floxiik.skyapi.bukkit.skycore.listeners.*;
import fr.floxiik.skyapi.bukkit.skycore.managers.PlayerManager;
import fr.floxiik.skyapi.bukkit.skycore.managers.TeleportManager;
import fr.floxiik.skyapi.bukkit.skycore.placeholders.CorePlaceholders;
import fr.floxiik.skyapi.bukkit.skycore.placeholders.chat.ChatPlaceholders;
import fr.floxiik.skyapi.bukkit.skycore.utils.QuestGenerator;
import fr.floxiik.skyapi.bukkit.skycore.vault.EconBridge;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;

public class SkyCore {

    public static SkyCore instance;
    private static TeleportManager teleportManager;
    private static PlayerManager playerManager;
    private static Economy economy;
    private String version = "1.0.0";

    public void init(SpigotCore spigotCore) {
        instance = this;

        teleportManager = new TeleportManager();
        playerManager = new PlayerManager();

        setupEconomy();

        if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new CorePlaceholders().register();
            new ChatPlaceholders().register();
        }

        QuestGenerator.init();

        registerEvents(spigotCore);
        registerCommands(spigotCore);

        spigotCore.getServer().getMessenger().registerOutgoingPluginChannel(spigotCore, "skycore:tp");
        spigotCore.getServer().getMessenger().registerIncomingPluginChannel(spigotCore, "skycore:tp", new PluginMessageListener());

        sendLog(true);
    }

    public void disable(SpigotCore spigotCore) {
        sendLog(false);
    }


    private void registerEvents(SpigotCore spigotCore) {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new RainListener(), spigotCore);
        pm.registerEvents(new AntiSpam(), spigotCore);
        pm.registerEvents(new PlayerJoinListener(), spigotCore);
        pm.registerEvents(new PlayerQuitListener(), spigotCore);
        pm.registerEvents(new QuestListener(), spigotCore);
        pm.registerEvents(new PlayerDeathListener(), spigotCore);
        pm.registerEvents(new PlayerLanguageChangeListener(), spigotCore);
        pm.registerEvents(new PlayerInteractListener(), spigotCore);
        pm.registerEvents(new PlayerRespawnListener(), spigotCore);
        pm.registerEvents(new EntityDamageByEntityListener(), spigotCore);
        pm.registerEvents(new TeleportListener(), spigotCore);
    }

    private void registerCommands(SpigotCore spigotCore) {
        spigotCore.getCommand("gm").setExecutor(new Gamemode());
        spigotCore.getCommand("gmc").setExecutor(new Gamemode());
        spigotCore.getCommand("gms").setExecutor(new Gamemode());
        spigotCore.getCommand("money").setExecutor(new Money());
        spigotCore.getCommand("fly").setExecutor(new Fly());
        spigotCore.getCommand("speed").setExecutor(new Speed());
        spigotCore.getCommand("craft").setExecutor(new Craft());
        spigotCore.getCommand("furnace").setExecutor(new Furnace());
        spigotCore.getCommand("anvil").setExecutor(new Anvil());
        spigotCore.getCommand("brew").setExecutor(new Brew());
        spigotCore.getCommand("heal").setExecutor(new Heal());
        spigotCore.getCommand("feed").setExecutor(new Feed());
        spigotCore.getCommand("milk").setExecutor(new Milk());
        spigotCore.getCommand("pay").setExecutor(new Pay());
        spigotCore.getCommand("tp").setExecutor(new Tp());
        spigotCore.getCommand("tpa").setExecutor(new Tpa());
        spigotCore.getCommand("tphere").setExecutor(new TpHere());
        spigotCore.getCommand("tpahere").setExecutor(new TpaHere());
        spigotCore.getCommand("hub").setExecutor(new Hub());
        spigotCore.getCommand("eco").setExecutor(new Eco());
        spigotCore.getCommand("trash").setExecutor(new Trash());
        spigotCore.getCommand("clear").setExecutor(new Clear());
    }

    private void setupEconomy() {
        if (Bukkit.getPluginManager().isPluginEnabled("Vault")) EconBridge.register(SpigotCore.getInstance());
        RegisteredServiceProvider<Economy> rsp = SpigotCore.getInstance().getServer().getServicesManager().getRegistration(Economy.class);
        economy = rsp.getProvider();
    }

    public String getLocalization() {
        String server = SpigotCore.getServerName();
        if(server.startsWith("Lobby")) {
            return "Spawn";
        } else if(server.startsWith("Minage")) {
            return "Minage";
        } else if(server.startsWith("Pvp")) {
            return "Pvp Zone";
        } else if(server.startsWith("Crew")) {
            return "Your Base";
        } else {
            return "Your Base";
        }
    }

    public static SkyCore getInstance() {
        return instance;
    }

    public static TeleportManager getTeleportManager() {
        return teleportManager;
    }

    public static PlayerManager getPlayerManager() {
        return playerManager;
    }

    public static Economy getEconomy() {
        return economy;
    }

    public String getVersion() {
        return version;
    }

    public void sendLog(boolean start) {
        SpigotCore.getInstance().getLogger().info("<==================================>");
        SpigotCore.getInstance().getLogger().info("");
        if(start) SpigotCore.getInstance().getLogger().info("  Module (SkyCore) Chargé avec succès !");
        if(!start) SpigotCore.getInstance().getLogger().info("  Module (SkyCore) Désactivé avec succès !");
        SpigotCore.getInstance().getLogger().info("  Version: " + getVersion());
        SpigotCore.getInstance().getLogger().info("  Autheur: Achereth");
        SpigotCore.getInstance().getLogger().info("");
        SpigotCore.getInstance().getLogger().info("<==================================>");
    }
}
