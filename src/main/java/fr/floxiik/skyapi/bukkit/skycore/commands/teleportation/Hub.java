package fr.floxiik.skyapi.bukkit.skycore.commands.teleportation;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Hub implements CommandExecutor {

    Location location = new Location(Bukkit.getWorlds().get(0), 0.5d, 4d, 0.5d, 90f, -0f);

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.hub")) {
                if (args.length > 0 && player.hasPermission("skycore.hub.other")) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        SpigotCore.getApi().sendPlayerMessage(target.getUniqueId(), LangAPI.get().parse(target.getUniqueId(), "skyapi.spawn"));
                        if(SpigotCore.getServerName().startsWith("Lobby-")) {
                            target.teleport(location);
                        } else {
                            target.kickPlayer(null);
                        }
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    SpigotCore.getApi().sendPlayerMessage(player.getUniqueId(), LangAPI.get().parse(player.getUniqueId(), "skyapi.spawn"));
                    if(SpigotCore.getServerName().startsWith("Lobby-")) {
                        player.teleport(location);
                    } else {
                        player.kickPlayer(null);
                    }
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
