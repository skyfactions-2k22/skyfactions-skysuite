package fr.floxiik.skyapi.bukkit.skycore.managers;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class CombatManager {

    private HashMap<UUID, Long> players = new HashMap<>();
    private final int cooldown = 15000;

    public void setInCombat(Player player) {
        UUID uuid = player.getUniqueId();
        long time = System.currentTimeMillis();
        if(players.containsKey(uuid)) {
            players.replace(uuid, time);
        } else {
            players.put(uuid, time);
        }
    }

    public void removeCombat(Player player) {
        players.remove(player.getUniqueId());
    }

    public boolean isInCombat(Player player) {
        return players.containsKey(player.getUniqueId());
    }

    public int getCooldown(Player player) {
        long time = players.get(player.getUniqueId());
        long current = System.currentTimeMillis();
        return (int) ((time+this.cooldown-current)/1000);
    }

    public String combatStatus(Player player) {
        if(isInCombat(player)) {
            return "§a✔";
        } else {
            return "§c✘";
        }
    }

    public String getActionBar(Player player) {
        return "§eEn Combat: " + this.combatStatus(player) + (this.isInCombat(player) ? " §7- §b" + this.getCooldown(player) + "s" : "");
    }
}
