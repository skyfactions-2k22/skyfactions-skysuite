package fr.floxiik.skyapi.bukkit.skycore.commands.extra;

import fr.floxiik.skyapi.api.LangAPI;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;

public class Heal implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.heal")) {
                if (args.length > 0 && player.hasPermission("skycore.heal.other")) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        target.setHealth(20D);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.heal.other")
                            .replace("{player}", target.getName()));
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    player.setHealth(20D);
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.heal"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
