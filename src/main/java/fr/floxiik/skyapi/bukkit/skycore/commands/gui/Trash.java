package fr.floxiik.skyapi.bukkit.skycore.commands.gui;

import fr.floxiik.skyapi.api.LangAPI;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class Trash implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.trash")) {
                if (args.length > 0 && player.hasPermission("skycore.trash.other")) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        Inventory trash = Bukkit.getServer().createInventory(target, 54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.trash"));
                        target.openInventory(trash);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.trash.other")
                            .replace("{player}", target.getName()));
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    Inventory trash = Bukkit.getServer().createInventory(player, 54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.trash"));
                    player.openInventory(trash);
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
