package fr.floxiik.skyapi.bukkit.skycore.placeholders;

import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;

public class CorePlaceholders extends PlaceholderExpansion {

    @Override
    public String getAuthor() {
        return "Floxiik";
    }

    @Override
    public String getIdentifier() {
        return "skycore";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier) {
        /*UUID playerUUID = player.getUniqueId();
        Account account = SpigotCore.getApi().getAccount(playerUUID);*/

        if (identifier.equals("timeplayed")) {
            //Date time = SpigotCore.getApi().getTimePlaying(account);
            //return "§a" + time.getHours() + "h, " + time.getMinutes() + "min, " + time.getSeconds() + "s";
        }
        if (identifier.equals("prefix")) {
            return PlaceholderAPI.setPlaceholders(player, "%img_" + PlaceholderAPI.setPlaceholders(player, "%vault_prefix%") + "%");
        }
        return null;
    }
}