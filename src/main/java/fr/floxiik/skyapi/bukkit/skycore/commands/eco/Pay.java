package fr.floxiik.skyapi.bukkit.skycore.commands.eco;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Pay implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.money.pay")) {
                if (args.length > 0) {
                    Account target = SpigotCore.getApi().getAccount(args[0]);
                    if (target != null) {
                        if(!target.getUuid().toString().equalsIgnoreCase(player.getUniqueId().toString())) {
                            if (args.length > 1) {
                                if (isInt(args[1])) {
                                    Account account = SpigotCore.getApi().getAccount(player.getUniqueId());
                                    int amount = Integer.parseInt(args[1]);
                                    if (SkyCore.getEconomy().has(Bukkit.getOfflinePlayer(account.getUuid()), amount)) {
                                        target.setMoney(target.getMoney() + amount);
                                        //SpigotCore.getEconomy().depositPlayer(Bukkit.getOfflinePlayer(target.getUuid()), amount);
                                        SpigotCore.getApi().setAccount(target, SpigotCore.getApi().isOnline(target.getUuid()), !SpigotCore.getApi().isOnline(target.getUuid()));
                                        //account.setMoney(account.getMoney() - amount);
                                        SkyCore.getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(account.getUuid()), amount);
                                        //SpigotCore.getApi().updateAccount(account, true);
                                        SpigotCore.getApi().sendPlayerMessage(target.getUuid(), LangAPI.get().parse(target.getUuid(), "skyapi.pay.receive")
                                                .replace("{amount}", String.valueOf(amount))
                                                .replace("{player}", player.getName()));
                                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.pay.send")
                                            .replace("{amount}", String.valueOf(amount))
                                            .replace("{player}", target.getUsername()));
                                    } else {
                                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.pay.noenought"));
                                    }
                                } else {
                                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.chosevalidnumber"));
                                }
                            } else {
                                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.pay.choseamount"));
                            }
                        } else {
                            player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.notyourself"));
                        }
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.choseplayer"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }

    public boolean isInt(String string) {
        int amount = 0;
        try {
            amount = Integer.parseInt(string);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return amount != 0;
    }
}
