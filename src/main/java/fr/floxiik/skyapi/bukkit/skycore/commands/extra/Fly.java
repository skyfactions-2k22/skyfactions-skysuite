package fr.floxiik.skyapi.bukkit.skycore.commands.extra;

import fr.floxiik.skyapi.api.LangAPI;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Fly implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.fly")) {
                if (args.length > 0 && player.hasPermission("skycore.fly.other")) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        if (target.getAllowFlight() || target.isFlying()) {
                            target.setAllowFlight(false);
                            target.setFlying(false);
                            target.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.flydisabled.other").replace("{player}", target.getName()));
                        } else {
                            target.setAllowFlight(true);
                            target.setFlying(true);
                            target.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.flyenabled.other").replace("{player}", target.getName()));
                        }
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    if (player.getAllowFlight() || player.isFlying()) {
                        player.setAllowFlight(false);
                        player.setFlying(false);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.flydisabled"));
                    } else {
                        player.setAllowFlight(true);
                        player.setFlying(true);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.flyenabled"));
                    }
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
