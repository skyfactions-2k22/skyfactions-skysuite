package fr.floxiik.skyapi.bukkit.skycore.placeholders.chat;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

public class ChatPlaceholders extends PlaceholderExpansion {

    @Override
    public String getAuthor() {
        return "Floxiik";
    }

    @Override
    public String getIdentifier() {
        return "skychat";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier) {
        /*UUID playerUUID = player.getUniqueId();
        Account account = SpigotCore.getApi().getAccount(playerUUID);*/

        if (identifier.equals("formated")) {

            Component msg = MiniMessage.miniMessage().deserialize("<rainbow>salut c'est moi</rainbow> :admin:");
            String msg2 = GsonComponentSerializer.gson().serialize(msg.asComponent());

            Bukkit.getServer().spigot().broadcast((BaseComponent) msg.asComponent());

            return msg2;
            //Date time = SpigotCore.getApi().getTimePlaying(account);
            //return "§a" + time.getHours() + "h, " + time.getMinutes() + "min, " + time.getSeconds() + "s";
        }
        return null;
    }
}