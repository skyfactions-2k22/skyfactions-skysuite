package fr.floxiik.skyapi.bukkit.skycore.listeners;

import com.rexcantor64.triton.api.events.PlayerChangeLanguageSpigotEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerLanguageChangeListener implements Listener {

    @EventHandler
    public void onLangChange(PlayerChangeLanguageSpigotEvent event) {
        event.getLanguagePlayer().refreshAll();
    }
}
