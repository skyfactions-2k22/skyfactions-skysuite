package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.*;

public class TeleportListener implements Listener {

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        if(SkyCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent event) {
        if(SkyCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if(SkyCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if(SkyCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onConsume(PlayerItemConsumeEvent event) {
        if(SkyCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(SkyCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if(SkyCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    /*@EventHandler
    public void onBucket(PlayerBucketEvent event) {
        if(SpigotCore.getTeleportManager().isTeleporting(event.getPlayer())) {
            event.setCancelled(true);
        }
    }*/
}
