package fr.floxiik.skyapi.bukkit.skycore.managers;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.teleportations.TeleportType;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;
import fr.floxiik.skyapi.data.Home;
import fr.floxiik.skyapi.bukkit.skycore.teleportations.TeleportRequest;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class TeleportManager {

    private HashMap<UUID, TeleportRequest> teleportRequests = new HashMap<>();
    private ArrayList<UUID> pendingTeleport = new ArrayList<>();

    public void addInCooldown(UUID uuid, int cooldown) {
        if(pendingTeleport.contains(uuid)) {
            SpigotCore.getApi().sendPlayerMessage(uuid, "§cVous êtes déjà en téléportation !");
        } else {
            pendingTeleport.add(uuid);
        }

        SpigotCore.getApi().saveAccount(uuid, true, false);

        Player player = Bukkit.getPlayer(uuid);
        if(player != null) {
            Location loc1 = player.getLocation();
            AtomicInteger timer = new AtomicInteger();
            new BukkitRunnable() {
                public void run() {
                    Location loc2 = player.getLocation();
                    if(loc1.getX() != loc2.getX() || loc1.getY() != loc2.getY() || loc1.getZ() != loc2.getZ()) {
                        cancelCooldown(uuid);
                        cancel();
                    } else if(timer.get() >= cooldown) {
                        finishCooldown(uuid);
                        cancel();
                    } else {
                        timer.getAndIncrement();
                    }
                }
            }.runTaskTimer(SpigotCore.getInstance(), 0, 20);
        }
    }

    public void cancelCooldown(UUID uuid) {
        SpigotCore.getApi().sendPlayerMessage(uuid, "§ctéléportation annulée, vous avez bougés");
        pendingTeleport.remove(uuid);
    }

    public void finishCooldown(UUID uuid) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("CooldownFinish");
        out.writeUTF(uuid.toString());
        Bukkit.getOnlinePlayers().stream().findFirst().get().sendPluginMessage(SpigotCore.getInstance(), "skycore:tp", out.toByteArray());

        pendingTeleport.remove(uuid);
    }

    public void addTeleportRequest(UUID uuid, TeleportRequest teleportRequest) {
        if(teleportRequests.containsKey(uuid)) {
            teleportRequests.replace(uuid, teleportRequest);
        } else {
            teleportRequests.put(uuid, teleportRequest);
        }
    }

    public void closeTeleportRequest(UUID uuid) {
        teleportRequests.remove(uuid);
    }

    public boolean hasRequest(UUID uuid) {
        return teleportRequests.containsKey(uuid);
    }

    public Location getTeleportLocation(UUID uuid) {
        if(hasRequest(uuid)) {
            return teleportRequests.get(uuid).getLocation();
        } else {
            return null;
        }
    }

    public HashMap<UUID, TeleportRequest> getTeleportRequests() {
        return teleportRequests;
    }

    public void sendToServerRandom(Player player, String serverGroup) {
        UUID playerUUID = player.getUniqueId();
        if(pendingTeleport.contains(playerUUID)) {
            player.sendMessage("§cVous avez déjà une téléportation en attente !");
            return;
        }
        pendingTeleport.add(playerUUID);
        SpigotCore.getApi().saveAccount(playerUUID, true, false);
        player.sendMessage(LangAPI.get().parse(playerUUID, "skyapi.teleport.server")
            .replace("{group}", serverGroup));
        Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () ->  {
            SpigotCore.getApi().sendToServerRandom(playerUUID, serverGroup);
            pendingTeleport.remove(playerUUID);
        }, 3 * 20);
    }

    public void teleportToLocation(Account account, Location location) {
        Player player = Bukkit.getPlayer(account.getUuid());
        teleportToLocation(player, location);
    }

    public void teleportToLocation(Player player, Location location) {
        if(player != null) {
            player.teleport(location);
        }
    }

    public void teleportToServer(Player player, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("TeleportRequest");
        out.writeUTF(player.getUniqueId().toString());
        out.writeUTF("");
        out.writeUTF(server);
        out.writeUTF(TeleportType.HOME.getType());
        out.writeInt(5);

        player.sendPluginMessage(SpigotCore.getInstance(), "skycore:tp", out.toByteArray());
    }

    public void teleportToLocation(Account account, Home home) {
        Player player = Bukkit.getPlayer(account.getUuid());
        if(player != null) {
            Crew crew = SpigotCore.getApi().getCrew(account);
            if (SpigotCore.getServerName().equals(crew.getServerId())) {
                teleportToLocation(account, new Location(Bukkit.getWorlds().get(0), home.getX(), home.getY(), home.getZ(), home.getYaw(), home.getPitch()));
            } else {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("TeleportRequest");
                out.writeUTF(account.getUuid().toString());
                out.writeUTF(home.getX() + ";" + home.getY() + ";" + home.getZ() + ";" + home.getYaw() + ";" + home.getPitch());
                out.writeUTF(crew.getServerId());
                out.writeUTF(TeleportType.HOME_WARP.getType());
                out.writeInt(5);

                player.sendPluginMessage(SpigotCore.getInstance(), "skycore:tp", out.toByteArray());
            }
        }
    }

    public boolean isTeleporting(Player player) {
        return pendingTeleport.contains(player);
    }
}
