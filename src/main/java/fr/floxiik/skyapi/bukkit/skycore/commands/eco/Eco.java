package fr.floxiik.skyapi.bukkit.skycore.commands.eco;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Eco implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.eco")) {
                if(args.length >= 3) {
                    Account account = SpigotCore.getApi().getAccount(args[1]);
                    if(account == null) {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                        return false;
                    }
                    int amount = 0;
                    if(!isInt(args[2])) {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.chosevalidnumber"));
                        return false;
                    }
                    amount = Integer.parseInt(args[2]);
                    if(args[0].equalsIgnoreCase("give")) {
                        account.setMoney(account.getMoney() + amount);
                        SpigotCore.getApi().setAccount(account, SpigotCore.getApi().isOnline(account.getUuid()), !SpigotCore.getApi().isOnline(account.getUuid()));
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.ecogive")
                                .replace("{amount}", String.valueOf(amount))
                                .replace("{player}", args[1]));
                    } else if(args[0].equalsIgnoreCase("take")) {
                        account.setMoney(account.getMoney() - amount);
                        SpigotCore.getApi().setAccount(account, SpigotCore.getApi().isOnline(account.getUuid()), !SpigotCore.getApi().isOnline(account.getUuid()));
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.ecotake")
                                .replace("{amount}", String.valueOf(amount))
                                .replace("{player}", args[1]));
                    } else if(args[0].equalsIgnoreCase("set")) {
                        account.setMoney(amount);
                        SpigotCore.getApi().setAccount(account, SpigotCore.getApi().isOnline(account.getUuid()), !SpigotCore.getApi().isOnline(account.getUuid()));
                        //SpigotCore.getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(account.getUuid()), account.getMoney());
                        //SpigotCore.getEconomy().depositPlayer(Bukkit.getOfflinePlayer(account.getUuid()), amount);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.ecoset")
                            .replace("{amount}", String.valueOf(amount))
                            .replace("{player}", args[1]));
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.eco.usage"));
                    }
                } else {
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.eco.usage"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }

    public boolean isInt(String string) {
        int amount = 0;
        try {
            amount = Integer.parseInt(string);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return amount != 0;
    }
}
