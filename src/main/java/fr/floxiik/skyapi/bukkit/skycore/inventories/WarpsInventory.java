package fr.floxiik.skyapi.bukkit.skycore.inventories;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.FastInv;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.ItemBuilder;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.Lore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

public class WarpsInventory extends FastInv {

    private boolean preventClose = false;
    private Player player;
    private Account account;

    public WarpsInventory(Player player) {
        super(54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.title.main") + " §8» §9Warps");
        this.player = player;
        this.account = SpigotCore.getApi().getAccount(player.getUniqueId());

        setItem(20, new ItemBuilder(Material.ENCHANTING_TABLE).name("§7• §dEnchantements §7•").addLore(Arrays.asList("", "§eEnchantez simplement vos", "§eitems en venant ici !", "", "§a§oClique pour être téléporté !")).build());
        setItem(22, new ItemBuilder(Material.MINECART).name("§7• §c§lHangar §7•").addLore(Arrays.asList("", "§eMarchez jusqu'à votre", "§edestination !", "", "§a§oClique pour être téléporté !")).build());
        setItem(24, new ItemBuilder(Material.CHEST).name("§7• §6Crates §7•").addLore(Arrays.asList("", "§eTentez de gagner de", "§enombreux lots !", "", "§a§oClique pour être téléporté !")).build());
        setItem(29, new ItemBuilder(Material.NETHER_WART).name("§7• §4Monde du Nether §7•").addLore(Arrays.asList("", "§eRejoignez le monde le", "§eplus sombre et dangereux !", "", "§a§oClique pour être téléporté !")).build());
        setItem(31, new ItemBuilder(Material.EXPERIENCE_BOTTLE).name("§7• §eMarchand §7•").addLore(Arrays.asList("", "§eAchetez et vendez vos items", "§eau prix Marchand !", "", "§a§oClique pour être téléporté !")).build());
        setItem(33, new ItemBuilder(Material.ENDER_EYE).name("§7• §5Monde de l'End §7•").addLore(Arrays.asList("", "§eRejoignez l'End et farmez", "§evos items favoris !", "", "§a§oClique pour être téléporté !")).build());

        // Add some blocks to the borders
        int color = ColorEnum.getServerData();
        setItems(getBorders(), new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").data(color).build());

        setItem(49, new ItemBuilder(Material.BARRIER).name("§7• §cRetour §7•").build());

        ItemStack i = new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3);
        SkullMeta im = (SkullMeta) i.getItemMeta();
        im.setOwner(player.getName());
        im.setLore(Lore.getProfileLore(this.account));
        im.setDisplayName("§7• §bProfil §7•");
        i.setItemMeta(im);
        setItem(4, new ItemBuilder(i).build());

        // Prevent from closing when preventClose is to true
        setCloseFilter(p -> this.preventClose);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You opened the inventory");
    }

    @Override
    public void onClose(InventoryCloseEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You closed the inventory");
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        if (event.getSlot() == 10) {

        } else if (event.getSlot() == 49) {
            new MenuInventory(player).open(player);
        }
    }
}