package fr.floxiik.skyapi.bukkit.skycore.commands.gui;

import fr.floxiik.skyapi.api.LangAPI;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

public class Brew implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.brew")) {
                if (args.length > 0 && player.hasPermission("skycore.brew.other")) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        Inventory i = Bukkit.getServer().createInventory(target, InventoryType.BREWING);
                        target.openInventory(i);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.brew.other")
                            .replace("{player}", target.getName()));
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    Inventory i = Bukkit.getServer().createInventory(player, InventoryType.BREWING);
                    player.openInventory(i);
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.brew"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
