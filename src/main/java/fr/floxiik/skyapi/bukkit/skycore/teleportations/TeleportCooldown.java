package fr.floxiik.skyapi.bukkit.skycore.teleportations;

import java.util.UUID;

public class TeleportCooldown {

    private UUID player;
    private long start;

    public TeleportCooldown(UUID player, long start) {
        this.player = player;
        this.start = start;
    }

    public UUID getPlayer() {
        return player;
    }

    public void setPlayer(UUID player) {
        this.player = player;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }
}
