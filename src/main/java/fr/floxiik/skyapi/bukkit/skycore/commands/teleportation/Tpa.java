package fr.floxiik.skyapi.bukkit.skycore.commands.teleportation;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Tpa implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if(args.length > 0) {
                Account target = SpigotCore.getApi().getAccount(args[0]);
                if(target != null && SpigotCore.getApi().isOnline(target.getUuid())) {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("TeleportToPlayerRequest");
                    out.writeUTF(player.getUniqueId().toString());
                    out.writeUTF(target.getUuid().toString());
                    out.writeUTF("tpa");
                    player.sendPluginMessage(SpigotCore.getInstance(), "skycore:tp", out.toByteArray());
                }
            }
        }
        return false;
    }
}