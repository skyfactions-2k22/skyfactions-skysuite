package fr.floxiik.skyapi.bukkit.skycore.commands.eco;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Money implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.money")) {
                if (args.length > 0 && player.hasPermission("skycore.money.other")) {
                    Account target = SpigotCore.getApi().getAccount(args[0]);
                    if (target != null) {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.money.other")
                            .replace("{player}", target.getUsername())
                            .replace("{amount}", String.valueOf(target.getMoney())));
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.money")
                            .replace("{amount}", String.valueOf(SpigotCore.getApi().getAccount(player.getUniqueId()).getMoney())));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
