package fr.floxiik.skyapi.bukkit.skycore.managers;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.jobs.JobsEnum;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.providers.spigot.SpigotAccountProvider;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class PlayerManager {

    private static final CloudNetDriver cloudNetDriver = CloudNetDriver.getInstance();
    private final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
    private HashMap<UUID, Long> timePlayed = new HashMap<>();

    public Account getAccount(UUID uuid) {
        if (SpigotCore.debug)
            Bukkit.broadcastMessage("*");
        return new SpigotAccountProvider(uuid).getAccount();
    }

    public Account getAccount(String username) {
        try {
            ICloudPlayer iCloudPlayer = playerManager.getFirstOnlinePlayer(username);
            if (iCloudPlayer != null) {
                return getAccount(iCloudPlayer.getPlayerExecutor().getPlayerUniqueId());
            } else {
                return new SpigotAccountProvider(username).getUsername().get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateAccount(Account account, boolean connected) {
        SpigotAccountProvider accountProvider = new SpigotAccountProvider(account.getUuid());

        Player player = Bukkit.getPlayer(account.getUuid());

        try {
            if (connected) {
                accountProvider.sendRedis(account);
            } else {
                accountProvider.sendRedis(account);
                accountProvider.sendMySQL(account);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }



    public void addJobXp(UUID uuid, float xp, String job) {
        Account account = getAccount(uuid);
        account.addLevel(job, xp);
        updateAccount(account, true);
    }

    public void removeJobXp(UUID uuid, float xp, String job) {
        Account account = getAccount(uuid);
        account.removeLevel(job, xp);
        updateAccount(account, true);
    }

    public void clearPlayer(Player player) {
        player.getInventory().setBoots(null);
        player.getInventory().setLeggings(null);
        player.getInventory().setChestplate(null);
        player.getInventory().setHelmet(null);

        player.getInventory().clear();

        giveDefaultInventory(SpigotCore.getApi().getAccount(player.getUniqueId()));
        player.updateInventory();
    }

    public void giveDefaultInventory(Account account) {
        Player player = Bukkit.getPlayer(account.getUuid());
        ItemStack itemStack;
        ItemMeta itemMeta;
        itemStack = new ItemStack(Material.NETHER_STAR);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(LangAPI.get().parse(player.getUniqueId(), "skyapi.item.mainmenu"));
        itemMeta.setUnbreakable(true);
        itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        itemStack.setItemMeta(itemMeta);
        player.getInventory().setItem(8, itemStack);
        player.updateInventory();
        if (SpigotCore.getServerName().startsWith("Minage-")) {
            int minerLevel = account.getLevel(JobsEnum.MINER.getLibelle());
            int woodcutterLevel = account.getLevel(JobsEnum.WOODCUTTER.getLibelle());
            int farmerLevel = account.getLevel(JobsEnum.FARMER.getLibelle());
            if (minerLevel < 6) {
                itemStack = new ItemStack(Material.WOODEN_PICKAXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, minerLevel, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (minerLevel < 11) {
                itemStack = new ItemStack(Material.STONE_PICKAXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, minerLevel - 5, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (minerLevel < 16) {
                itemStack = new ItemStack(Material.IRON_PICKAXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, minerLevel - 10, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (minerLevel < 21) {
                itemStack = new ItemStack(Material.DIAMOND_PICKAXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, minerLevel - 15, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else {
                itemStack = new ItemStack(Material.DIAMOND_PICKAXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, minerLevel - 15, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            }
            player.getInventory().setItem(0, itemStack);
            if (woodcutterLevel < 6) {
                itemStack = new ItemStack(Material.WOODEN_AXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, woodcutterLevel, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (woodcutterLevel < 11) {
                itemStack = new ItemStack(Material.STONE_AXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, woodcutterLevel - 5, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (woodcutterLevel < 16) {
                itemStack = new ItemStack(Material.IRON_AXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, woodcutterLevel - 10, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (woodcutterLevel < 21) {
                itemStack = new ItemStack(Material.DIAMOND_AXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, woodcutterLevel - 15, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else {
                itemStack = new ItemStack(Material.DIAMOND_AXE);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, woodcutterLevel - 15, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            }
            player.getInventory().setItem(1, itemStack);
            if (farmerLevel < 6) {
                itemStack = new ItemStack(Material.WOODEN_SHOVEL);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, farmerLevel, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (farmerLevel < 11) {
                itemStack = new ItemStack(Material.STONE_SHOVEL);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, farmerLevel - 5, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (farmerLevel < 16) {
                itemStack = new ItemStack(Material.IRON_SHOVEL);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, farmerLevel - 10, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else if (farmerLevel < 21) {
                itemStack = new ItemStack(Material.DIAMOND_SHOVEL);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, farmerLevel - 15, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            } else {
                itemStack = new ItemStack(Material.DIAMOND_SHOVEL);
                itemMeta = itemStack.getItemMeta();
                itemMeta.addEnchant(Enchantment.DIG_SPEED, farmerLevel - 15, true);
                itemMeta.setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
            }
            player.getInventory().setItem(2, itemStack);
        }
    }
}
