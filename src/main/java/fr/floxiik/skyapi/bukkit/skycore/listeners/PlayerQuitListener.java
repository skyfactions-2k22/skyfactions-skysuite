package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.data.Account;
import nl.marido.deluxecombat.DeluxeCombat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        /*if(SpigotCore.getCombatManager().isInCombat(event.getPlayer())) {
            event.getPlayer().damage(500);
        }*/

        if(DeluxeCombat.getAPI().isInCombat(player)) {
            player.damage(5000);
        }

        Account account = SpigotCore.getApi().getAccount(player.getUniqueId());
    }
}
