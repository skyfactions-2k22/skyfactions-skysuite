package fr.floxiik.skyapi.bukkit.skycore.utils.inventory;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.data.Account;
import fr.floxiik.skyapi.data.Crew;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Lore {

    public static ArrayList<String> getProfileLore(Account account) {
        ArrayList<String> lore = new ArrayList<>();

        Crew crew = SpigotCore.getApi().getCrew(account);
        int level = 0;
        for (String job : account.getLevels().keySet()) {
            level += account.getLevel(job);
        }
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        String strDate = formatter.format(date);

        lore.add("");
        lore.add("§7" + strDate + " §8" + SpigotCore.getServerName());
        lore.add(" §fRank: §cAdmin");
        lore.add(" §fFaction: §d" + (crew == null ? "None" : crew.getName()));
        lore.add(" §fMoney: §2$§a" + account.getMoney());
        lore.add(" §fLevel: §b" + level + "✭");
        lore.add(" §7⏣ §a" + SkyCore.getInstance().getLocalization());
        lore.add("");
        lore.add(" §fCrates: §64 left");
        lore.add("");

        return lore;
    }
}
