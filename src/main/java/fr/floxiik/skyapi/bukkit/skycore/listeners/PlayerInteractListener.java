package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.inventories.MenuInventory;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(event.getItem() != null) {
            if (event.getItem().hasItemMeta()) {
                if (event.getItem().getItemMeta().isUnbreakable() && event.getItem().getType() == Material.NETHER_STAR) {
                    event.setCancelled(true);
                    new MenuInventory(event.getPlayer()).open(event.getPlayer());
                }
            }
        }
    }
}
