package fr.floxiik.skyapi.bukkit.skycore.utils;

import com.google.common.base.Strings;

public class ProgressBar {

    public static String getProgressBar(int current, int max, int totalBars, char symbol, String completedColor,
                                        String notCompletedColor) {
        float percent = (float) current / max;
        int progressBars = (int) (totalBars * percent);

        if(progressBars > 1) {
            return Strings.repeat("" + completedColor + symbol, progressBars - 1) + "§a" + symbol
                    + Strings.repeat("" + notCompletedColor + symbol, totalBars - progressBars);
        } else {
            return Strings.repeat("" + completedColor + symbol, progressBars)
                    + Strings.repeat("" + notCompletedColor + symbol, totalBars - progressBars);
        }
    }
}
