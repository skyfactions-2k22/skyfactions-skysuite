package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener implements Listener {

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        SkyCore.getPlayerManager().giveDefaultInventory(SpigotCore.getApi().getAccount(player.getUniqueId()));
    }
}
