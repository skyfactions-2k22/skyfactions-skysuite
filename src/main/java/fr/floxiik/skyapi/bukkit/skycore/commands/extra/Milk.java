package fr.floxiik.skyapi.bukkit.skycore.commands.extra;

import fr.floxiik.skyapi.api.LangAPI;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

public class Milk implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("skycore.milk")) {
                if (args.length > 0 && player.hasPermission("skycore.milk.other")) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        for (PotionEffect effect : player.getActivePotionEffects())
                            target.removePotionEffect(effect.getType());
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.milk.other")
                            .replace("{player}", target.getName()));
                    } else {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.playernotfound"));
                    }
                } else {
                    for (PotionEffect effect : player.getActivePotionEffects())
                        player.removePotionEffect(effect.getType());
                    player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.milk"));
                }
            } else {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.noperm"));
            }
        }
        return false;
    }
}
