package fr.floxiik.skyapi.bukkit.skycore.inventories;

import com.connorlinfoot.titleapi.TitleAPI;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.FastInv;
import fr.floxiik.skyapi.bukkit.skycore.utils.inventory.ItemBuilder;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;

public class EffectsInventory extends FastInv {

    private boolean preventClose = false;
    private Player player;
    private Account account;

    public EffectsInventory(Player player) {
        super(54, LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.title.main") + " §8» §9Effets");
        this.player = player;
        this.account = SpigotCore.getApi().getAccount(player.getUniqueId());

        setItem(21, new ItemBuilder(Material.POTION).name("§7• §5Night Vision §7•").addLore(Arrays.asList("", "§eNiveau de §2Farm §eminimum:", " §7- §6Level 2", "", "§7§lClic-Gauche §7pour §aactiver", "§7§lClic-Droit §7pour §cdésactiver")).flags(ItemFlag.HIDE_POTION_EFFECTS).data(8262).build());
        setItem(23, new ItemBuilder(Material.POTION).name("§7• §aJump Boost §7•").addLore(Arrays.asList("", "§eNiveau de §2Farm §eminimum:", " §7- §6Level 5", "", "§7§lClic-Gauche §7pour §aactiver", "§7§lClic-Droit §7pour §cdésactiver")).flags(ItemFlag.HIDE_POTION_EFFECTS).data(8235).build());
        setItem(32, new ItemBuilder(Material.POTION).name("§7• §6Haste §7•").addLore(Arrays.asList("", "§eNiveau de §2Farm §eminimum:", " §7- §6Level 15", "", "§7§lClic-Gauche §7pour §aactiver", "§7§lClic-Droit §7pour §cdésactiver")).data(8259).flags(ItemFlag.HIDE_POTION_EFFECTS).build());
        setItem(30, new ItemBuilder(Material.POTION).name("§7• §bSpeed §7•").addLore(Arrays.asList("", "§eNiveau de §2Farm §eminimum:", " §7- §6Level 10", "", "§7§lClic-Gauche §7pour §aactiver", "§7§lClic-Droit §7pour §cdésactiver")).data(8226).flags(ItemFlag.HIDE_POTION_EFFECTS).build());

        // Add some blocks to the borders
        int color = ColorEnum.getServerData();
        setItems(getBorders(), new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).name(" ").data(color).build());

        setItem(49, new ItemBuilder(Material.BARRIER).name("§7• §cRetour §7•").build());

        // Prevent from closing when preventClose is to true
        setCloseFilter(p -> this.preventClose);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You opened the inventory");
    }

    @Override
    public void onClose(InventoryCloseEvent event) {
        //event.getPlayer().sendMessage(ChatColor.GOLD + "You closed the inventory");
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        if(event.getView().getTitle().equals(LangAPI.get().parse(player.getUniqueId(), "skyapi.gui.title.main") + " §8» §9Effets")) {
            if(event.getSlot() == 21) {
                if(account.getLevel("farm") < 2) {
                    player.closeInventory();
                    player.sendMessage("§cNiveau de farm trop bas !");
                    return;
                }
                if(event.isLeftClick()) {
                    player.closeInventory();
                    player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 99999, 1));
                    TitleAPI.sendTitle(player, "§a§lActivé", "§6Vous venez d'§aactiver §6l'effet §5Night Vision §6!", 0, 25, 10);
                } else if(event.isRightClick()) {
                    player.closeInventory();
                    player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                    TitleAPI.sendTitle(player, "§c§lDésactivé", "§6Vous venez de §adésactiver §6l'effet §5Night Vision §6!", 0, 25, 10);
                }
            } else if(event.getSlot() == 23) {
                if(account.getLevel("farm") < 5) {
                    player.closeInventory();
                    player.sendMessage("§cNiveau de farm trop bas !");
                    return;
                }
                if(event.isLeftClick()) {
                    player.closeInventory();
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 99999, 1));
                    TitleAPI.sendTitle(player, "§a§lActivé", "§6Vous venez d'§aactiver §6l'effet §aJump Boost §6!", 0, 25, 10);
                } else if(event.isRightClick()) {
                    player.closeInventory();
                    player.removePotionEffect(PotionEffectType.JUMP);
                    TitleAPI.sendTitle(player, "§c§lDésactivé", "§6Vous venez de §adésactiver §6l'effet §aJump Boost §6!", 0, 25, 10);
                }
            } else if(event.getSlot() == 32) {
                if(account.getLevel("farm") < 15) {
                    player.closeInventory();
                    player.sendMessage("§cNiveau de farm trop bas !");
                    return;
                }
                if(event.isLeftClick()) {
                    player.closeInventory();
                    player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 99999, 1));
                    TitleAPI.sendTitle(player, "§a§lActivé", "§6Vous venez d'§aactiver §6l'effet §6Haste §6!", 0, 25, 10);
                } else if(event.isRightClick()) {
                    player.closeInventory();
                    player.removePotionEffect(PotionEffectType.FAST_DIGGING);
                    TitleAPI.sendTitle(player, "§c§lDésactivé", "§6Vous venez de §adésactiver §6l'effet §6Haste §6!", 0, 25, 10);
                }
            } else if(event.getSlot() == 30) {
                if(account.getLevel("farm") < 10) {
                    player.closeInventory();
                    player.sendMessage("§cNiveau de farm trop bas !");
                    return;
                }
                if(event.isLeftClick()) {
                    player.closeInventory();
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 99999, 1));
                    TitleAPI.sendTitle(player, "§a§lActivé", "§6Vous venez d'§aactiver §6l'effet §bSpeed §6!", 0, 25, 10);
                } else if(event.isRightClick()) {
                    player.closeInventory();
                    player.removePotionEffect(PotionEffectType.SPEED);
                    TitleAPI.sendTitle(player, "§c§lDésactivé", "§6Vous venez de §adésactiver §6l'effet §bSpeed §6!", 0, 25, 10);
                }
            } else if(event.getSlot() == 49) {
                new MenuInventory(player).open(player);
            }
        }
    }
}