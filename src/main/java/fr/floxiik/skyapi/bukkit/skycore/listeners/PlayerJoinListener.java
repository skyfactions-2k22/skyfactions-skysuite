package fr.floxiik.skyapi.bukkit.skycore.listeners;

import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.data.Account;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        Player player = event.getPlayer();

        Account account = SpigotCore.getApi().getAccount(player.getUniqueId());

        Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () -> {
            SkyCore.getPlayerManager().giveDefaultInventory(account);
        }, 10);

        //Téléportation
        Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () -> {
            if (SkyCore.getTeleportManager().hasRequest(player.getUniqueId())) {
                player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.teleport.progress"));
                Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () -> {
                    Location location = SkyCore.getTeleportManager().getTeleportLocation(player.getUniqueId());
                    if (location == null) {
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.error"));
                    } else {
                        player.teleport(location);
                        player.sendMessage(LangAPI.get().parse(player.getUniqueId(), "skyapi.teleport.success"));
                    }
                    SkyCore.getTeleportManager().closeTeleportRequest(player.getUniqueId());
                }, 5L);
            }
        }, 5L);
    }
}
