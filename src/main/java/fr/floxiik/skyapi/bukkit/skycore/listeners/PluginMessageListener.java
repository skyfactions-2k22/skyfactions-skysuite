package fr.floxiik.skyapi.bukkit.skycore.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import fr.floxiik.skyapi.api.LangAPI;
import fr.floxiik.skyapi.bukkit.SpigotCore;
import fr.floxiik.skyapi.bukkit.skycore.SkyCore;
import fr.floxiik.skyapi.bukkit.skycore.teleportations.TeleportRequest;
import fr.floxiik.skyapi.bukkit.skycore.teleportations.TeleportType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

public class PluginMessageListener implements org.bukkit.plugin.messaging.PluginMessageListener {

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        if (channel.equalsIgnoreCase("skycore:tp")) {
            final ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
            final String subChannel = in.readUTF();

            switch (subChannel) {
                case "TeleportToPlayer":
                    final String playerWhoTeleport = in.readUTF();
                    final String playerWhereTeleport = in.readUTF();
                    final String serverToTeleport = in.readUTF();

                    final UUID uuid = UUID.fromString(playerWhoTeleport);
                    final UUID uuid2 = UUID.fromString(playerWhereTeleport);

                    final Player player1 = Bukkit.getPlayer(uuid);
                    final Player player2 = Bukkit.getPlayer(uuid2);

                    if (player1 != null && player2 != null) {
                        //Téléportation
                        player1.sendMessage(LangAPI.get().parse(player1.getUniqueId(), "skyapi.teleport.progress"));
                        Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () -> {
                            player1.teleport(player2.getLocation());
                            player1.sendMessage(LangAPI.get().parse(player1.getUniqueId(), "skyapi.teleport.success"));
                        }, 5 * 20L);
                    } else {
                        if (SpigotCore.getServerName().equals(serverToTeleport)) {
                            SkyCore.getTeleportManager().addTeleportRequest(uuid, new TeleportRequest(uuid, null, uuid2));
                        }
                    }
                    break;
                case "TeleportCooldown":
                    final UUID from = UUID.fromString(in.readUTF());
                    final int cooldown = in.readInt();
                    SkyCore.getTeleportManager().addInCooldown(from, cooldown);
                    break;
                case "TeleportRequest":
                    final UUID playerUUID = UUID.fromString(in.readUTF());
                    final String location = in.readUTF();
                    final String server = in.readUTF();
                    final String type = in.readUTF();

                    if(!TeleportType.HOME.getType().equals(type)) {
                        String[] values = location.split(";");
                        final Location loc = new Location(Bukkit.getWorlds().get(0), Double.parseDouble(values[0]), Double.parseDouble(values[1]), Double.parseDouble(values[2]), Float.parseFloat(values[3]), Float.parseFloat(values[4]));

                        if (SpigotCore.getServerName().equals(server)) {
                            SkyCore.getTeleportManager().addTeleportRequest(playerUUID, new TeleportRequest(playerUUID, loc, null));
                        }
                    }
                    break;
                case "RefreshAccount":
                    final UUID account = UUID.fromString(in.readUTF());
                    //SpigotCore.getApi().refreshAccount(account);
                    break;
                default:
                    player.sendMessage("Ce sous channel n'existe pas.");
            }
        }
    }
}

