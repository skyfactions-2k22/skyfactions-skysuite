package fr.floxiik.skyapi.bukkit.skycore.teleportations;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.UUID;

public class TeleportRequest {

    final private UUID player1;
    final private UUID player2;
    final private Location location;

    public TeleportRequest(UUID player1, Location location, UUID player2) {
        this.player1 = player1;
        this.location = location;
        this.player2 = player2;
    }

    public UUID getPlayer1() {
        return player1;
    }

    public UUID getPlayer2() {
        return player2;
    }

    public Location getLocation() {
        if(location == null) {
            return Bukkit.getPlayer(getPlayer2()).getLocation();
        } else {
            return location;
        }
    }
}